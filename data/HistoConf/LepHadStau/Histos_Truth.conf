# axis definitions
Import XAMPPplotting/HistoConf/LepHadStau/Histos_Defs.conf
1DHisto jetVar_pt 20 0 400
1DHisto LowPt 40 0 200
2DHisto LowPtLowPt 40 0 200 40 0 200
#########################################################
#                                                       #
#########################################################
NewVar
    Type 2D
    Template LowPtLowPt
    xLabel p_{T}(e) [GeV]
    yLabel p_{T}(#tau)[GeV]
    Name tau_ele_pt
    ParReader SignalElectrons pt
    ParReader SignalTaus pt    
EndVar
NewVar
    Type 2D
    Template LowPtLowPt
    xLabel p_{T}(#mu) [GeV]
    yLabel p_{T}(#tau)[GeV]
    Name tau_muo_pt
    ParReader SignalMuons pt
    ParReader SignalTaus pt
    noOverFlowPull    
EndVar
   
1DHisto StauProd 3 206 209
1DHisto StauMixAngle 19 0 90
NewVar
    Type 1D
    Name prod_mode
    EvReader int SUSYFinalState
    Template StauProd
    xLabel #tilde{#tau} production mode
    BinXLabel 1 #tilde{#tau}_{1} #tilde{#tau}_{1}
    BinXLabel 2 #tilde{#tau}_{2} #tilde{#tau}_{2}
    BinXLabel 3 #tilde{#tau}_{1} #tilde{#tau}_{2}    
EndVar
NewVar 
    Type 1D
    Template phi
    Name met_phi
    EvReader float TruthMET_phi
    xLabel #phi(E_{T}^{miss}) [rad]
EndVar

NewVar 
    Type 1D
    Template metVar_et
    Name met    
    EvReader floatGeV TruthMET_met
    xLabel E_{T}^{miss} [GeV]
EndVar

NewVar 
    Type Cul1D
    Template met_et
    Name Cul_met    
    EvReader floatGeV TruthMET_met
    xLabel Culumative E_{T}^{miss} [GeV]
EndVar

NewVar 
    Type 1D
    Template metVar_sumet
    Name met_sumet
    EvReader floatGeV TruthMET_sumet
    xLabel #Sigma{E_{T}} [GeV]
EndVar

#########################################################
#               Electrons                               #
#########################################################

NewVar  
    Type 1D
    Template LowPt
    Name el_pt
    ParReader SignalElectrons pt
    xLabel p_{T}(e) [GeV]
EndVar
NewVar  
    Type 1D
    Template eta
    Name el_eta
    ParReader SignalElectrons eta
    xLabel #eta(e)
EndVar
NewVar  
    Type 1D
    Template phi
    Name el_phi
    ParReader SignalElectrons phi
    xLabel #phi(e)
EndVar
NewVar
    Type 1D
    Template metVar_et
    Name  el_MT
    ParReader SignalElectrons MT[0]
    xLabel m_{T}(e, E_{T}^{miss})
EndVar
NewVar
    Type 1D
    Template dPhi
    Name el_dPhi_met
    dPhiToMetReader SignalElectrons[0] TruthMET
    xLabel #Delta#phi(E_{T}^{miss}, e)
EndVar
#########################################################
#               Muons                                   #
#########################################################
NewVar  
    Type 1D
    Template LowPt
    Name mu_pt
    ParReader SignalMuons pt
    xLabel p_{T}(#mu) [GeV]
EndVar
NewVar  
    Type 1D
    Template eta
    Name mu_eta
    ParReader SignalMuons eta
    xLabel #eta(#mu)
EndVar
NewVar  
    Type 1D
    Template phi
    Name mu_phi
    ParReader SignalMuons phi
    xLabel #phi(#mu)
EndVar

NewVar
    Type 1D
    Template metVar_et
    Name  mu_MT
    ParReader SignalMuons MT[0]
    xLabel m_{T}(#mu, E_{T}^{miss})
EndVar
NewVar
    Type 1D
    Template dPhi
    Name mu_dPhi_met
    dPhiToMetReader SignalMuons[0] TruthMET
    xLabel #Delta#phi(E_{T}^{miss}, #mu)
EndVar
############################################################
#                 Taus                                     #
############################################################
NewVar  
    Type 1D
    Template LowPt
    Name tau_pt
    ParReader SignalTaus pt
    xLabel p_{T}(#tau) [GeV]
EndVar
NewVar  
    Type 1D
    Template eta
    Name tau_eta
    ParReader SignalTaus eta
    xLabel #eta(#tau)
EndVar
NewVar  
    Type 1D
    Template phi
    Name tau_phi
    ParReader SignalTaus phi
    xLabel #phi(#tau) [rad]
EndVar
NewVar
    Type 1D
    Template metVar_et
    Name  tau_MT
    ParReader SignalTaus MT[0]
    xLabel m_{T}(#tau, E_{T}^{miss})
EndVar
NewVar
    Type 1D
    Template dPhi
    Name tau_dPhi_met
    dPhiToMetReader SignalTaus[0] TruthMET
    xLabel #Delta#phi(E_{T}^{miss}, #tau)
EndVar

############################################################
#                 Jets                                     #
############################################################
NewVar  
    Type 1D
    Template jetVar_pt
    Name jet_pt
    ParReader SignalJets pt
    xLabel p_{T}(jet) [GeV]
EndVar
NewVar  
    Type 1D
    Template eta
    Name jet_eta
    ParReader SignalJets eta
    xLabel #eta(jet)
EndVar
NewVar  
    Type 1D
    Template phi
    Name jet_phi
    ParReader SignalJets phi
    xLabel #phi(jet) [rad]
EndVar
NewVar
    Type 1D
    Template metVar_et
    Name  jet_MT
    MtMetReader SignalJets TruthMET
    xLabel m_{T}(jet, E_{T}^{miss})
EndVar
NewVar
    Type 1D
    Template num_jet
    Name N_jets
    NumParReader SignalJets
    xLabel N_{jets}
EndVar

################################################################
#          Staus                                               #
################################################################
NewVar  
    Type 1D
    Template jetVar_pt
    Name stau_pt
    ParReader TruthStaus pt
    xLabel p_{T}(#tilde{#tau}) [GeV]
EndVar

NewVar  
    Type 1D
    Template broad_eta
    Name stau_eta
    ParReader TruthStaus eta
    xLabel #eta(#tilde{#tau})
EndVar
NewVar  
    Type 1D
    Template phi
    Name stau_phi
    ParReader TruthStaus phi
    xLabel #phi(#tilde{#tau}) [rad]
EndVar

NewVar  
    Type 1D
    Template cos_theta
    Name stau_theta
    ParReader TruthStaus CosThetaStar
    xLabel cos(#theta^{*}) , #theta^{*} = #angle(#tilde{#tau} #dot #tilde{#chi}^{0}_{1})
EndVar

1DHisto StauMass 20 200 400
NewVar
    Type 1D
    Template StauMass
    Name stau_mass
    ParReader TruthStaus m
    xLabel m_{#tilde{#tau}}
EndVar
##########################################################################
#               correlations between the particles                       #
##########################################################################
1DHisto Imbalance 20 0. 1.
NewVar
    Type 1D
    Template Imbalance   
    xLabel imbalance p_{T} (e, #tau)
    Name Imbal_e_tau 
    |PtImbalReader| SignalTaus[0] SignalElectrons[0]
EndVar
NewVar
    Type 1D
    Template Imbalance   
    xLabel imbalance p_{T} (#mu, #tau)
    Name Imbal_mu_tau 
    |PtImbalReader| SignalTaus[0] SignalMuons[0]
EndVar
NewVar
    Type 1D
    Template Imbalance   
    xLabel imbalance p_{T} (#tau, #tau)
    Name Imbal_tau_tau 
    |PtImbalReader| SignalTaus[1] SignalTaus[0]
EndVar

NewVar
    Type 1D
    Template dR
    dRReader SignalTaus[0] SignalMuons[0]
    Name dR_tau_muo
    xLabel #DeltaR(#tau, #mu)
EndVar
NewVar
    Type 1D
    Template dR
    dRReader SignalTaus[0] SignalTaus[1]
    Name dR_tau_tau
    xLabel #DeltaR(#tau, #tau)
EndVar
NewVar
    Type 1D
    Template dR
    dRReader SignalTaus[0] SignalElectrons[0]
    Name dR_tau_ele
    xLabel #DeltaR(#tau, e)
EndVar
##### dEta
NewVar
    Type 1D
    Template dEta
    |dEtaReader| SignalTaus[0] SignalMuons[0]
    Name dEta_tau_muo
    xLabel #Delta#eta(#tau, #mu)
EndVar
NewVar
    Type 1D
    Template dEta
    |dEtaReader| SignalTaus[0] SignalTaus[1]
    Name dEta_tau_tau
    xLabel #Delta#eta(#tau, #tau)
EndVar
NewVar
    Type 1D
    Template dEta
    |dEtaReader| SignalTaus[0] SignalElectrons[0]
    Name dEta_tau_ele
    xLabel #Delta#eta(#tau, e)
EndVar
#### dPhi
NewVar
    Type 1D
    Template dPhi
    |dPhiReader| SignalTaus[0] SignalMuons[0]
    Name dPhi_tau_muo
    xLabel #Delta#phi(#tau, #mu)
EndVar
NewVar
    Type 1D
    Template dPhi
    |dPhiReader| SignalTaus[0] SignalTaus[1]
    Name dPhi_tau_tau
    xLabel #Delta#phi(#tau, #tau)
EndVar
NewVar
    Type 1D
    Template dPhi
    |dPhiReader| SignalTaus[0] SignalElectrons[0]
    Name dPhi_tau_ele
    xLabel #Delta#phi(#tau, e)
EndVar



# effective pt = (lep + tau + met ) pt (vectorial)
NewVar 
    Type 1D
    Template Var_pTeff
    Name pTeff_lep_tau_met    
    EvReader floatGeV VecSumPt_LepTau
    xLabel p_{T}^{eff}(lep, #tau, MET) [GeV]
EndVar


### stransverse mass ##
NewVar
    Type 1D
    Template mt2
    Name mt2
    EvReader floatGeV MT2_max
    xLabel m_{T2}(lep, #tau, MET) [GeV]
EndVar

###       Event shape variables ###

NewVar
    Type 1D
    Template spherocity
    Name spherocity
    EvReader float Spherocity
    xLabel Spherocity
EndVar

NewVar
    Type 1D
    Template sphericity
    Name sphericity
    EvReader float Sphericity
    xLabel Sphericity
EndVar

NewVar
    Type 1D
    Template planarity
    Name planarity
    EvReader float Planarity
    xLabel Planarity
EndVar

NewVar
    Type 1D
    Template aplanarity
    Name aplanarity
    EvReader float Aplanarity
    xLabel Aplanarity
EndVar

NewVar
    Type 1D
    Template sphericity
    Name lin_sphericity
    EvReader float Lin_Sphericity
    xLabel Linearized Sphericity
EndVar

NewVar
    Type 1D
    Template sphericity
    Name lin_sphericity_C
    EvReader float Lin_Sphericity_C
    xLabel Linearized Sphericity C
EndVar

NewVar
    Type 1D
    Template sphericity
    Name lin_sphericity_D
    EvReader float Lin_Sphericity_D
    xLabel Linearized Sphericity D
EndVar

NewVar
    Type 1D
    Template planarity
    Name lin_planarity
    EvReader float Lin_Planarity
    xLabel Linearized Planarity
EndVar

NewVar
    Type 1D
    Template aplanarity
    Name lin_aplanarity
    EvReader float Lin_Aplanarity
    xLabel Linearized Aplanarity
EndVar

NewVar
    Type 1D
    Template thrust
    Name thrust
    EvReader float Thrust
    xLabel Thrust
EndVar

NewVar
    Type 1D
    Template thrust_axis
    Name thrust_major
    EvReader float ThrustMajor
    xLabel Thrust major axis
EndVar

NewVar
    Type 1D
    Template thrust_axis
    Name thrust_minor
    EvReader float ThrustMinor
    xLabel Thrust minor axis
EndVar

NewVar
    Type 1D
    Template oblateness
    Name oblateness
    EvReader float Oblateness
    xLabel Oblateness
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_0
    EvReader float WolframMoment_0
    xLabel Fox-Wolfram moment 0
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_1
    EvReader float WolframMoment_1
    xLabel Fox-Wolfram moment 1
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_2
    EvReader float WolframMoment_2
    xLabel Fox-Wolfram moment 2
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_3
    EvReader float WolframMoment_3
    xLabel Fox-Wolfram moment 3
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_4
    EvReader float WolframMoment_4
    xLabel Fox-Wolfram moment 4
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_5
    EvReader float WolframMoment_5
    xLabel Fox-Wolfram moment 5
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_6
    EvReader float WolframMoment_6
    xLabel Fox-Wolfram moment 6
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_7
    EvReader float WolframMoment_7
    xLabel Fox-Wolfram moment 7
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_8
    EvReader float WolframMoment_8
    xLabel Fox-Wolfram moment 8
EndVar
NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_9
    EvReader float WolframMoment_9
    xLabel Fox-Wolfram moment 9
EndVar
