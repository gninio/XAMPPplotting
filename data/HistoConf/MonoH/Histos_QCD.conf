#############################################################################################################
#                             Create the histogram templates                                                #
#############################################################################################################

#TH1D <name> <nbins> <low_edge> <high_edge>
1DHisto jet_m 50 0 500
1DHisto jet_pt 50 0 1500
1DHisto fatjet_m 50 0 750
1DHisto phi_hist 40 0 4
1DHisto dR_hist 10 0 5
1DHisto n_hist 10 0 10
1DHisto n_jetmuon 4 0 4
1DHisto met_hist 50 0 2000
1DHisto metsig_hist 50 0 50
1DHisto rho_hist 20 -2 2

VarHisto var_met_hist
0 150 200 300 500 2000 
End_VarHisto

##############################################################################################################
#             With the created templates above lets plot some variables                                      #
##############################################################################################################
#########################
#          Jets         #
#########################

NewVar  
    Type 1D
    Template jet_m
    Name m_jj
    EvReader floatGeV m_jj
    xLabel m_{jj} [GeV]
    noUnderFlowPull
EndVar


########################
#        Fatjet        #
########################
NewVar  
    Type 1D
    Template fatjet_m
    Name fatjets_m
    ParReader FatJetReader m
    xLabel All FatJet M [GeV]
EndVar


#############################
#           MET             #
#############################
NewVar 
    Type 1D
    Template met_hist
    Name met    
    EvReader floatGeV MetTST_met
    xLabel E_{T}^{miss} [GeV]
EndVar

NewVar 
    Type 1D
    Template var_met_hist
    Name met_variable    
    EvReader floatGeV MetTST_met
    xLabel E_{T}^{miss} [GeV]
EndVar

#############################
#       Met Signif          #
#############################
NewVar 
    Type 1D
    Template metsig_hist
    Name met_oversqrt_HT
    EvReader float MetTST_OverSqrtHT
    xLabel E_{T}^{miss}/#sqrt{HT}
EndVar

NewVar 
    Type 1D
    Template metsig_hist
    Name met_oversqrt_sumet  
    EvReader float MetTST_OverSqrtSumET
    xLabel E_{T}^{miss}/#sqrt{sum E_{T}^{miss}}
EndVar

NewVar 
    Type 1D
    Template metsig_hist
    Name met_significance  
    EvReader float MetTST_Significance
    xLabel Object based E_{T}^{miss} Significance
EndVar

NewVar 
    Type 1D
    Template metsig_hist
    Name met_significance_hard
    EvReader float MetTST_Significance_hard
    xLabel Object based E_{T}^{miss} Significance hard dir.
EndVar

NewVar 
    Type 1D
    Template metsig_hist
    Name met_significance_soft 
    EvReader float MetTST_Significance_soft
    xLabel Object based E_{T}^{miss} Significance soft dir.
EndVar

NewVar 
    Type 1D
    Template metsig_hist
    Name met_significance_phireso
    EvReader float MetTST_Significance_phireso
    xLabel Object based E_{T}^{miss} Significance ang. reso.
EndVar

NewVar 
    Type 1D
    Template jet_pt
    Name met_significance_varL
    EvReader float MetTST_Significance_VarL
    xLabel E_{T}^{miss} Significance VarL
EndVar

NewVar 
    Type 1D
    Template jet_pt
    Name met_significance_varL_hard
    EvReader float MetTST_Significance_hard_VarL
    xLabel E_{T}^{miss} Significance Var_L hard
EndVar


NewVar 
    Type 1D
    Template jet_pt
    Name met_significance_varL_soft
    EvReader float MetTST_Significance_soft_VarL
    xLabel E_{T}^{miss} Significance Var_L soft
EndVar

NewVar 
    Type 1D
    Template jet_pt
    Name met_significance_varL_phireso
    EvReader float MetTST_Significance_phireso_VarL
    xLabel E_{T}^{miss} Significance VarL ang. reso.
EndVar

NewVar 
    Type 1D
    Template rho_hist
    Name met_significance_rho_soft
    EvReader float MetTST_Significance_soft_Rho
    xLabel E_{T}^{miss} Significance soft #rho
EndVar

NewVar 
    Type 1D
    Template rho_hist
    Name met_significance_rho
    EvReader float MetTST_Significance_Rho
    xLabel E_{T}^{miss} Significance #rho
EndVar

NewVar 
    Type 1D
    Template rho_hist
    Name met_significance_rho_hard
    EvReader float MetTST_Significance_hard_Rho
    xLabel E_{T}^{miss} Significance hard #rho
EndVar

NewVar 
    Type 1D
    Template rho_hist
    Name met_significance_rho_phireso
    EvReader float MetTST_Significance_phireso_Rho
    xLabel E_{T}^{miss} Significance ang. reso. #rho
EndVar

#############################
#        EventVariables     #
#############################
NewVar 
    Type 1D
    Template phi_hist
    Name event_dPhi_min_MET_jet123
    |EvReader| float DeltaPhiMin3
    xLabel |#Delta#phi_{min}(#vec{jet}_{1-3},#vec{E}_{T}^{miss})|
EndVar

NewVar 
    Type 1D
    Template phi_hist
    Name event_dPhi_j_j
    |EvReader| float DeltaPhiJJ
    xLabel |#Delta#phi(#vec{jet}_{1},#vec{jet}_{2})|
EndVar

NewVar 
    Type 1D
    Template phi_hist
    Name event_dPhi_MET_jj
    |EvReader| float DeltaPhiMetJJ
    xLabel |#Delta#phi(#vec{E}_{T}^{miss},#vec{jet}_{1}+#vec{jet}_{2})|
EndVar

NewVar 
    Type 1D
    Template dR_hist
    Name event_dR_j_j
    |EvReader| float DeltaRJJ
    xLabel |#DeltaR(#vec{jet}_{1},#vec{jet}_{2})|
EndVar


#######################################
#        ParticleMultiplicities       #
#######################################

NewVar 
    Type 1D
    Template n_jetmuon
    Name N_JetsWithMuon04    
    EvReader int N_JetsWithMuon04
    xLabel N(jets with #mu)
EndVar























