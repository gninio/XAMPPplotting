##
## Lepton predefinitions
###########################
defineMultiLine LeptonVarsToLoad
    Var float charge
    Var float z0sinTheta
    Var float d0sig
	Var floatGeV MT
End_MultiLine

defineMultiLine GoodLeptonCut
  # Cut char signal = 1
   Cut char isol = 1
End_MultiLine

defineMultiLine LooseLeptonCut
   CombCut OR
  #      Cut char signal = 0
        Cut char isol = 0
   End_CombCut
End_MultiLine

defineMultiLine NonIsoLeptonCut
   #Cut char signal = 1
   Cut char isol = 0
End_MultiLine

defineMultiLine TrueElectronDef
	CombCut OR
		 Cut int truthType = 21 #GenParticle
		 Cut int truthType = 1  #IsoElectron
		 Cut int truthType = 2  #NonIsoElectron
         Cut int truthType = 3  #BkgElectron
    End_CombCut
	CombCut OR
		Cut int truthOrigin = 1   #SingleElec
        Cut int truthOrigin = 10  #Top
        Cut int truthOrigin = 12  #WBoson
        Cut int truthOrigin = 13  #ZBoson
        Cut int truthOrigin = 14  #Higgs
        Cut int truthOrigin = 15  #HiggsMSSM
        Cut int truthOrigin = 16  #HeavyBoson
        Cut int truthOrigin = 17  #WBosonLRSM
        Cut int truthOrigin = 22  #SUSY
        Cut int truthOrigin = 43  #DiBoson
        Cut int truthOrigin = 44  #ZorHeavyBoson
	End_CombCut
End_MultiLine

defineMultiLine TrueMuonDef
	CombCut OR
		Cut int truthType = 21 #GenParticle
		Cut int truthType = 5  #IsoMuon
		Cut int truthType = 6  #NonIsoMuon
		Cut int truthType = 7  #BkgMuon    
	End_CombCut
	CombCut OR
		Cut int truthOrigin = 2   #SingleMuon
        Cut int truthOrigin = 10  #Top
        Cut int truthOrigin = 12  #WBoson
        Cut int truthOrigin = 13  #ZBoson
        Cut int truthOrigin = 14  #Higgs
        Cut int truthOrigin = 15  #HiggsMSSM
        Cut int truthOrigin = 16  #HeavyBoson
        Cut int truthOrigin = 17  #WBosonLRSM
        Cut int truthOrigin = 22  #SUSY
        Cut int truthOrigin = 43  #DiBoson
        Cut int truthOrigin = 44  #ZorHeavyBoson
	End_CombCut	
End_MultiLine

##
## Baseline lepton
################################
New_Particle BaseMuons muons
   @LeptonVarsToLoad
   ifdef isMC
       Var int truthOrigin
       Var int truthType
   endif 
End_Particle
New_Particle BaseElectrons electrons
   @LeptonVarsToLoad
   ifdef isMC
       Var int truthOrigin
       Var int truthType
   endif
End_Particle

##
## Signal leptons
##########################
New_Particle SignalMuons muons 
   @GoodLeptonCut
   @LeptonVarsToLoad
   ifdef isMC
       Var int truthOrigin
       Var int truthType
   endif 
End_Particle 

New_Particle SignalElectrons electrons
   @GoodLeptonCut
   @LeptonVarsToLoad
   ifdef isMC
        Var int truthOrigin
        Var int truthType
    endif
End_Particle 

##
## Loose leptons
##########################
New_Particle LooseElectrons electrons
   @LeptonVarsToLoad
   @LooseLeptonCut
End_Particle

New_Particle LooseMuons muons
   @LeptonVarsToLoad
   @LooseLeptonCut
End_Particle


## Signal Anti-Iso leptons
##########################

#New_Particle <Name>  <Name of the InputBranches>
New_Particle AntiIsoMuons muons 
   @LeptonVarsToLoad
   @NonIsoLeptonCut
End_Particle 

New_Particle AntiIsoElectrons electrons
   @LeptonVarsToLoad
   @NonIsoLeptonCut
End_Particle 

##
##  leptons with truth match
####################################
ifdef isMC

 #signal (i.e. iso)
 New_Particle SignalTrueElectrons electrons
    @GoodLeptonCut
    @TrueElectronDef
 End_Particle

 New_Particle SignalTrueMuons muons
    @GoodLeptonCut
    @TrueMuonDef
 End_Particle

 # non-iso
 New_Particle AntiIsoTrueElectrons electrons
    @NonIsoLeptonCut
    @TrueElectronDef
 End_Particle

 New_Particle AntiIsoTrueMuons muons
    @NonIsoLeptonCut
    @TrueMuonDef
 End_Particle


endif
