import os, sys, ROOT
import argparse
from pprint import pprint
import XAMPPplotting.PlotUtils
from XAMPPplotting.Utils import setupBaseParser
from XAMPPplotting.Utils import ExtractMasses

#TObjects =[]
#xfilegg = "XAMPPplotting/data/xSection/LHCSUSY_GG_LLE12k_13TeV.txt"
#xfiless = "XAMPPplotting/data/xSection/LHCSUSY_SS_LLE12k_13TeV.txt"


def MassFiles(s):
    print s
    try:
        M, F = int(s.split(",")[0]), s.split(",")[1]
        return M, F
    except:
        raise argparse.ArgumentTypeError("The arguments must be mass ,filename")


def setupArgumentParser():
    parser = argparse.ArgumentParser(description='This script produces graphs showing the cross-sections as a function of the NSLP mass"',
                                     prog='MPlotXsec',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument("--LHCFile", help="The cross-sections copied directly from the LHC cross-section page", default='')
    parser.add_argument('--ResumminoFiles', help='File from resummino', default='')

    #~ parser.add_argument('--ResumminoFile', help='File from resummino', type=MassFiles, default=[], nargs="+")
    return parser


def MassFromFileName(Name):
    return int(Name.replace(".txt", "").split("_")[-1])


def ReadInResummino(Location, Order=2):
    FileContent = {}
    Read = open(Location, "r")
    for Line in Read:
        if "nan" in Line:
            continue
        Line = Line.strip()

        FinalState = int(Line.split("|")[0])
        xSecAndErr = Line.split("|")[Order + 1]

        xSec = float(xSecAndErr.split()[0])
        xSecErr = 1.
        if xSec > 0:
            xSecErr = float(xSecAndErr.split()[1]) / xSec
        else:
            continue
        xSec /= 1000.
        FileContent[FinalState] = {}
        FileContent[FinalState] = (xSec, xSecErr)

    #Overwrite missing stau prodictions
    if 201 in FileContent.iterkeys():
        FileContent[206] = FileContent[201]
    if 204 in FileContent.iterkeys():
        FileContent[210] = FileContent[204]
    if 205 in FileContent.iterkeys():
        FileContent[211] = FileContent[205]
    return FileContent


if __name__ == '__main__':
    Options = setupArgumentParser().parse_args()
    if not os.path.exists(Options.outputDir):
        os.system("mkdir -p " + Options.outputDir)

    xSecDB = {}
    if len(Options.ResumminoFiles) > 0:
        if os.path.isdir(Options.ResumminoFiles):
            for F in os.listdir(Options.ResumminoFiles):
                if not F.endswith(".txt"): continue
                #Assume that the last argument is the mass
                xSecDB[MassFromFileName(F)] = ReadInResummino(Options.ResumminoFiles + "/" + F)
    pprint(xSecDB)
