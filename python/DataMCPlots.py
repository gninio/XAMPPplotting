#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import PlotUtils, draw2DHisto, Stack
from XAMPPplotting.Utils import DifferHistos, CreateRatioHistos, setupBaseParser, store_yields, RemoveSpecialChars
from ClusterSubmission.Utils import CreateDirectory
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.FileStructureHandler import GetStructure, ClearServices
from XAMPPplotting.YieldsHandler import *
import logging


def drawHisto(Options, analysis, region, var, bonusstr="", yields_handler=None):
    HistList = []
    HistToDraw = []
    ratios = []
    datasamples = []

    FullHistoSet = CreateHistoSets(Options, analysis, region, var)

    if not FullHistoSet or not FullHistoSet[0].CheckHistoSet():
        logging.warning('Cannot draw stacked background MC histogram without background samples, skipping...')
        return False

    DefaultSet = FullHistoSet[0]
    pu = PlotUtils(status=Options.label, size=24, lumi=DefaultSet.GetLumi(), lumilabel=Options.lumi_label)

    if len(DefaultSet.GetData()) == 0:
        logging.error("This is DataMCPlots. Please provide a datasample.")
        return False

    if DefaultSet.isTH2():
        if Options.skipTH2: return False
        return draw2DHisto(FullHistoSet, Options, DrawData=True, PlotType="DataMC", SummaryFile="AllDataMCPlots")

    if not DefaultSet.isTH1() or Options.skipTH1:
        return False

    # Create the TCanvas
    logstr = "" if not Options.doLogY else "_LogY"

    if Options.noRatio:
        pu.Prepare1PadCanvas("DataMC_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
        if Options.doLogY:
            pu.GetCanvas().SetLogy()
    else:
        pu.Prepare2PadCanvas("DataMC_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
        pu.GetTopPad().cd()
        if Options.doLogY:
            pu.GetTopPad().SetLogy()

    pu.CreateLegend(Options.LegendXCoords[0], Options.LegendYCoords[0], Options.LegendXCoords[1], Options.LegendYCoords[1],
                    Options.LegendTextSize)

    ### We need to copy the list to a new one otherwise strange things are going to happen
    datasamples = [D for D in DefaultSet.GetData()]
    # Depending on the number of given DSConfigs, there can be more than 1 histoset, use the others for comparison plots
    comparisonStyles = [(ROOT.kGreen, 3395), (ROOT.kBlue, 3305), (ROOT.kRed, 3345)]
    if len(FullHistoSet) > 1:
        for i in range(1, len(FullHistoSet)):
            sumBGHisto = FullHistoSet[i].GetSummedBackground()
            sumBGHisto.SetLineColor(comparisonStyles[-i][0])
            sumBGHisto.SetLineStyle(ROOT.kDashed)
            sumBGHisto.SetTitle(FullHistoSet[i].GetName())
            HistList.append(sumBGHisto)
            pu.AddToLegend(sumBGHisto, Style="L")
            HistToDraw.append((sumBGHisto.GetHistogram(), "sameHIST"))
            for CompareData in FullHistoSet[i].GetData():
                CompareData.SetMarkerColor(comparisonStyles[-i][0])
                CompareData.SetLineColor(comparisonStyles[-i][0])
                CompareData.SetLineStyle(ROOT.kDashed)
                # Check if the datahisto has a difference in a bin
                AppendDataHisto = True
                for CurrentData in datasamples:
                    if not DifferHistos(CompareData, CurrentData):
                        AppendDataHisto = False
                        break
                if AppendDataHisto:
                    datasamples += [CompareData]

            ratios += CreateRatioHistos(FullHistoSet[i].GetData(), FullHistoSet[i].GetSummedBackground())

    # Create Stack containing all the backgrounds in it
    stk = Stack(DefaultSet.GetBackgrounds())
    # HistToDraw.append((stk,"sameHIST"))

    if not Options.noSignal:
        if len(DefaultSet.GetSignals()) == 0:
            logging.warning("You want to draw signals but did not define them in your config.")
        for Sig in DefaultSet.GetSignals(not Options.noSignalStack):
            HistList.append(Sig)
            HistToDraw.append((Sig, "same" + Sig.GetDrawStyle()))

    for Data in datasamples:
        Halo = pu.CreateHaloHistogram(Data.GetHistogram())
        HistToDraw.append((Halo, "same"))
        HistToDraw.append((Data, "same"))
        HistList.append(Data)

    # draw the stack in order to get the histogram for the axis ranges
    HistList.append(DefaultSet.GetSummedBackground())
    ymin, ymax = pu.GetFancyAxisRanges(HistList, Options)

    if math.fabs(ymin) <= 1.e-10 and math.fabs(ymax) <= 1.e-10:
        logging.info("Night time %.2f day time %.2f... Skip plot" % (ymin, ymax))
        return False
    pu.drawStyling(DefaultSet.GetSummedBackground(), ymin, ymax, RemoveLabel=not Options.noRatio, TopPad=not Options.noRatio)
    stk.Draw("sameHist")
    pu.drawSumBg(DefaultSet.GetSummedBackground(), (not Options.noSyst and not Options.noUpperPanelSyst))
    for H, Opt in HistToDraw:
        H.Draw(Opt)

    ### Add the histograms to legend
    pu.AddToLegend(datasamples, Style="PL")
    pu.AddToLegend(stk.GetHistogramStack(), Style="FL")
    pu.AddToLegend(DefaultSet.GetSignals(), Style="L")
    pu.DrawLegend(NperCol=Options.EntriesPerLegendColumn)

    pu.DrawPlotLabels(0.19, 0.83, Options.regionLabel if len(Options.regionLabel) > 0 else region, analysis, Options.noATLAS)

    #yields
    if yields_handler and Options.store_yields_store_values:
        yields_handler.append(Yields(DefaultSet.GetSummedBackground().GetHistogram()))
        yields_handler.append(Yields(DefaultSet.GetData()[0].GetHistogram()))

    #no ratio
    if not Options.noRatio:
        pu.GetTopPad().RedrawAxis()
        pu.GetBottomPad().cd()
        pu.GetBottomPad().SetGridy()
        ratios += CreateRatioHistos(DefaultSet.GetData(), DefaultSet.GetSummedBackground())
        ymin, ymax = pu.GetFancyAxisRanges(ratios, Options, doRatio=True)
        pu.drawRatioStyling(DefaultSet.GetSummedBackground(), ymin, ymax, "Data/MC")
        pu.drawRatioErrors(DefaultSet.GetSummedBackground(), not Options.noSyst, not Options.noRatioSysLegend)

        for r in ratios:
            r.Draw("same")
            r.Draw("sameE0")
            if yields_handler and Options.store_yields_store_ratios:
                if Options.store_yields_dupes_source and Options.store_yields_dupes_target:
                    yields_handler.duplicate(Yields(r), Options.store_yields_dupes_source, Options.store_yields_dupes_target)
                else:
                    yields_handler.append(Yields(r))

    ROOT.gPad.RedrawAxis()

    PreString = region
    if analysis != "":
        PreString = analysis + "_" + region
    if not Options.summaryPlotOnly:
        pu.saveHisto("%s/DataMC_%s_%s%s" % (Options.outputDir, PreString, var, logstr), Options.OutFileType)

    if not Options.noRatio:
        pu.GetTopPad().cd()
    pu.DrawTLatex(0.5, 0.94, "%s%s in %s (%s analysis)" % (var, logstr, region, analysis), 0.04, 52, 22)

    pu.saveHisto("%s/AllDataMCPlots%s" % (Options.outputDir, bonusstr), ["pdf"])

    return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script produces Data/MC histograms from tree files. For more help type \"python DataMCPlots.py -h\"',
        prog='DataMCPlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument('--noHalo', help='draw data points without white shadow', action='store_true', default=False)
    PlottingOptions = parser.parse_args()

    CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions, use_data_ref=True)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    #yields handler
    yh = YieldsHandler(outdir=PlottingOptions.outputDir,
                       outfile=PlottingOptions.store_yields_outfile,
                       raw_yields=PlottingOptions.store_yields_raw,
                       latex_yields=PlottingOptions.store_yields_latex,
                       hbins_yields=PlottingOptions.store_yields_hbins,
                       histo_yields=PlottingOptions.store_yields_histo) if store_yields(PlottingOptions) else None  #use late binding

    #do the actual job
    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            bonusstr = '_%s_%s' % (ana, region)
            if PlottingOptions.doLogY:
                bonusstr += '_LogY'
            bonusstr = RemoveSpecialChars(bonusstr)
            dummy.SaveAs("%s/AllDataMCPlots%s.pdf[" % (PlottingOptions.outputDir, bonusstr))

            Drawn = False
            for var in FileStructure.GetConfigSet().GetVariables(ana, region):
                Drawn = drawHisto(PlottingOptions, ana, region, var, bonusstr=bonusstr, yields_handler=yh) or Drawn

            if Drawn:
                dummy.SaveAs("%s/AllDataMCPlots%s.pdf]" % (PlottingOptions.outputDir, bonusstr))
            else:
                os.system("rm %s/AllDataMCPlots%s.pdf" % (PlottingOptions.outputDir, bonusstr))
    ClearServices()
