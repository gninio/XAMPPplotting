import ROOT, argparse, logging
from array import array
from ClusterSubmission.Utils import CreateDirectory
from XAMPPplotting.Utils import setupBaseParser
m_histo_templater = None


def SetupTemplater(HistoCfg=""):
    global m_histo_templater
    if m_histo_templater: return m_histo_templater
    try:
        m_histo_templater = ROOT.XAMPP.HistoTemplates.getHistoTemplater()
    except:
        ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
        ROOT.xAOD.Init().ignore()
        m_histo_templater = ROOT.XAMPP.HistoTemplates.getHistoTemplater()

    if not m_histo_templater.InitTemplates(HistoCfg):
        sys.exit(1)
    return m_histo_templater


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='This script produces fake ratio files \"python CalculateFakeFactors.py -h\"',
                                     prog='CalculateFakeRatios',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument("--outFile", default="Histograms.root")
    parser.add_argument("--inputFile", help="Input txt to write the histograms from", required=True)
    parser.set_defaults(outputDir="HistoFile/")

    Options = parser.parse_args()

    CreateDirectory(Options.outputDir, False)

    if os.path.exists("%s/%s" % (Options.outputDir, Options.outFile)):
        logging.info("INFO: Delete existing file %s/%s" % (Options.outputDir, Options.outFile))
        os.system("rm %s/%s" % (Options.outputDir, Options.outFile))

    if not Options.outFile.endswith(".root"):
        logging.error("No valid outfile name %s. The file must end with .root" % (Options.outFile))
        exit(1)
    SetupTemplater(Options.inputFile)

    TFile = ROOT.TFile("%s/%s" % (Options.outputDir, Options.outFile), "RECREATE")
    TFile.cd()
    Histo = None
    HistoName = ""
    with open(ResolvePath(Options.inputFile)) as in_file:
        for line in in_file:
            line = line.strip()
            if len(line) < 2 or line.startswith("#"):
                continue
            if line.split()[0] == "New_HistoToSave":
                if Histo:
                    logging.error("Missing 'End_HistoToSave' statement for %s" % (Histo.GetName()))
                    exit(1)
                if "/" in line.split()[2]:
                    if not TFile.GetDirectory(line.split()[2].rsplit("/", 1)[0]):
                        TFile.mkdir(line.split()[2].rsplit("/", 1)[0])
                    TFile.cd(line.split()[2].rsplit("/", 1)[0])
                HistoName = line.split()[2].rsplit("/", 1)[-1]
                Histo = SetupTemplater().GetTemplate(line.split()[1]).Clone(line.split()[2].replace("/", ""))
                Histo.Reset()
            elif line.split()[0] == "End_HistoToSave" and Histo:
                XAMPPplotting.Utils.PrintHistogram(Histo)
                Histo.Write(HistoName)
                Histo = None
            elif line.split()[0] == "BinX":
                Histo.SetBinContent(int(line.split()[1]), float(line.split()[2]))
                if len(line.split()) > 4 and "pm" in line.split()[3]:
                    Histo.SetBinError(int(line.split()[1]), float(line.split()[4]))
            elif line.split()[0] == "BinXY":  #for 2D Histos
                Histo.SetBinContent(int(line.split()[1]), int(line.split()[2]), float(line.split()[3]))
                if len(line.split()) > 5 and "pm" in line.split()[4]:
                    Histo.SetBinError(int(line.split()[1]), int(line.split()[2]), float(line.split()[5]))
            elif line.split()[0] == "BinXYZ":
                Histo.SetBinContent(int(line.split()[1]), int(line.split()[2]), int(line.split()[3]), float(line.split()[4]))
                if len(line.split()) > 6 and "pm" in line.split()[5]:
                    Histo.SetBinError(int(line.split()[1]), int(line.split()[2]), int(line.split()[3]), float(line.split()[6]))
        if Histo:
            logging.error("Missing 'End_HistoToSave' statement for %s" % (Histo.GetName()))
            exit(1)

    TFile.Close()
