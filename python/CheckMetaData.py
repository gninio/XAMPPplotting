#! /usr/bin/env python
import sys, os, re, commands, logging, math
import sqlite3 as lite
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from ClusterSubmission.Utils import ReadListFromFile, CheckRucioSetup, ClearFromDuplicates, convertStdVector
from ClusterSubmission.AMIDataBase import getAMIDataBase

from XAMPPplotting.FileUtils import ReadInputConfig
from XAMPPplotting.PeriodRunConverter import *
from XAMPPplotting.CalculateLumiFromIlumicalc import SetupPRWTool, GetMissingLumiBlocks, CalculateLumiFromRuns, CalculateRecordedLumi

# global variable to hold the normalisation data base
m_NormBase = None


def getCheckMetaDataArguments():
    """
    Get arguments from command line using an argument parser.

    @return     Argument parser with the getCheckMetaData arguments.
    """
    parser = ArgumentParser(
        description=
        'This script checks the number of processed events for XAMPPplotting input configs. For more help type \"python CheckMetaData.py -h\"',
        prog='CheckMetaData',
        formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '-I', '--InputConfig', help='specify one or more input config(s)', required=True, nargs='+', default=[])
    parser.add_argument('--SampleList',
                        '-l',
                        '-L',
                        help='specify sample list(s) used to match datasets instead of using derivation and tag name',
                        nargs='+',
                        default=[],
                        required=True)
    parser.add_argument('-d', '-D', '--derivation', help='specify name of derivation', default='SUSY2')
    parser.add_argument('--ScaleTeXVertically',
                        '-v',
                        '-V',
                        action="store_true",
                        help='scale LaTeX table vertically instead of horizontally',
                        default=False)
    parser.add_argument('-t', '-T', '--tex', help='specify filename for writing table to a LaTeX file', default='')
    parser.add_argument('-C', '--csv', help='specify filename for writing table to a CSV file', default='')
    parser.add_argument('--lumi', action="store_true", help='calculate lumi from lumiblock', default=False)
    parser.add_argument("--MissingOnly",
                        help="Only runs/DS where something is missing will be printed (implemented only for data)",
                        action="store_true",
                        default=False)
    parser.add_argument('-c', '--campaign', help='Specify the MC campaign', default="mc16_13TeV")
    parser.add_argument("--runNumberMC16a",
                        help="Monte Carlo samples have specific runNumbers to distinguish the different campaigns.",
                        type=int,
                        default=284500)
    parser.add_argument("--runNumberMC16d",
                        help="Monte Carlo samples have specific runNumbers to distinguish the different campaigns.",
                        type=int,
                        default=300000)
    parser.add_argument("--runNumberMC16e",
                        help="Monte Carlo samples have specific runNumbers to distinguish the different campaigns.",
                        type=int,
                        default=310000)
    # r9838 special mc16a tag with lowered ditau pt threshold
    parser.add_argument('--mc16aTag', help='Which rtag should be used for the mc16a campaign', default=['r9364', 'r9838'], nargs="+")
    # r10261 special mc16d tag with lowered ditau pt threshold
    parser.add_argument('--mc16dTag', help='Which rtag should be used for the mc16c campaign', default=['r10201', 'r10261'], nargs="+")
    # r11262 special mc16d tag with lowered ditau pt threshold
    parser.add_argument('--mc16eTag', help='Which rtag should be used for the mc16c campaign', default=['r10724', 'r11262'], nargs="+")
    parser.add_argument("--AvailableOnly",
                        help="Only datasets having an entry in the meta-data tree are considered",
                        default=False,
                        action='store_true')
    return parser


def GetNormalizationDB(Files=[], Delete=False):
    """
    Create XAMPP normalization database managing the metadata from the XAMPP
    ntuples (Normalization, TotalEvents, etc.). If it already exists return
    existing database. The NormalizationDatabase is defined in
    XAMPPplotting/XAMPPplotting/MetaDataTreeReader.h.

    @param      Files   The files for which entries in the database should be created.
    @param      Delete  Delete the current normalisation database and create a new one.

    @return     The normalization db.
    """
    global m_NormBase
    if m_NormBase:
        if not Delete: return m_NormBase
        else: m_NormBase.resetDataBase()
    try:
        import ROOT
        m_NormBase = ROOT.XAMPP.NormalizationDataBase.getDataBase()
    except:
        logging.error(
            "ERROR: This is a crazy error (probably) that crashed this call. If you see some <python: symbol lookup error: >, you can solve it by using a fresh shell, FIRST setting up rucio and pyami and THEN setting up asetup. It is weird. We are as frustrated as you are. There is no solution to the problem (only Zuul)."
        )

    TotalMetaDataList = convertStdVector(Files)
    if not m_NormBase.init(TotalMetaDataList):
        raise Exception("Could not setup the database")
    m_NormBase.PromptMetaDataTree()
    return m_NormBase


def GetFilesPerSample(InputConfigs):
    """
    Show associated files to the input config. If instead of an input config the
    argument is a directory, loop over all inputs in it.

    @param      InputConfigs  Path to an input config or path to a folder with
                              input configs in it

    @return     Dictionary with files per sample
    """
    FilesPerSample = {}
    # Check if the inputconfigs variable is a path to a directory: if so, loop
    # over all input configs inside.
    if len(InputConfigs) == 1 and os.path.isdir(InputConfigs[0]):
        logging.info("The given input is a directory: %s" % (InputConfigs[0]))
        configs = os.listdir(InputConfigs[0])
        configList = []
        for c in configs:
            # Parse recursively all directories contained in the top level directory
            if os.path.isdir("%s/%s" % (InputConfigs[0], c)):
                FilesPerSample.update(GetFilesPerSample("%s/%s" % (InputConfigs[0], c)))
            else:
                configList.append("%s/%s" % (InputConfigs[0], c))
    else:
        configList = InputConfigs

    for config in configList:
        sample = None
        files = ReadInputConfig(config, sample)
        if not sample:
            sample = config[:config.rfind(".")]  ### Get rid of the .conf in the file names
        if sample not in FilesPerSample.iterkeys():
            FilesPerSample[sample] = files
        else:
            logging.warning("<GetFilesPerSample>: Sample %s already exists" % (sample))
            FilesPerSample[sample] += files
    return FilesPerSample


def convertToAOD(DAOD):
    """
    Convert derived xAOD filename to xAOD filename. For MC files, use the
    "recon" tag, for data files, use the "merge" tag for the AOD name.

    @param      DAOD  The daod name

    @return     The converted AOD name
    """
    AOD = DAOD
    # if the filename is in the rucio format scope:file, remove the scope
    if AOD.find(":") > -1:
        AOD = AOD[AOD.find(":") + 1:]
    # replace any .deriv in the dataset name
    AOD = AOD.replace(".deriv", "")
    # usual DAOD string
    daod_pos = AOD.find("DAOD")
    if daod_pos != -1:
        isData = bool(re.compile("data1[5-8]_13TeV.").match(AOD))
        if isData:
            merge_pos = AOD.find(".merge")
            AOD = AOD[:daod_pos] + ("merge.AOD" if merge_pos == -1 else "AOD") + AOD[AOD.find(".", daod_pos):]
        else:
            recon_pos = AOD.find(".recon")
            AOD = AOD[:daod_pos] + ("recon.AOD" if recon_pos == -1 else "AOD") + AOD[AOD.find(".", daod_pos):]
    # replace the NTUP_PILEUP string
    ntup_pileup = AOD.find("NTUP_PILEUP")
    if ntup_pileup != -1:
        merge_pos = AOD.find(".merge")
        AOD = AOD[:ntup_pileup] + ("merge.AOD" if merge_pos == -1 else "AOD") + AOD[AOD.find(".", ntup_pileup):]
    # remove the ptags
    while AOD.rfind("_p") > AOD.find("AOD"):
        AOD = AOD[:AOD.rfind("_p")]
        break
    return AOD


def GetDatasetsFromPeriodContainer(container):
    """
    @brief      Gets the datasets from period container.

    @param      container  The container

    @return     The datasets from period container.
    """
    if container.split(".")[1].find("period") == -1:
        logging.warning("<GetDatasetsFromPeriodContainer> : The input is not a period container: %s" % (container))
        return []
    Datasets = commands.getoutput("rucio list-content %s --short" % (container)).split("\n")
    # A typical output of that command might be
    # data17_13TeV:data17_13TeV.00334842.physics_Main.deriv.DAOD_SUSY18.f867_m1860_p3372_tid12616776_00
    # Need to get rid of the scope in the beginning and the _tid appendices in
    # the name otherwise AMI will not process it
    return [D[(0 if D.find(":") == -1 else D.find(":") + 1):D.find("_tid")] for D in Datasets]


def GetSampleToCheck(Options):
    """
    @brief      Gets the sample to check as a list. Dynamically decide if the
                sample list is a period container of data or something else. If
                the sample list contains derivations, the derivation is
                autodetected and set to the last processed derivation. Since in
                most cases each analysis uses only one derivation, this is the
                desired behavour.

    @param      Options  The run options of this script.

    @return     List of the samples that should be checked.
    """
    logging.info("<GetSampleToCheck>: Load the samples from the file list used to submit the ntuples")
    SampleToCheck = []
    for ilist in Options.SampleList:
        files_persample = []
        for L in ReadListFromFile(ilist):
            L = L.split(":")[-1].replace("/", "")
            # User submitted on a period container
            if L.find("period") != -1:
                files_persample += GetDatasetsFromPeriodContainer(L)
            else:
                files_persample += [L]

        SampleToCheck += files_persample
    for S in SampleToCheck:
        if S.find("DAOD") != -1:
            Options.derivation = S.split('DAOD_')[1].split('.')[0]
            logging.info('<GetSampleToCheck>: Changing derivation to %s' % (Options.derivation))
            break
    if len(SampleToCheck) > 0:
        logging.info('<GetSampleToCheck>: Having found %s datasets to check against.' % len(SampleToCheck))
    return ClearFromDuplicates(SampleToCheck)


def ListHasOnly(ilist, string):
    """
    @brief      Helper function, returns true if the string supplied as an argument is included in every single element in the list.

    @param      ilist   The ilist that should be checked
    @param      string  The string that should be contained in every element of the list to return True

    @return     True if every list element contains the string as a substring, else False
    """
    if len(ilist) == 0: return False
    for l in ilist:
        if string not in l: return False
    return True


def GetDSTag(DSName):
    """
    @brief      Get dataset tag from dataset name. Assume that the dataset name
                is something like
                data17_13TeV.00334842.physics_Main.deriv.DAOD_SUSY18.f867_m1860_p3372

    @param      DSName  The dataset name

    @return     The ds tag.
    """
    return DSName.split(".")[-1]


def GetDSFromList(List, ident):
    """
    @brief      Gets the ds from list by identifier.

    @param      List   The list
    @param      ident  The identifier

    @return     The ds from list.
    """
    for item in List:
        # #Assume that the DS has data_.Ident
        if (item.find(str(ident)) > item.find(".")):
            return item
    return ""


def GetNumberOfEventsFromAMI(SamplesToCheck, identifier, deriv_flavour, ConvertToAOD):
    """
    @brief      Gets the number of events from ami. Currently only seems to work for data.

    @param      SamplesToCheck  The samples to check
    @param      identifier      The identifier
    @param      deriv_flavour   The deriv flavour
    @param      ConvertToAOD    The convert to aod

    @return     The number of events from ami.
    """

    Dataset = GetDSFromList(SamplesToCheck, identifier)
    if len(Dataset) == 0: return -1
    if ConvertToAOD: Dataset = convertToAOD(Dataset)
    if GetNormalizationDB().isData():
        run_element = getAMIDataBase().getRunElement(identifier)
        if run_element:
            return run_element.getEvents(tag=GetDSTag(Dataset), data_type="AOD" if ConvertToAOD else "DAOD_%s" % (deriv_flavour))
    else:
        logging.info("Monte carlo")
    return 0


def GetxSectTimes(mc):
    """
    @brief      Using the XAMPPplotting::NormalizationDataBasemethod, retrieve
                cross-section times branching ratio times k-factor from the
                normalization database. If the mc sample has different possible
                processes (typical for SUSY searches), then the process is
                determined automatically and if there are more processes, the
                sum of these is considered.

    @param      mc    dsid of the MC sample

    @return     Cross-section (taking into account filter efficiency and
                k-factor)
    """
    Proc = [P for P in GetNormalizationDB().GetListOfProcesses(mc) if P < 1000]
    if len(Proc) == 0:
        return -1.
    elif len(Proc) <= 2:
        return GetNormalizationDB().GetxSectTimes(mc, Proc[-1])
    xSec = 0.
    for P in Proc:
        if P == 0: continue
        xSec += GetNormalizationDB().GetxSectTimes(mc, P) * GetNormalizationDB().GetTotalEvents(mc, P) / float(
            GetNormalizationDB().GetTotalEvents(mc, 0))
    return xSec


def GetAMIMetaData(Options, xsecDB):
    """
    @brief      Get the metadata information from AMI using the AMI database.
                The relevant information is retrieved in large chunks to speed
                up the process. This may cause some error messages due to
                non-existing datasets because also DSIDs that are non-associated
                to AMI are checked but these can be ignored.

    @param      Options  The command line options.
    @param      xsecDB   The xsec db

    @return     The AMI dictionary with the metadata information from AMI.
    """
    SamplesToCheck = GetSampleToCheck(Options)
    AMIDict = {}
    if xsecDB.isData():
        Years = [i for i in range(15, 19)]
        # Check if the samples to check consist only of one year - if so, check
        # only this year:
        for i in range(15, 19):
            if ListHasOnly(SamplesToCheck, "data%d" % (i)):
                lgging.info('Constraining data to check to year ' + str(i))
                Years = [i]

        Submitted_Runs = [int(R.split(".")[1]) for R in SamplesToCheck]

        # loop over years
        for Y in Years:
            getAMIDataBase().loadRuns(Y, derivations=["DAOD_%s" % Options.derivation])
            AMIDict[Y] = {}
            Periods = GetPeriodRunConverter().GetPeriods(Y)
            for P in Periods:
                logging.info("Check the runs in period %s of year %i" % (P, Y))
                AMIDict[Y][P] = {}
                RunsToCheck = GetPeriodRunConverter().GetRunsFromPeriod(Y, P)
                for R in RunsToCheck:
                    # Only look at the runs which were actually submitted
                    if R not in Submitted_Runs: continue
                    # Was the run really processed on the grid?
                    RunIsInMetaData = R in xsecDB.GetRunNumbers()
                    Lumi = 0. if not Options.lumi or not RunIsInMetaData else CalculateRecordedLumi(R)
                    ExpLumi = 0. if not Options.lumi else CalculateLumiFromRuns(R, R)
                    # Events in the AMI database
                    TotalEvInAmi = GetNumberOfEventsFromAMI(SamplesToCheck, R, Options.derivation, True)
                    ProcessedEvInAmi = GetNumberOfEventsFromAMI(SamplesToCheck, R, Options.derivation, False)
                    if Options.MissingOnly and RunIsInMetaData and (TotalEvInAmi == xsecDB.GetTotalEvents(R)
                                                                    or ProcessedEvInAmi == xsecDB.GetProcessedEvents(R)):
                        continue
                    AMIDict[Y][P][R] = {
                        "TotalEvAMI": TotalEvInAmi,
                        "ProcessedEvAMI": ProcessedEvInAmi,
                        "TotalEv": xsecDB.GetTotalEvents(R) if RunIsInMetaData else 0,
                        "ProcessedEv": xsecDB.GetProcessedEvents(R) if RunIsInMetaData else 0,
                        "ExpLumi": ExpLumi,
                        "Lumi": Lumi,
                        "MissingLumiBlocks": [] if not Options.lumi or (Lumi - ExpLumi) < 1.e-12 else GetMissingLumiBlocks(R),
                    }
    else:
        sys.stdout.write('Retrieving data from AMI..')
        sys.stdout.flush()
        Submitted_DSIDs = ClearFromDuplicates([int(MC.split(".")[1]) for MC in SamplesToCheck if not Options.AvailableOnly] +
                                              [ch for ch in xsecDB.GetListOfMCSamples() if not Options.MissingOnly])

        getAMIDataBase().getMCDataSets(channels=Submitted_DSIDs, campaign=Options.campaign, derivations=["DAOD_%s" % (Options.derivation)])
        ### search for the pile- up periods in
        for mc in Submitted_DSIDs:
            channel = getAMIDataBase().getMCchannel(mc, campaign=Options.campaign)
            if not channel:
                logging.warning(
                    "<GetAMIMetaData>: Whaaat the dsid %d is unkown??? Did you specify the correct campaign? Currently you have selected %s"
                    % (mc, Options.campaign))
                continue

            DSID_Periods = xsecDB.getMCperiodHandler(mc) if mc in xsecDB.GetListOfMCSamples() else None
            ### The dataset campaigns depend on the period run number. We'd need to add a new runnumber if mc16f
            ### goes online. Okay that is not optimal but I've not a better solution thus far
            Campaigns = [Options.runNumberMC16a, Options.runNumberMC16d, Options.runNumberMC16e]
            ### Let's just check for saftey that we did not process something from mc15 whatever.
            ### So add all runnumber from the MC
            if DSID_Periods != None:
                Campaigns += [period for period in DSID_Periods.getMCcampaigns()]

            Campaigns = ClearFromDuplicates(Campaigns)

            ### Now the fun begins counting the event numbers
            for prw_period in Campaigns:
                rtag_to_use = []
                if prw_period == Options.runNumberMC16a:
                    rtag_to_use = Options.mc16aTag
                elif prw_period == Options.runNumberMC16d:
                    rtag_to_use = Options.mc16dTag
                elif prw_period == Options.runNumberMC16e:
                    rtag_to_use = Options.mc16eTag
                else:
                    logging.warning("<GetAMIMetaData>: Unkown pile up period number %d" % (prw_period))
                    continue

                ### Extract the corresponding datasets from the list
                inputDS = [
                    D for D in SamplesToCheck if int(D.split(".")[1].split(".")[0]) == mc and D.find("DAOD_%s" % Options.derivation) != -1
                    and len([T for T in rtag_to_use if D.find(T) != -1]) == 1
                ]
                if len(inputDS) == 0: continue
                Ami_TotEv = 0
                Ami_ProcEv = 0

                ### Where is my dataset in the input configs???!
                TotEv = DSID_Periods.getHandler(prw_period).TotalEvents(
                    0) if DSID_Periods != None and prw_period in DSID_Periods.getMCcampaigns() else 0
                ProcEv = DSID_Periods.getHandler(prw_period).ProcessedEvents(
                    0) if DSID_Periods != None and prw_period in DSID_Periods.getMCcampaigns() else 0

                ### Loop over the extensions
                for DAOD in inputDS:
                    inputAOD = convertToAOD(DAOD)
                    DAODTag = GetDSTag(DAOD)
                    AODTag = GetDSTag(inputAOD)
                    ### According to PMG we should fall back to the recon.AOD files ---> Get rid of the second r-tag
                    #### Dafuq dudes if you're asking yourself why this would make a difference in AMI, I must admit, I've no clue...
                    #### I'm sitting here with opened mouth and astrounding...
                    while AODTag.find("_r") != AODTag.rfind("_r"):
                        AODTag = AODTag[:AODTag.rfind("_r")]
                    Ami_TotEv += channel.nEvents(tag=AODTag, data_type="AOD")
                    Ami_ProcEv += channel.nEvents(tag=DAODTag, data_type="DAOD_%s" % Options.derivation)

                ### Get the  total integrated luminosity of the Monte Carlo sample
                IntLumi = float(TotEv) / GetxSectTimes(mc)
                if IntLumi < 0 and channel.xSection(): IntLumi = float(TotEv) / channel.xSection()
                if Options.MissingOnly and (Ami_TotEv == TotEv or Ami_ProcEv == ProcEv): continue
                AMIDict[(mc, prw_period)] = {
                    "DSID":
                    mc,
                    "Name":
                    channel.name(),
                    "TotalEvAMI":
                    Ami_TotEv,
                    "ProcessedEvAMI":
                    Ami_ProcEv,
                    "TotalEv":
                    TotEv,
                    "ProcessedEv":
                    ProcEv,
                    "IntLumi":
                    IntLumi,
                    "xSec":
                    -1 if mc not in xsecDB.GetListOfMCSamples() else
                    (xsecDB.GetxSectTimes(mc) if xsecDB.GetNumberOfProcesses(mc) == 1 else sum(
                        [xsecDB.GetxSectTimes(mc, procID) for procID in xsecDB.GetListOfProcesses(mc) if procID != 0]))
                }
    if len(AMIDict) == 0:
        logging.error('No datasets found in PyAmi, exiting...')
        sys.exit(1)
    logging.info("Done!")
    return AMIDict


def formatItem(item, doLaTeX=True):
    if isinstance(item, float):
        if math.fabs(item) >= 1.e-2 or item == 0.: return "%.2f" % (item) if not doLaTeX else "$%.2f$" % (item)
        try:
            ln10 = int(math.log10(item)) - 1
        except ValueError:
            ln10 = 0
        item *= pow(10, math.fabs(ln10))
        if doLaTeX: return "$%.2f\cdot 10^{%d}$" % (item, ln10)
        return "%.2fe%d" % (item, ln10)
    if isinstance(item, int):
        return "%d" % (item) if not doLaTeX else "$%d$" % (item)

    item_str = str(item)
    format_replacement = []
    if doLaTeX:
        format_replacement = [
            ("_", "\\_"),
            ("#bar", "\\bar"),
            ("#rightarrow", "\\rightarrow"),
            ("#downarrow", "\\downarrow"),
            ("#uparrow", "\\uparrow"),
            ("#wedge", "\\wedge"),
            ("Delta", "\\Delta"),
            ("sigma", "\\sigma"),
            ("#tilde", "\\tilde"),
            ("#bf", "\\mathbf"),
            ("#it", "\\mathit"),
            ("#nu", "\\nu"),
            ("#chi", "\\chi"),
            ("#alpha", "\\alpha"),
            ("#beta", "\\beta"),
            ("#pm", "\\pm"),
            ("#mu", "\\mu"),
            ("\ipb'", "pb$^{-1}$"),
            ("$", ""),
            #Assume that _{ is for lowering
            ('\_{', '_{'),
            ("#tilde", "\\tilde"),
        ]
    else:
        format_replacement = [
            ("Delta", (u'\u0394').encode('utf-8')),
            ("Sigma", (u'\u03C3').encode('utf-8')),
            ("\\pm", (u'\u00B1').encode('utf-8')),
            ("$", ""),
        ]
    old_str = item_str
    for rep, wit in format_replacement:
        item_str = item_str.replace(rep, wit)
    if old_str != item_str and doLaTeX: return "\\ensuremath{%s}" % (item_str)
    return item_str


def max_width(data, doLaTeX=False):
    widths = []
    for row in data:
        for i, item in enumerate(row):
            l = len(formatItem(item, doLaTeX=doLaTeX))
            try:
                widths[i] = max(widths[i], l)
            except:
                widths += [l]
    return widths


def prettyPrint(itemsToPrint=[], width=10, doLaTeX=False, row_widths=[]):
    """
    Format output row or header nicely. Also supports LaTeX formatting.

    @param      itemsToPrint  The items to print
    @param      width         The width (good idea to set it to the width
                              retrieved as output of previous call of
                              prettyPrint)
    @param      doLaTeX       Toggle if the output should be formatted for a
                              LaTeX document

    @return     String containing formatted line and width of the output (can be
                used to set width in future call of prettyPrint)
    """
    Line = ""
    myWidth = max([len(formatItem(item, doLaTeX=doLaTeX)) for item in itemsToPrint] + [width]) if len(row_widths) == 0 else 0
    for i, item in enumerate(itemsToPrint):
        if doLaTeX and i != 0: Line += " & "
        Line += formatItem(item, doLaTeX=doLaTeX).ljust(myWidth + 1 if len(row_widths) == 0 else row_widths[i] + 1)
    if doLaTeX:
        Line += '\\\\\n'
        Line = Line.replace('\ipb', 'pb$^{-1}$')
    return Line


def getHeader(isData):
    """
    Provide header as a list for output.

    @param      isData  Indicates if the header is for data or for MC.

    @return     The header as a list of strings.
    """
    header = []
    if isData:
        header += [
            'RunNumber', "Period", 'AMI-Entries (AOD)', 'MetaData-Entries (AOD)', 'Delta(AMI,MetaData) (AOD)',
            'Delta(AMI,MetaData)/AMI (AOD)'
        ]
        header += ['AMI-Entries (DAOD)', 'MetaData-Entries (DAOD)', 'Delta(AMI,MetaData) (DAOD)', 'Delta(AMI,MetaData)/AMI (AOD)']
        header += ["Int. lumi [\ipb]", "Expected int. lumi [\ipb]", "Missing int. lumi [\ipb]"]
    else:
        header = [
            'DSID', 'periodNumber', 'Physics', 'AMI-Entries (AOD)', 'MetaData-Entries (AOD)', 'Delta(AMI,MetaData) (AOD)',
            'Delta(AMI,MetaData)/AMI (DAOD)'
        ]
        header += ['AMI-Entries (DAOD)', 'MetaData-Entries (DAOD)', 'Delta(AMI,MetaData) (DAOD)', 'Delta(AMI,MetaData)/AMI (DAOD)']
        header += ['Cross-section [pb]', 'Total int. lumi [\ipb]']
    return header


def writeTableHeader(header=[], adjust="r", vertical=False, id_cols=2, doLaTeX=True):
    n_col = len(header) - 1
    multi_cols = [int(h[h.find("{") + 1:h.find("}")]) for h in header if h.startswith("\\multicolumn{")]
    if len(multi_cols) > 0: n_col += sum(multi_cols)
    return "\n".join([
        "\\begin{%s}" % ("table" if not vertical else "sidewaystable"),
        "\\begin{adjustbox}{width =0.99\\textwidth}",
        "\\begin{tabular}{%s %s}" % (" ".join(["l" for l in range(id_cols)]), " ".join([adjust for i in range(n_col)])),
        "\\toprule",
        prettyPrint(header, doLaTeX=doLaTeX),
        "\\midrule",
        "\n",
    ])


def writeTableFooter(sample_name, continued=False, vertical=False):
    return "\n".join([
        "\\bottomrule",
        "\\end{tabular}",
        "\\end{adjustbox}",
        "\\caption{MetaData information of %s %s}" % (sample_name.replace("_", "\\_"), '(continued)' if continued else ''),
        "\\end{%s}\n" % ("table" if not vertical else "sidewaystable"),
        "\\clearpage\n\n\n",
    ])


def writeToTex(data, isData, Options, FilesPerSample):
    """
    Write result of checkmetadata to LaTeX file and compile it.

    @param      data            The data retrieved from the database that should
                                be written to a LaTeX document.
    @param      isData          Indicates if data or MC
    @param      Options         The parsed command line options from the
                                ArgumentParser
    @param      FilesPerSample  The files per sample

    """
    # create output file
    with open("%s.tex" % Options.tex, "w") as OutFile:
        # write latex header
        OutFile.write("\\documentclass{article}\n")
        OutFile.write("\\usepackage[a4paper,margin=1in,landscape]{geometry}\n")
        OutFile.write("\\usepackage{booktabs}\n")
        OutFile.write("\\usepackage{graphicx}\n")
        OutFile.write("\\usepackage{adjustbox}\n")
        OutFile.write("\\begin{document}\n")
        # write table header
        OutFile.write(writeTableHeader(header=getHeader(isData), vertical=Options.ScaleTeXVertically))

        widths = max_width(data, doLaTeX=True)
        # write table entries - if there are too many entries, create a new table on a new page
        for count_entries, row in enumerate(data):
            strToTeX = prettyPrint(row, doLaTeX=True, row_widths=widths)
            OutFile.write(strToTeX)
            # if too many entries in table, create new table
            if count_entries > 0 and count_entries % 39 == 0:
                OutFile.write(writeTableFooter(sample_name=FilesPerSample.iterkeys().next(), continued=count_entries > 39))
                OutFile.write(writeTableHeader(header=getHeader(isData), vertical=Options.ScaleTeXVertically))

        OutFile.write(writeTableFooter(sample_name=FilesPerSample.iterkeys().next(), continued=count_entries > 39))
        OutFile.write("\\end{document}\n")
        OutFile.close()

    # build document with pdflatex
    os.system("pdflatex %s.tex" % Options.tex)
    os.system("rm %s.log" % Options.tex)
    os.system("rm %s.aux" % Options.tex)


def writeToCSV(data, isData, Options):
    """
    Write result of checkmetadata to csv file.

    @param      data     The data retrieved from the database that should be
                         written to the CSV file.
    @param      isData   Indicates if data or MC
    @param      Options  The parsed command line options from the ArgumentParser

    """
    # create output file
    OutFile = open("%s.csv" % Options.csv, "w")
    # write to output file
    header = getHeader(isData)
    OutFile.write(','.join([str(h).replace(',', '') for h in header]) + '\n')
    for row in data:
        OutFile.write(','.join([str(r) for r in row]) + '\n')


def printData(data, isData):
    """
    Print result of checkmetadata to screen.

    @param      data    The data retrieved from the database that should be
                         printed to the output.
    @param      isData  Indicates if data or MC

    """
    textOut = prettyPrint(getHeader(isData))
    print(textOut)
    widths = max_width(data, doLaTeX=False)
    for row in data:
        textOut = prettyPrint(row, row_widths=widths)
        print(textOut)


def main():
    Options = getCheckMetaDataArguments().parse_args()
    CheckRucioSetup()

    MissingLumiBlocks = {}
    TotalLumi = 0.
    TotalLumiExp = 0.
    TotalLumiMis = 0.

    # analyse input configs and get all files associated to the input configs
    FilesPerSample = GetFilesPerSample(Options.InputConfig)
    Files = []
    for F in FilesPerSample.itervalues():
        Files += F

    xsecDB = GetNormalizationDB(Files)
    if Options.lumi: SetupPRWTool()
    AMIDict = GetAMIMetaData(Options, xsecDB)

    # set up sqlite database to store metadata
    database = 'checkmetadata.db'
    try:
        con = lite.connect(database)
        c = con.cursor()
        c.execute('DROP TABLE IF EXISTS data')
        c.execute('''CREATE TABLE data
             (run int, period text,
              aod_ami int, aod_local int, aod_delta int, aod_frac real,
              daod_ami int, daod_local int, daod_delta int, daod_frac real,
              lumipb_int real, lumipb_exp real, lumipb_missing)''')
        c.execute('DROP TABLE IF EXISTS mc')
        c.execute('''CREATE TABLE mc
             (dsid int, period int, name text,
              aod_ami int, aod_local int, aod_delta int, aod_frac real,
              daod_ami int, daod_local int, daod_delta int, daod_frac real,
              xsecpb real, lumipb_total real)''')

        if xsecDB.isData():
            AMIruns = []
            for Y in sorted(AMIDict.iterkeys()):
                for P in sorted(AMIDict[Y].iterkeys()):
                    for run in sorted(AMIDict[Y][P].iterkeys()):
                        AMIRun = AMIDict[Y][P][run]
                        AMIruns.append(run)
                        if AMIRun["TotalEvAMI"] == 0: continue
                        # write result to database
                        c.execute(
                            """INSERT INTO data (run,period,aod_ami,aod_local,aod_delta,aod_frac,
                                                       daod_ami,daod_local,daod_delta,daod_frac,
                                                       lumipb_int,lumipb_exp,lumipb_missing)
                                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)""",
                            (run, "%s (%i)" % (P, Y), AMIRun["TotalEvAMI"], AMIRun["TotalEv"], AMIRun["TotalEvAMI"] - AMIRun["TotalEv"],
                             float(AMIRun["TotalEvAMI"] - AMIRun["TotalEv"]) / float(AMIRun["TotalEvAMI"]), AMIRun["ProcessedEvAMI"],
                             AMIRun["ProcessedEv"], AMIRun["ProcessedEvAMI"] - AMIRun["ProcessedEv"],
                             float(AMIRun["ProcessedEvAMI"] - AMIRun["ProcessedEv"]) / float(AMIRun["ProcessedEvAMI"]), AMIRun["Lumi"],
                             AMIRun["ExpLumi"], AMIRun["ExpLumi"] - AMIRun["Lumi"]))

                        # diagnostic checks of the luminosity
                        if Options.lumi:
                            TotalLumi += AMIRun["Lumi"]
                            TotalLumiExp += AMIRun["ExpLumi"]
                            RunLumiMis = AMIRun["ExpLumi"] - AMIRun["Lumi"]
                            if RunLumiMis < 1.e-12:
                                RunLumiMis = 0.
                            if len(AMIRun["MissingLumiBlocks"]) > 0:
                                MissingLumiBlocks[run] = AMIRun["MissingLumiBlocks"]
                            TotalLumiMis += RunLumiMis
            # print diagnostic check about luminosity
            if Options.lumi:
                if len(MissingLumiBlocks) > 0:
                    print 'WARNING: Missing LumiBlocks:'
                    for run, blocks in MissingLumiBlocks.iteritems():
                        print 'Run %i: Did not find the following LumiBlocks in the MetaData tree, which are on the GRL: %s' % (run, blocks)
                for run in xsecDB.GetRunNumbers():
                    if run not in AMIruns:
                        print 'ERROR: Run %s is contained in MetaDataTree but was not found in AMI check!' % run

                TotLumiLine = [
                    "Total int. lumi [\ipb] | expected int. lumi [\ipb] | missing int. lumi [\ibp]: ", TotalLumi, TotalLumiExp, TotalLumiMis
                ]
                strToPrint = prettyPrint(TotLumiLine)
                print strToPrint

            # retrieve data from database
            c.execute("""SELECT * FROM data""")
            data = c.fetchall()

        else:
            for mcChannel, periodNumber in sorted(AMIDict.iterkeys(), key=lambda dict_entry: dict_entry[0]):
                AMIRun = AMIDict[(mcChannel, periodNumber)]
                # calculate integrated luminosity
                intlumi = sum([
                    xsecDB.GetxSection(mcChannel, procID) * xsecDB.GetFilterEfficiency(mcChannel, procID) *
                    xsecDB.GetkFactor(mcChannel, procID) for procID in xsecDB.GetListOfProcesses(mcChannel)
                    if xsecDB.GetNumberOfProcesses(mcChannel) == 1 or procID != 0
                ] if mcChannel in xsecDB.GetListOfMCSamples() else [-1])

                # write result to database
                c.execute(
                    """INSERT INTO mc (dsid,period,name,aod_ami,aod_local,aod_delta,aod_frac,
                                             daod_ami,daod_local,daod_delta,daod_frac,
                                             xsecpb,lumipb_total) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)""",
                    (mcChannel, periodNumber, AMIRun['Name'], AMIRun["TotalEvAMI"], AMIRun["TotalEv"], AMIRun["TotalEvAMI"] -
                     AMIRun["TotalEv"], 0. if float(AMIRun["TotalEvAMI"]) == 0. else float(AMIRun["TotalEvAMI"] - AMIRun["TotalEv"]) *
                     100. / float(AMIRun["TotalEvAMI"]), AMIRun["ProcessedEvAMI"], AMIRun["ProcessedEv"],
                     AMIRun["ProcessedEvAMI"] - AMIRun["ProcessedEv"],
                     0. if float(AMIRun["ProcessedEvAMI"]) == 0. else float(AMIRun["ProcessedEvAMI"] - AMIRun["ProcessedEv"]) * 100. /
                     float(AMIRun["ProcessedEvAMI"]), intlumi, AMIRun["IntLumi"]))

            # retrieve data from database
            c.execute("""SELECT * FROM mc""")
            data = c.fetchall()

        # output
        printData(data, isData=xsecDB.isData())
        if Options.tex: writeToTex(data, isData=xsecDB.isData(), Options=Options, FilesPerSample=FilesPerSample)
        if Options.csv: writeToCSV(data, isData=xsecDB.isData(), Options=Options)
    except lite.Error, e:
        logging.error("Error with database for storing metadata %s:" % e.args[0])
        sys.exit(1)
    finally:
        # close connection
        if con:
            con.close()
        # clean up database
        os.remove(database)


if __name__ == "__main__":
    main()
