from XAMPPplotting.Defs import *
import logging


def AssimovZn(S, B, Sn):
    SB = S + B
    LogFirst = SB * (B + Sn * Sn) / (B * B + SB * Sn * Sn)
    First = SB * math.log(LogFirst)
    LogSecond = 1. + (Sn * Sn * S) / (B * (B + Sn * Sn))
    Second = math.pow((B / Sn), 2) * math.log(LogSecond)
    return 0.5 * (First - Second)


def MCpvalue(S, B, Sn, LowSide, n):
    if S == B and B == 0.:
        return 1.
    Pass = 0
    Fail = 0
    Iter = n
    Prompt = n / 50
    Random = ROOT.TRandom3()
    logging.info("Generate MC based P-value with Background: %.2f Sigma: %.2f Signal: %.2f using %d iterations" % (B, Sn, S, n))
    while (Iter > 0):
        NewBg = Random.Gaus(B, Sn)
        if NewBg < 0.:
            if Fail < 0.1 * n:
                Fail += 1
                continue
            else:
                print "More than 10% of the trials are failing. Return -1."
                return -1.

        NewS = Random.Poisson(NewBg)
        if LowSide and NewS <= S:
            Pass += 1
        elif not LowSide and NewS >= S:
            Pass += 1
        Iter -= 1
        if Iter % Prompt == 0:
            logging.info("%.0f/100%% of the toy probability distributions processed " % (100 * (float(n - Iter) / float(n))))
    P = float(Pass) / float(n)
    if Pass == 0:
        P = 1. / n
    logging.info("Toy Monte Carlo probability distributions processed: %d of which %d  were passed. Return a p-value of %f" % (n, Pass, P))

    return P


def sigmaZ0(cdata, csm, sigma):
    alpha = sigma * sigma / csm
    x = cdata
    y = csm / alpha
    Z0 = 2. / math.sqrt(1. + alpha) * (math.sqrt(x + 0.375) - math.sqrt(alpha * (y + 0.375)))
    return Z0


def ierf(P):
    A = 0.
    B = 0.
    X = 0.
    Z = 0.
    W = 0.
    WI = 0.
    SN = 0.
    SD = 0.
    F = 0.
    Z2 = 0.
    A1 = -0.5751703
    A2 = -1.896513
    A3 = -0.5496261e-1
    B0 = -0.1137730
    B1 = -3.293474
    B2 = -2.374996
    B3 = -1.187515
    C0 = -0.1146666
    C1 = -0.1314774
    C2 = -0.2368201
    C3 = 0.5073975e-1
    D0 = -44.27977
    D1 = 21.98546
    D2 = -7.586103
    E0 = -0.5668422e-1
    E1 = 0.3937021
    E2 = -0.3166501
    E3 = 0.6208963e-1
    F0 = -6.266786
    F1 = 4.666263
    F2 = -2.962883
    G0 = 0.1851159e-3
    G1 = -0.2028152e-2
    G2 = -0.1498384
    G3 = 0.1078639e-1
    H0 = 0.9952975e-1
    H1 = 0.5211733
    H2 = -0.6888301e-1
    #### FIRST  EXECUTABLE STATEMENT
    SIGMA = 0.
    Y = 0.
    X = P
    # ##SIGN(1.0,X)
    SIGMA = P / math.fabs(P)
    # TEST FOR INVALID ARGUMENT
    if X <= -1. or X >= 1.:
        return 0.
    Z = math.fabs(X)
    if Z <= .85:
        # # BETWEEN 0. AND .85, APPROX. F
        Z2 = Z * Z
        # ## BY A RATIONAL FUNCTION IN Z
        F = Z + Z * (B0 + A1 * Z2 / (B1 + Z2 + A2 / (B2 + Z2 + A3 / (B3 + Z2))))
    else:
        A = 1. - Z
        B = Z
        # # REDUCED ARGUMENT IS IN (.85,1.),
        # # BTAIN THE TRANSFORMED VARIABLE
        W = math.sqrt(-math.log(A + A * B))
        if W < 2.5:
            ##### W BETWEEN 1.13222 AND 2.5, APPROX.
            SN = ((C3 * W + C2) * W + C1) * W
            ##### F BY A RATIONAL FUNCTION IN W
            SD = ((W + D2) * W + D1) * W + D0
            F = W + W * (C0 + SN / SD)

        elif W < 4.:
            # #    W BETWEEN 2.5 AND 4., APPROX. F
            SN = ((E3 * W + E2) * W + E1) * W
            # ## F BY A RATIONAL FUNCTION IN W
            SD = ((W + F2) * W + F1) * W + F0
            F = W + W * (E0 + SN / SD)

        else:
            # #W GREATER THAN 4., APPROX. F BY A
            WI = 1. / W
            # ## RATIONAL FUNCTION IN 1./W
            SN = ((G3 * WI + G2) * WI + G1) * WI
            SD = ((WI + H2) * WI + H1) * WI + H0
            F = W + W * (G0 + SN / SD)

    ####  FORM THE SOLUTION BY MULT. F BY THE PROPER SIGN
    Y = SIGMA * F
    return Y


def Gauss(x, x0, sig):
    E = (float(x) - float(x0)) / float(sig)
    return (math.exp(-E * E / 2.) / (math.sqrt(2 * math.pi) * sig))


def fGaussGauss(x=[], par=[]):
    xmin = x[0] - 10. * math.sqrt(x[0])
    xmax = x[0] + 10. * math.sqrt(x[0])
    y = 0.
    ErfTerm = ROOT.TMath.Erf((x[0] - par[2]) / math.sqrt(2.) / math.sqrt(x[0])) - ROOT.TMath.Erf(
        (x[0] - xmax) / math.sqrt(2.) / math.sqrt(x[0]))
    SqrtTerm = 1. / math.sqrt(2 * ROOT.TMath.Pi()) / par[1]
    if x[0] > 0.:
        y = 0.5 * (ErfTerm) * SqrtTerm * ROOT.TMath.Gaus(x[0], par[0], par[1])
    return y


def sigmaZn(cdata, csm, sigma):
    p = pverr2(cdata, csm, sigma)
    if p <= 1.e-5:
        logging.info("The p-value %f seems to be small. Check via MC methods the validity of the result" % (p))
        p = MCpvalue(cdata, csm, sigma, False, 5.e7)
    Zn = ierf(1 - 2. * p) * math.sqrt(2.)
    if Zn > 10.:
        return 10.
    elif Zn < 0:
        return 0
    return Zn


def fPoisGauss(x, par):
    FactN = 1.
    if int(par[2]) - 1 > 0:
        FactN = 1. * math.factorial(int(par[2]) - 1)
    if par[1] == 0:
        par[1] = 1e-7
        # #  Double y=1/sqrt(2*TMath::Pi())/par[1]*TMath::Gaus(x[0],par[0],par[1])*par[2]*
        # #     (TMath::Gamma(par[2])*TMath::Gamma(par[2],x[0]))/FactN;
    y = 1 / math.sqrt(2 * math.pi) / par[1] * ROOT.TMath.Gaus(x[0], par[0],
                                                              par[1]) * (ROOT.TMath.Gamma(par[2]) * ROOT.TMath.Gamma(par[2], x[0])) / FactN
    return y


# ## convolution of poiss+gauss using root functions by E. Sauvan
def pverr2(cdata, csm, sigma):
    #### function pverr1: p value err 1
    ####Aim : calculate Prob. with system. err for a counting exp.
    ####       with signal estimate
    ####     data>=expectation --> Poisson prob convoluted with gaussian
    prob = -1.
    chi2 = 0.
    # ## chi2=fabs(cdata-csm)/(sigma+sqrt(csm));
    if cdata == 0. and csm < 0.1:
        return 1.
    if sigma == 0.:
        sigma = 0.0001
    ErrfPG = ROOT.TF1("ErrfPG", fPoisGauss, 0., 1000., 3)
    ErrfGG = ROOT.TF1("ErrfGG", fGaussGauss, 0., 1000., 3)

    xmin = (csm - 7. * sigma)
    realxmin = (csm - 7. * sigma)
    xmax = (csm + 7. * sigma)
    if xmin < 0.:
        xmin = 0.
    Errf = 0.
    if cdata == 0.:
        return 1.
    if cdata <= 100.:
        Errf = ErrfPG
    else:
        Errf = ErrfGG

    Errf.SetRange(xmin, xmax)
    Errf.FixParameter(0, csm)
    Errf.FixParameter(1, sigma)
    Errf.FixParameter(2, cdata)
    ffint = Errf.Integral(xmin, xmax)

    norm = 0.5 * (math.erf((csm - xmin) / math.sqrt(2.) / sigma) - math.erf((csm - xmax) / math.sqrt(2.) / sigma))

    return ffint / norm


def ButtingerLefebvreSignificance(nObs, nExpBkg, bkgerror):
    """
    Definition from recommendation in https://cds.cern.ch/record/2643488
    """
    if bkgerror < 0:
        logging.warning("Uncertainty on the background is negative!?")
    if nObs <= 0:
        logging.debug("Observed <=0 events. => Significance = 0")
        return 0
    if nExpBkg <= 0:
        logging.debug("Expecting 0 background events. => Significance = 0")
        return 0
    log1 = math.log((nObs * (nExpBkg + bkgerror**2)) / (nExpBkg**2 + nObs * bkgerror**2))
    if bkgerror == 0:
        logging.info("Uncertainty on the background is zero. Using reduced significance formula.")
        return math.copysign(math.sqrt(2 * (nObs * log1 - (nObs - nExpBkg))), nObs - nExpBkg)
    log2 = math.log(1 + (bkgerror**2 * (nObs - nExpBkg)) / (nExpBkg * (nExpBkg + bkgerror**2)))
    z = math.sqrt(2 * (nObs * log1 - (nExpBkg**2 / bkgerror**2) * log2))
    return math.copysign(z, nObs - nExpBkg)
