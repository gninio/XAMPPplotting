import sys, ROOT

from XAMPPplotting import TauHistoLoader as THL
from XAMPPplotting import TauOptions as TO
from XAMPPplotting import TauHelper as TH
from XAMPPplotting import TauFakeFactorsCalc as TFFC


##
# @short main
##
def main(argv):
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    options = TO.TauOptions("tau options").instance

    file_structure = TH.FileStructure(options)

    ##
    #@short important: set same lumi to all MC histos, w/ and w/o truth info
    # otherwise truth histos will be normalized to 1/fb
    options.lumi = file_structure.GetConfigSet().GetLumi()

    histo_loader = THL.TauHistoLoader("direct stau-stau lep-had", options=options, file_structure=file_structure)

    tau_fakes = TFFC.TauFakeFactorsCalc(name="tau fake factors", options=options, tau_histo_loader=histo_loader)

    print "TauMain: Done! Releasing memory - please wait..."


##
# @brief execute main
##
if __name__ == "__main__":

    main(sys.argv[1:])
