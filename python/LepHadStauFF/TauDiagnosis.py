import os
from ROOT import TH1F, TFile, TCanvas, TLegend, TPad, gStyle, gROOT, TLine
from XAMPPplotting.Utils import GetNbins
#
rfilename = "TauAux.root"
rf = TFile.Open(rfilename)

if not rf:
    print "Error: input root file not existing..."
    exit(1)


#
def runs():
    return "276262_364292"


#
def leg(h=None):
    return h.GetName().replace("Period_" + runs(), "").replace("_", " ").replace("nominal", "")


def test(h=None, name=""):
    if not h:
        print "Error: Null histo '%s'" % (name)
        return False

    return True


#
def get(f=None, name=""):

    h = f.Get(name)
    if not test(h, name):
        print "Error: problem with '%s'" % (name)
        exit(1)

    return h


#
def get_histo_limits(histo=None):
    if not histo:
        print "Error: Null histo, cannot retrieve range ..."
        return 0., 0.

    n = GetNbins(histo)
    return histo.GetBinLowEdge(1), histo.GetBinLowEdge(n) + histo.GetBinWidth(n)


#
def overlay(measure="", flavor="", CR1="", CR2="", SR="", eta="", prongs="", maxy=1.5, max_x=0.):

    mainplot0 = "%s_combined_nominal_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Period_%s" % (measure, flavor, CR1, SR, eta, prongs, runs())
    mainplot1 = "%s_combined_nominal_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Period_%s" % (measure, flavor, CR2, SR, eta, prongs, runs())

    h0 = get(rf, mainplot0)
    h1 = get(rf, mainplot1)

    h0.SetLineColor(1)
    h0.SetLineWidth(2)
    h0.SetMarkerColor(1)

    h1.SetLineColor(2)
    h1.SetLineWidth(2)
    h1.SetMarkerColor(2)

    c = TCanvas("c_" + mainplot0 + mainplot1, mainplot0 + mainplot1, 60, 60, 800, 600)
    c.SetTopMargin(c.GetTopMargin() * 1.2)
    c.SetBottomMargin(c.GetBottomMargin() * 1.5)
    c.SetLeftMargin(c.GetLeftMargin() * 1.4)
    c.cd()

    gStyle.SetOptStat(0)

    h0.SetTitle("")
    h0.Draw("e")
    min_x = h0.GetBinLowEdge(1)
    h0.GetXaxis().SetRangeUser(min_x, max_x)
    h0.GetYaxis().SetLabelSize(h0.GetYaxis().GetLabelSize() * 1.)
    h0.GetXaxis().SetLabelSize(h0.GetXaxis().GetLabelSize() * 1.)
    h0.GetXaxis().SetTitleSize(h0.GetXaxis().GetTitleSize() * 1.)

    h0.GetYaxis().SetTitle(measure)

    h0.SetMaximum(maxy)
    h0.SetMinimum(0.)

    h1.Draw("e same")

    legend = TLegend(0.3, 0.85, 0.99, 0.99)
    legend.AddEntry(h0, leg(h0), "l")
    legend.AddEntry(h1, leg(h1), "l")
    legend.Draw()

    ###
    c.Modified()
    c.Update()

    outdir = "./FF"
    outfile = mainplot0 + mainplot1
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    c.SaveAs("%s/%s.pdf" % (outdir, outfile))
    c.SaveAs("%s/AllDiagnostics.pdf" % (outdir))


#
def specific(measure="", flavor="", CR="", SR="", eta="", prongs="", maxy=1.5):

    mainplot = "%s_combined_nominal_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Period_%s" % (measure, flavor, CR, SR, eta, prongs, runs())
    h0 = get(rf, mainplot)

    h0.SetLineColor(1)
    h0.SetLineWidth(2)
    h0.SetMarkerColor(1)

    c = TCanvas("c_" + mainplot, mainplot, 10, 10, 800, 600)
    c.SetTopMargin(c.GetTopMargin() * 2)
    c.cd()

    h0.SetTitle("")
    h0.Draw()
    h0.GetYaxis().SetLabelSize(h0.GetYaxis().GetLabelSize() * 1.)
    h0.GetXaxis().SetLabelSize(h0.GetXaxis().GetLabelSize() * 1.)
    h0.GetXaxis().SetTitleSize(h0.GetXaxis().GetTitleSize() * 1.)

    h0.SetMaximum(maxy)
    h0.SetMinimum(0.)

    legend = TLegend(0.3, 0.8, 0.99, 0.99)
    legend.AddEntry(h0, leg(h0), "l")
    legend.Draw()

    ###
    c.Modified()
    c.Update()

    outdir = "./FF"
    outfile = mainplot
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    c.SaveAs("%s/%s.pdf" % (outdir, outfile))
    c.SaveAs("%s/AllDiagnostics.pdf" % (outdir))


def general(measure="", enum="", deno="", flavor="", CR_enum="", CR_deno="", SR="", eta="", prongs="", maxy=2.5, max_x=0.):

    mainplot = "%s_combined_nominal_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Period_%s" % (measure, flavor, CR_enum, SR, eta, prongs, runs())
    h0 = get(rf, mainplot)

    if CR_enum is "Wjets" or CR_deno is "Wjets":
        specific(measure, flavor, CR_enum, SR, eta, prongs, maxy)
        return

    h1 = get(rf, "%s_dat_%s_nominal_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Period_%s" % (measure, enum, flavor, CR_enum, SR, eta, prongs, runs()))
    h2 = get(rf, "%s_dat_%s_nominal_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Period_%s" % (measure, deno, flavor, CR_deno, SR, eta, prongs, runs()))
    h3 = get(rf, "%s_sim_%s_nominal_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Period_%s" % (measure, enum, flavor, CR_enum, SR, eta, prongs, runs()))
    h4 = get(rf, "%s_sim_%s_nominal_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Period_%s" % (measure, deno, flavor, CR_deno, SR, eta, prongs, runs()))

    h13 = h1.Clone()
    h24 = h2.Clone()

    if not h13.Add(h3, -1.):
        print "Error: cannot subtract 3 from 1"

    if not h24.Add(h4, -1.):
        print "Error: cannot subtract 4 from 2"

    #
    lw = 3

    #
    h0.SetLineColor(1)
    h0.SetLineWidth(lw)
    h0.SetMarkerColor(2)

    #data
    h1.SetLineColor(4)
    h1.SetMarkerColor(4)
    h1.SetLineWidth(lw)

    h2.SetLineColor(2)
    h2.SetMarkerColor(2)
    h2.SetLineWidth(lw)

    #mc
    h3.SetLineColor(4)
    h3.SetMarkerColor(4)
    h3.SetLineWidth(lw)
    h3.SetLineStyle(7)

    h4.SetLineColor(2)
    h4.SetMarkerColor(2)
    h4.SetLineWidth(lw)
    h4.SetLineStyle(7)

    #
    h13.SetLineColor(8)
    h13.SetMarkerColor(8)
    h13.SetLineWidth(lw)

    h24.SetLineColor(6)
    h24.SetMarkerColor(6)
    h24.SetLineWidth(lw)

    c = TCanvas("c_" + mainplot, mainplot, 10, 10, 800, 700)
    c.cd()

    ###
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0.01)
    pad1.SetGridx()
    pad1.SetTicks(1, 1)
    pad1.Draw()
    pad1.cd()
    gStyle.SetOptStat(0)

    h1.SetTitle("")
    h1.Draw("")
    min_x = h1.GetBinLowEdge(1)
    h1.GetXaxis().SetRangeUser(min_x, max_x)

    h2.Draw("same")
    h3.Draw("same")
    h4.Draw("same")
    h13.Draw("same")
    h24.Draw("same")

    h1.GetXaxis().SetLabelSize(0.)
    h1.GetYaxis().SetTitleOffset(1.)
    h1.GetYaxis().SetTitle(measure)
    h1.SetMaximum(h1.GetMaximum() * 1.e3)
    pad1.SetLogy()

    legend = TLegend(0.3, 0.6, 0.99, 0.99)
    legend.AddEntry(h1, leg(h1), "l")
    legend.AddEntry(h2, leg(h2), "l")
    legend.AddEntry(h3, leg(h3), "l")
    legend.AddEntry(h4, leg(h4), "l")
    legend.AddEntry(h13, "enum", "l")
    legend.AddEntry(h24, "denom", "l")
    legend.Draw()

    ###
    c.cd()
    pad2 = TPad("pad2", "pad2", 0, 0, 1, 0.29)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(pad2.GetBottomMargin() * 3)
    pad2.SetGridx()
    pad2.SetTicks(1, 1)
    pad2.Draw()
    pad2.cd()
    gStyle.SetOptStat(0)

    h0.SetTitle("")
    h0.Draw()
    min_x = h0.GetBinLowEdge(1)
    h0.GetXaxis().SetRangeUser(min_x, max_x)

    h0.GetYaxis().SetLabelSize(h0.GetYaxis().GetLabelSize() * 1.5)
    h0.GetXaxis().SetLabelSize(h0.GetXaxis().GetLabelSize() * 2)
    h0.GetXaxis().SetTitleSize(h0.GetXaxis().GetTitleSize() * 2)

    h0.SetMaximum(maxy)
    h0.SetMinimum(0.)

    x1, x2 = get_histo_limits(h0)
    line = TLine(x1, 1., x2, 1.)
    line.SetLineColor(41)
    line.SetLineStyle(7)
    line.Draw()

    ###
    c.Modified()
    c.Update()

    outdir = "./FF"
    outfile = mainplot
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    c.SaveAs("%s/%s.pdf" % (outdir, outfile))
    c.SaveAs("%s/AllDiagnostics.pdf" % (outdir))


#
flavors = [
    "Muo",
    #"Ele"
]
CRs = ["QCD", "Wjets"]
SRs = [
    "PreSel",
    #"0jet",
    #"1jet"
]
prongs = ["1", "3"]
etas = ["0p0_0p8", "0p8_2p5"]

do_F = True
do_I = True
do_R = True

max_x = 170.

if len(flavors) * len(CRs) * len(SRs) * len(prongs) * len(etas) > 1:
    gROOT.SetBatch(1)

DummyCanvas = TCanvas("dummy", "dummy", 800, 600)
DummyCanvas.SaveAs("./FF/AllDiagnostics.pdf[")

if do_F:
    for CR in CRs:
        for SR in SRs:
            for eta in etas:
                for prong in prongs:
                    for flavor in flavors:
                        general(measure="F",
                                enum="pass",
                                deno="fail",
                                flavor=flavor,
                                CR_enum=CR,
                                CR_deno=CR,
                                SR=SR,
                                eta=eta,
                                prongs=prong,
                                maxy=1.75,
                                max_x=max_x)

if do_I:
    for CR in CRs:
        for eta in etas:
            for prong in prongs:
                for flavor in flavors:
                    general(measure="I",
                            enum="pass_iso",
                            deno="pass_anti_iso",
                            flavor=flavor,
                            CR_enum=CR,
                            CR_deno=CR,
                            SR="PreSel",
                            eta=eta,
                            prongs=prong,
                            maxy=20,
                            max_x=max_x)

if do_R:
    for CR in CRs:
        for SR in SRs:
            for eta in etas:
                for prong in prongs:
                    for flavor in flavors:
                        general(measure="R",
                                enum="fail",
                                deno="fail",
                                flavor=flavor,
                                CR_enum=CR,
                                CR_deno="NotApplied",
                                SR=SR,
                                eta=eta,
                                prongs=prong,
                                maxy=1.05,
                                max_x=max_x)

    for SR in SRs:
        for eta in etas:
            for prong in prongs:
                for flavor in flavors:
                    overlay(measure="R", flavor=flavor, CR1="Wjets", CR2="QCD", SR=SR, eta=eta, prongs=prong, maxy=1.05, max_x=max_x)
'''
KEY: TH1DFF_combined_nominal_EleTau_CR_NotApplied_SR_Presel_Eta_0p0_0p8_Prong_1_Period_%s;1Data
KEY: TH1DFF_combined_nominal_EleTau_CR_NotApplied_SR_Presel_Eta_0p8_2p5_Prong_1_Period_%s;1Data
KEY: TH1DFF_combined_nominal_EleTau_CR_NotApplied_SR_Presel_Eta_0p0_0p8_Prong_3_Period_%s;1Data
'''
#specific( measure = "I", enum = "", deno = "pass_anti_iso", flavor = "Mu", CR="Wjets", SR="Presel", eta="0p0_0p8", prongs="1")

#specific( measure = "R", enum = "fail", deno = "fail", flavor = "Mu", CR="Wjets", CR_deno="NotApplied", SR="Presel", eta="0p0_0p8", prongs="1")

DummyCanvas.SaveAs("./FF/AllDiagnostics.pdf]")
## wait for input to keep the GUI (which lives on a ROOT event dispatcher) alive
if __name__ == '__main__':
    rep = ''
    while not rep in ['q', 'Q']:
        rep = raw_input('enter "q" to quit: ')
        if 1 < len(rep):
            rep = rep[0]
