from XAMPPplotting import TauLogger as TL
from XAMPPplotting import TauStore as TS
from XAMPPplotting import TauHelper as TH
from XAMPPplotting import TauFlatFF as TF
from XAMPPplotting.CheckMetaData import GetNormalizationDB
from ClusterSubmission.Utils import id_generator, convertDoubleVector, convertFloatVector, CreateDirectory, ClearFromDuplicates
from XAMPPplotting.PlotUtils import PlotUtils
from XAMPPplotting.Utils import GetNbins, ProjectInto1D, PrintHistogram

import ROOT, math

##
# @class TauFakeFactors
##


##
##  Fake-rate: f = (N_{data}^{pass} - N_{MC,real}^{pass}) / (N_{data}^{failed} - N_{MC,real}^{failed})
##             R = (N_{data,CR-QCD}^{fail} - N_{MC,real,CR-QCD}^{fail}) / (N_{data,SR}^{fail} - N_{MC,real,SR}^{fail})
##       --> thereby CR-QCD is defined as OS (#tau, lepton) pairs with an antiisolated lepton
##      The transfer from AntiIso --> Iso can be corrected by the isolation factor defined by
##             I = (N_{data,QCD-SS}^{fail} - N_{MC,real,CR-QCD-SS}) / (N_{data,QCD-AntiIso}^{fail} - N_{MC,real,CR-QCD-AntiIso})
##      based on an ABCD method.
##  The x-axis is extended up to 6.5 TeV if the observable name contains a pt... So make sure
##  to define histograms in this way if you're using pt as variable
class TauFakeFactorsCalc(object):
    def __init__(self, name="tau fake factod", options=None, tau_histo_loader=None):
        "Analyze loaded histos to estimate tau fake factors"
        #parsed options
        self.__name = name
        self.__options = options
        self.__tau_histo_loader = tau_histo_loader
        # message service
        self.__msg = TL.msg(2)
        self.__msg.info("TauFakeFactorCalc", "'%s' loaded ..." % (self.__name))
        #database
        self.__db_create_report = False  #keep FF yields in a database
        self.__db_file_name = 'TauFF.dat'
        self.__db_file = None
        self.__db_file_ready = self.__create_db()

        self.__tau_definition = options.tau_definition
        self.__observable = options.fake_ratio_obs
        self.__observable_iso = options.isolation_obs
        self.__observable_rfactor = options.rfactor_obs

        self.__subtract_mc = "Real"

        #fixed configurations
        self.__nominal_name = "nominal"
        self.__stat_up_name = "stat_1sigma_up"
        self.__stat_down_name = "stat_1sigma_down"
        self.__flat_ff = [
            TF.TauFlatFF([1], [0.0, 0.8], 0.9),
            TF.TauFlatFF([1], [0.8, 2.5], 1.1),
            TF.TauFlatFF([3], [0.0, 0.8], 0.15),
            TF.TauFlatFF([3], [0.8, 2.5], 0.25)
        ]
        #output root file
        self.__tau_store = TS.TauStore(filename=options.out_file)
        #actions
        self.__calculate_ff = self.__calculate()
        self.__ready = self.__finalize()

    #FF database
    def __create_db(self):
        if not self.__db_create_report:
            return False
        self.__db_file = open(self.__db_file_name, 'w+')

        if not self.__db_file:
            self.__msg.warning("TauFakeFactorsCalc", "Failed to create DB file '%s'..." % (self.__db_file_name))
            return False

        self.__msg.debug("TauFakeFactorsCalc", "DB file '%s' has been created..." % (self.__db_file_name))
        return True

    #stringify
    def __stringify_eta_prongs(self, eta=[], prongs=[]):
        if not eta or not prongs:
            return "None", "None"

        eta_str = ("%.1f_%.1f" % (eta[0], eta[1])).replace(".", "p") if len(eta) == 2 else "Incl"
        prongs_str = ''.join(str(p) for p in prongs)

        return eta_str, prongs_str

    def __subtract_reals(self, channel="", SR="", CR="", prongs=[], eta=[], ID="", observable=""):
        data_histo = self.__tau_histo_loader.tau_holder.find(flavor="data",
                                                             channel=channel,
                                                             SR=SR,
                                                             CR=CR,
                                                             definition=self.__tau_definition,
                                                             origin="Incl",
                                                             prongs=prongs,
                                                             eta=eta,
                                                             ID=ID,
                                                             lepton="Incl",
                                                             observable=observable)
        real_mc_histo = self.__tau_histo_loader.tau_holder.find(flavor="mc",
                                                                channel=channel,
                                                                SR=SR,
                                                                CR=CR,
                                                                definition=self.__tau_definition,
                                                                origin=self.__subtract_mc,
                                                                prongs=prongs,
                                                                eta=eta,
                                                                ID=ID,
                                                                lepton="Real",
                                                                observable=observable)
        if not real_mc_histo:
            raise NameError("Failed to load MC histogram")
        elif not data_histo:
            raise NameError("Failed to load data histogram")
        data_histo.histo.Add(real_mc_histo.histo, -1)
        return data_histo

    # F calculation
    def __calculate_F(self, variation="", channel="", SR="", CR="", prongs=[], eta=[]):

        #stringify eta and prongs
        eta_str, prongs_str = self.__stringify_eta_prongs(eta, prongs)

        F_histo_dt_p = self.__subtract_reals(channel=channel, SR=SR, CR=CR, prongs=prongs, eta=eta, ID="pass", observable=self.__observable)
        F_histo_dt_f = self.__subtract_reals(channel=channel, SR=SR, CR=CR, prongs=prongs, eta=eta, ID="fail", observable=self.__observable)

        nom = F_histo_dt_p.histo.GetHistogram()
        den = F_histo_dt_f.histo.GetHistogram()
        if self.__options.dynamic_rebinning:
            self.__msg.info("TauFakeFactorsCalc",
                            "__calculate_F rebin %s %s %s %s %s %s " % (variation, channel, SR, CR, prongs_str, eta_str))
            ### Define a fixed eta binning
            if self.__observable == "TauPtAbsEta":
                bins_y_list = []
                if channel == "Muo" and prongs == [3] and channel != "1jet":
                    bins_y_list = [0., 0.1, 1.4, 1.5, 1.75, 2., 2.25, 2.5]
                elif channel == "Muo" and prongs == [3]:
                    bins_y_list = [0., 0.1, 1.4, 1.5, 2.5]
                elif channel == "Muo" and prongs == [1]:
                    bins_y_list = [0., 0.1, 1.4, 1.5, 2., 2.25, 2.5]
                elif channel == "Ele":
                    bins_y_list = [0., 1.4, 1.5, 2., 2.25, 2.5]
                else:
                    raise RuntimeError("Unkown channel %s" % (channel))
                bins_y = convertDoubleVector(bins_y_list)
                rebinner = ROOT.XAMPP.HistogramRebinning(F_histo_dt_p.histo.GetHistogram())
                rebinned_h = rebinner.rebin_x(self.__options.aim_stat_error)
                bins_x = F_histo_dt_p.histo.ExtractBinningFromAxis(rebinned_h.GetXaxis())
                n_x = 1 * bins_x.size() - 1
                n_y = 1 * bins_y.size() - 1
                nom_binning = ROOT.TH2D(id_generator(20), "Stonjek", n_x, bins_x.data(), n_y, bins_y.data())
                nom = F_histo_dt_p.histo.TransformToBinning(nom_binning)

            else:
                nom = F_histo_dt_p.histo.ReBin(self.__options.aim_stat_error)

            den = F_histo_dt_f.histo.TransformToBinning(nom)

        F = nom.Clone(id_generator(23))
        if not F.Divide(den.get()):
            self.__msg.fatal("TauFakeFactorCalc", "__calculate_F - Failed to calculate F")

        #store findings in a db
        if self.__db_create_report and self.__db_file_ready:
            buf = "F: %s %s %s, Variation=%s, Prongs=%s, Eta=%s, Data pass=%f, fail=%s, MC pass=%f, fail=%f\n"
            self.__db_file.write(buf % (channel, SR, CR, variation, prongs_str, eta_str, F_histo_dt_p.histo.Integral(),
                                        F_histo_dt_f.histo.Integral(), F_histo_mc_p.histo.Integral(), F_histo_mc_f.histo.Integral()))
        self.__tau_store.store(F, TS.TauName("F", "combined", variation, info=F_histo_dt_f.info))

        return F

    # R calculation
    def __calculate_R(self, variation="", channel="", SR="", CR="", prongs=[], eta=[]):
        ### Calculate the R-factor by a simultaenous fit of the tau-width
        ### in W+jets data, QCD data to the width in SR data
        R_histo_dt_Wjets_f = self.__subtract_reals(channel=channel,
                                                   SR=SR,
                                                   CR="Wjets",
                                                   prongs=prongs,
                                                   eta=eta,
                                                   ID="fail",
                                                   observable=self.__observable_rfactor)
        R_histo_dt_QCD_f = self.__subtract_reals(
            channel=channel,
            SR=SR,
            #CR="QCD",
            CR="AntiIsoSS",
            prongs=prongs,
            eta=eta,
            ID="fail",
            observable=self.__observable_rfactor)
        R_histo_dt_SR_f = self.__subtract_reals(channel=channel,
                                                SR=SR,
                                                CR="NotApplied",
                                                prongs=prongs,
                                                eta=eta,
                                                ID="fail",
                                                observable=self.__observable_rfactor)

        if R_histo_dt_SR_f.histo.GetDimension() == 2:
            nom_binning = R_histo_dt_SR_f.histo.GetHistogram().get()
            if self.__options.dynamic_rebinning:
                #self.__msg.info("TauFakeFactorsCalc",
                #            "__calculate_R rebin %s %s %s %s %s %s " % (variation, channel, SR, CR, prongs_str, eta_str))
                ### It's a first trial to let the binning float
                rebinner = ROOT.XAMPP.HistogramRebinning(R_histo_dt_SR_f.histo.GetHistogram())
                rebinner_qcd = ROOT.XAMPP.HistogramRebinning(R_histo_dt_QCD_f.histo.GetHistogram())
                rebinner_wjets = ROOT.XAMPP.HistogramRebinning(R_histo_dt_Wjets_f.histo.GetHistogram())

                rebinned_sr = rebinner.rebin_x(self.__options.aim_stat_error)
                rebinned_qcd = rebinner_qcd.rebin_x(self.__options.aim_stat_error)
                rebinned_wjets = rebinner_wjets.rebin_x(self.__options.aim_stat_error)
                bins_x_sr = R_histo_dt_SR_f.histo.ExtractBinningFromAxis(rebinned_sr.GetXaxis())
                bins_x_qcd = R_histo_dt_SR_f.histo.ExtractBinningFromAxis(rebinned_qcd.GetXaxis())
                bins_x_wjets = R_histo_dt_SR_f.histo.ExtractBinningFromAxis(rebinned_wjets.GetXaxis())

                bins_y = R_histo_dt_SR_f.histo.ExtractBinningFromAxis(rebinned_sr.GetYaxis())

                bins_x = convertDoubleVector(
                    sorted(
                        ClearFromDuplicates([x for x in bins_x_sr if x in bins_x_qcd and x in bins_x_wjets] +
                                            [x for x in bins_x_qcd if x in bins_x_sr and x in bins_x_wjets] +
                                            [x for x in bins_x_wjets if x in bins_x_qcd and x in bins_x_sr])))
                # ~ bins_x = bins_x_sr
                nom_binning = ROOT.TH2D(id_generator(20), "rebinned", 1 * bins_x.size() - 1, bins_x.data(), 1 * bins_y.size() - 1,
                                        bins_y.data())
                nom_binning.GetXaxis().SetTitle(R_histo_dt_SR_f.histo.GetXaxis().GetTitle())
                nom_binning.GetYaxis().SetTitle(R_histo_dt_SR_f.histo.GetYaxis().GetTitle())
            fit_slicer = ROOT.XAMPP.FractionSlicer(R_histo_dt_SR_f.histo, R_histo_dt_QCD_f.histo, R_histo_dt_Wjets_f.histo, 0)
            fit_slicer.fit(nom_binning)
            CreateDirectory("PostFits/", False)
            ts = TS.TauName("R", "combined", variation, info=R_histo_dt_QCD_f.info, alt_obs_name="TauPt")

            dummy_canvas = ROOT.TCanvas("dummy", "dummy", 600, 600)
            dummy_canvas.SaveAs("PostFits/All_%s.pdf[" % (ts.name))
            for fit_bin, fit in enumerate(fit_slicer.get_fits(), 1):
                if not fit.is_valid(): continue

                pu = PlotUtils(lumi=R_histo_dt_SR_f.histo.GetLumi())
                pu.Prepare2PadCanvas(id_generator(15))
                #pu.GetTopPad().SetLogy()
                pu.GetTopPad().cd()
                frame = fit.get_frame()
                frame.SetMaximum(frame.GetMaximum() * 1.5)
                frame.SetMinimum(1)
                frame.Draw()
                pu.CreateLegend(0.7, 0.75, 0.9, 0.9)
                pu.GetLegend().AddEntry("Data", "Data", "P")
                pu.GetLegend().AddEntry("Model", "GJ + QJ", "L")
                pu.GetLegend().AddEntry("f1", "GJ", "L")
                pu.DrawLegend()

                BinLowEdge_x = nom_binning.GetXaxis().GetBinLowEdge(fit_bin)
                BinUpEdge_x = nom_binning.GetXaxis().GetBinUpEdge(fit_bin)
                pu.DrawTLatex(0.2, 0.85, "#chi2/nDoF: %.2f (%i)" % (fit.chi2() / fit.nBins(), fit.nBins()))
                pu.DrawTLatex(0.2, 0.8, "%.2f<%s<%.2f bin(%d)" % (BinLowEdge_x, nom_binning.GetXaxis().GetTitle(), BinUpEdge_x, fit_bin))
                y_pos = 0.73
                x_pos = 0.55
                pu.DrawTLatex(x_pos, y_pos, "GJ fraction (%.2f #pm %.2f)%%" % (fit.fraction_f1() * 100., fit.fraction_f1_error() * 100.))
                y_pos -= 0.05
                pu.DrawTLatex(x_pos, y_pos, "Data integral: %.0f #pm %.0f" % (fit.integral(), math.sqrt(fit.integral())))

                pu.DrawAtlas(0.38, 0.925)
                pu.DrawLumiSqrtS(0.15, 0.925)
                ### Bottom panel
                pu.GetBottomPad().cd()
                ratio = fit.data().Clone(id_generator(24))
                ratio.Divide(fit.predicted().get())
                ratio.GetXaxis().SetTitle(nom_binning.GetYaxis().GetTitle())
                ymin, ymax = pu.GetFancyAxisRanges([ratio], self.__options, doRatio=True)
                pu.drawRatioStyling(ratio, 0.9, 1.1, "Data/Fit")
                ratio.Draw("sameE2")
                pu.saveHisto("PostFits/" + ts.name + "_" + str(fit_bin), ["pdf"])
                pu.GetCanvas().SaveAs("PostFits/All_%s.pdf" % (ts.name))

            dummy_canvas.SaveAs("PostFits/All_%s.pdf]" % (ts.name))
            #### Assume slicing x-axis pt and y axis Width
            R = fit_slicer.get_result().Clone(id_generator(100))
            #store histo output
            self.__tau_store.store(R, ts)
            return R
        CR_denominator = "NotApplied"

        R_histo_dt_CR_f = self.__subtract_reals(channel=channel,
                                                SR=SR,
                                                CR=CR,
                                                prongs=prongs,
                                                eta=eta,
                                                ID="fail",
                                                observable=self.__observable_rfactor)
        R_histo_dt_SR_f = self.__subtract_reals(channel=channel,
                                                SR=SR,
                                                CR=CR_denominator,
                                                prongs=prongs,
                                                eta=eta,
                                                ID="fail",
                                                observable=self.__observable_rfactor)

        #stringify eta and prongs
        eta_str, prongs_str = self.__stringify_eta_prongs(eta, prongs)

        #db storage
        if self.__db_create_report and self.__db_file_ready:
            buf = "R: %s %s %s, Tau ID Fail, Variation=%s, Prongs=%s, Eta=%s. Data CR=%f, SR=%s. MC CR=%f, SR=%f\n"
            self.__db_file.write(
                buf % (channel, SR, CR, variation, prongs_str, eta_str, R_histo_dt_CR_f.histo.Integral(), R_histo_dt_SR_f.histo.Integral(),
                       R_histo_mc_CR_f.histo.Integral(), R_histo_mc_SR_f.histo.Integral()))

        nom = R_histo_dt_CR_f.histo.GetHistogram()
        den = R_histo_dt_SR_f.histo.GetHistogram()
        if self.__options.dynamic_rebinning:
            self.__msg.info("TauFakeFactorsCalc",
                            "__calculate_R rebin %s %s %s %s %s %s " % (variation, channel, SR, CR, prongs_str, eta_str))

            nom = R_histo_dt_CR_f.histo.ReBin(self.__options.aim_stat_error)
            den = R_histo_dt_SR_f.histo.TransformToBinning(nom)

        R = nom.Clone(id_generator(23))
        if not R.Divide(den.get()):
            self.__msg.fatal("TauFakeFactorCalc", "__calculate_F - Failed to calculate F")

        #store histo output
        self.__tau_store.store(R, TS.TauName("R", "combined", variation, info=R_histo_dt_CR_f.info))

        return R

    def __complementary_R(self, variation="", channel="", SR="", CR="", prongs=[], eta=[], R_input=None):

        R_histo_dt_CR_f = self.__subtract_reals(channel=channel,
                                                SR=SR,
                                                CR=CR,
                                                prongs=prongs,
                                                eta=eta,
                                                ID="fail",
                                                observable=self.__observable_rfactor)
        R = TH.Complementary(R_input)
        #store histo output
        self.__tau_store.store(R, TS.TauName("R", "combined", variation, info=R_histo_dt_CR_f.info, alt_obs_name="TauPt"))

        return R

    # I calculation
    def __I_setup(self):
        return "PreSel", "IsoSS", "AntiIsoSS"

    def __calculate_I(self, variation="", channel="", CR="", prongs=[], eta=[]):

        SR, CR_iso, CR_anti_iso = self.__I_setup()

        I_histo_dt_iso = self.__tau_histo_loader.tau_holder.find(flavor="data",
                                                                 channel=channel,
                                                                 SR=SR,
                                                                 CR=CR_iso,
                                                                 definition=self.__tau_definition,
                                                                 origin="Incl",
                                                                 prongs=prongs,
                                                                 eta=eta,
                                                                 ID="pass",
                                                                 lepton="Incl",
                                                                 observable=self.__observable_iso)

        I_histo_dt_anti_iso = self.__tau_histo_loader.tau_holder.find(flavor="data",
                                                                      channel=channel,
                                                                      SR=SR,
                                                                      CR=CR_anti_iso,
                                                                      definition=self.__tau_definition,
                                                                      origin="Incl",
                                                                      prongs=prongs,
                                                                      eta=eta,
                                                                      ID="pass",
                                                                      lepton="Incl",
                                                                      observable=self.__observable_iso)

        I_histo_mc_iso = self.__tau_histo_loader.tau_holder.find(flavor="mc",
                                                                 channel=channel,
                                                                 SR=SR,
                                                                 CR=CR_iso,
                                                                 definition=self.__tau_definition,
                                                                 origin=self.__subtract_mc,
                                                                 prongs=prongs,
                                                                 eta=eta,
                                                                 ID="pass",
                                                                 lepton="IsoReal",
                                                                 observable=self.__observable_iso)

        I_histo_mc_anti_iso = self.__tau_histo_loader.tau_holder.find(flavor="mc",
                                                                      channel=channel,
                                                                      SR=SR,
                                                                      CR=CR_anti_iso,
                                                                      definition=self.__tau_definition,
                                                                      origin=self.__subtract_mc,
                                                                      prongs=prongs,
                                                                      eta=eta,
                                                                      ID="pass",
                                                                      lepton="IsoReal",
                                                                      observable=self.__observable_iso)

        #stringify eta and prongs
        eta_str, prongs_str = self.__stringify_eta_prongs(eta, prongs)

        #db storage
        if self.__db_create_report and self.__db_file_ready:
            buf = "I: Channel=%s, TauID=pass, CR=%s, Variation=%s, Prongs=%s, Eta=%s, Data iso=%f anti-iso=%f, MC Data iso=%f anti-iso=%f\n"
            self.__db_file.write(buf % (channel, variation, CR, prongs_str, eta_str, I_histo_dt_iso.Integral(),
                                        I_histo_dt_anti_iso.Integral(), I_histo_mc_iso.Integral(), I_histo_mc_anti_iso.Integral()))

        #store histo output
        self.__tau_store.aux_store(I_histo_dt_iso.histo.GetHistogram(), TS.TauName("I", "dat_pass_iso", variation,
                                                                                   info=I_histo_dt_iso.info))
        self.__tau_store.aux_store(I_histo_mc_iso.histo.GetHistogram(), TS.TauName("I", "sim_pass_iso", variation,
                                                                                   info=I_histo_mc_iso.info))
        self.__tau_store.aux_store(I_histo_dt_anti_iso.histo.GetHistogram(),
                                   TS.TauName("I", "dat_pass_anti_iso", variation, info=I_histo_dt_anti_iso.info))
        self.__tau_store.aux_store(I_histo_mc_anti_iso.histo.GetHistogram(),
                                   TS.TauName("I", "sim_pass_anti_iso", variation, info=I_histo_mc_anti_iso.info))

        if not I_histo_dt_iso.histo.Add(I_histo_mc_iso.histo, -1) or not I_histo_dt_anti_iso.histo.Add(I_histo_mc_anti_iso.histo, -1):
            self.__msg.fatal("TauFakeFactorCalc", "__calculate_I - Failed to subtract MC from data")

        nom = I_histo_dt_iso.histo.GetHistogram()
        den = I_histo_dt_anti_iso.histo.GetHistogram()
        if self.__options.dynamic_rebinning:
            nom = I_histo_dt_iso.histo.ReBin(self.__options.aim_stat_error)
            den = I_histo_dt_anti_iso.histo.TransformToBinning(nom)

        I = nom.Clone(id_generator(23))
        if not I.Divide(den.get()):
            self.__msg.fatal("TauFakeFactorCalc", "__calculate_I - Failed to calculate I")

        self.__tau_store.store(I, TS.TauName("I", "combined", variation, info=I_histo_mc_anti_iso.info))
        return I

    def __unit_I(self, variation="", channel="", CR="", prongs=[], eta=[], input_histo=None):

        SR, CR_iso, CR_anti_iso = self.__I_setup()
        input_histo = self.__tau_histo_loader.tau_holder.find(flavor="mc",
                                                              channel=channel,
                                                              SR=SR,
                                                              CR=CR_iso,
                                                              definition=self.__tau_definition,
                                                              origin="Incl",
                                                              prongs=prongs,
                                                              eta=eta,
                                                              ID="pass",
                                                              lepton="Incl",
                                                              observable=self.__observable_iso).histo

        I = TH.Unity(input_histo)

        #stringify eta and prongs
        eta_str, prongs_str = self.__stringify_eta_prongs(eta, prongs)

        #db storage
        if self.__db_create_report and self.__db_file_ready:
            buf = "I: %s %s %s, Tau ID=pass, Variation=%s, Prongs=%s, Eta=%s, I=%f\n"
            self.__db_file.write(buf % (channel, SR, CR, variation, prongs_str, eta_str, I.Integral()))

        #store histo output
        self.__tau_store.aux_store(I, TS.TauName("I", "combined", variation, channel, CR, SR, eta_str, prongs_str, self.__observable_iso))

        return I

    # FF calculation
    def __calculate_FF_F_R_I(self,
                             variation="",
                             channel="",
                             SR="",
                             CR="",
                             prongs=[],
                             eta=[],
                             R_input=None,
                             calculate_I=True,
                             calculate_R=False):

        F = self.__calculate_F(variation=variation, channel=channel, SR=SR, CR=CR, prongs=prongs, eta=eta)

        if calculate_I:
            I = self.__calculate_I(variation=variation, channel=channel, CR=CR, prongs=prongs, eta=eta)
        else:
            I = None  #self.__unit_I(variation=variation, channel=channel, CR=CR, prongs=prongs, eta=eta)

        if not R_input:
            R = self.__calculate_R(variation=variation, channel=channel, SR=SR, CR=CR, prongs=prongs, eta=eta)
        else:
            R = self.__complementary_R(variation=variation, channel=channel, SR=SR, CR=CR, prongs=prongs, eta=eta, R_input=R_input)

        #calculate FF
        #FF = TH.Multiply([F, R, I])

        #stringify eta and prongs
        eta_str, prongs_str = self.__stringify_eta_prongs(eta, prongs)

        #store
        #self.__tau_store.aux_store(FF, TS.TauName("FF", "combined", variation, channel, CR, SR, eta_str, prongs_str, self.__observable))

        #if self.__db_create_report and self.__db_file_ready:
        #    buf = "FF: %s %s %s, Variation=%s, Prongs=%s, Eta=%s. Integral=%f\n"
        #   self.__db_file.write(buf % (channel, SR, CR, variation, prongs_str, eta_str, FF.Integral()))

        return F, R, I

    #combined FF
    def __combined_FF(self, variation="", F1=None, F2=None, channel="", SR="", prongs=[], eta=[]):

        #calculate combined FF
        FF = TH.Sum(F1, F2)
        FF = self.__finalize_ff_histo(ff_histo=FF, abs_truncate=False)

        #stringify eta and prongs
        eta_str, prongs_str = self.__stringify_eta_prongs(eta, prongs)

        #store
        self.__tau_store.store(FF, TS.TauName("FF", "combined", variation, channel, "NotApplied", SR, eta_str, prongs_str,
                                              self.__observable))

        #if nominal calculate and store stat uncertainties
        if variation == self.__nominal_name:
            FF_stat_up, FF_stat_down = TH.StatHistos(FF)
            FF_stat_up = self.__finalize_ff_histo(ff_histo=FF_stat_up, abs_truncate=True)
            FF_stat_down = self.__finalize_ff_histo(ff_histo=FF_stat_down, abs_truncate=True)
            self.__tau_store.store(
                FF_stat_up,
                TS.TauName("FF", "combined", self.__stat_up_name, channel, "NotApplied", SR, eta_str, prongs_str, self.__observable))
            self.__tau_store.store(
                FF_stat_down,
                TS.TauName("FF", "combined", self.__stat_down_name, channel, "NotApplied", SR, eta_str, prongs_str, self.__observable))

        #db
        if self.__db_create_report and self.__db_file_ready:
            buf = "FF: %s %s, Variation=%s, Prongs=%s, Eta=%s, Integral=%f\n"
            self.__db_file.write(buf % (channel, SR, variation, prongs_str, eta_str, FF.Integral()))

        return FF

    #combined FF
    def __flat_FF(self, variation="", channel="", SR="", CR="", prongs=[], eta=[]):

        #identify template
        F = self.__calculate_F(variation=variation, channel=channel, SR=SR, CR=CR, prongs=prongs, eta=eta)

        #stringify eta and prongs
        eta_str, prongs_str = self.__stringify_eta_prongs(eta, prongs)

        #identify flat factor
        factor = -1.
        for tf in self.__flat_ff:
            if tf.prongs == prongs and tf.eta == eta:
                factor = tf.ff
                break

        #check result
        if factor < 0.:
            self.__msg.warning("TauFakeFactorsCalc", "No flat factor identified for %s %s ..." % (eta_str, prongs_str))

        #calculate combined FF
        FF = TH.SetTo(F, factor)

        #store
        self.__tau_store.store(FF, TS.TauName("FF", "combined", variation, channel, "NotApplied", SR, eta_str, prongs_str,
                                              self.__observable))

        return FF

    def __calculate_core(self, variation="", channel="", SR="", prongs=[], eta=[]):

        #force flattened FFs
        if self.__options.flat_ff:
            FF = self.__flat_FF(variation=variation, channel=channel, SR=SR, CR="QCD", prongs=prongs, eta=eta)

            return True

        # F, R  QCD
        F_QCD, R_QCD, I_QCD = self.__calculate_FF_F_R_I(variation=variation,
                                                        channel=channel,
                                                        SR=SR,
                                                        CR="AntiIsoSS",
                                                        prongs=prongs,
                                                        eta=eta,
                                                        R_input=None,
                                                        calculate_I=False)

        # F, R  Wjets
        F_Wjets, R_Wjets, I_Wjets = self.__calculate_FF_F_R_I(variation=variation,
                                                              channel=channel,
                                                              SR=SR,
                                                              CR="Wjets",
                                                              prongs=prongs,
                                                              eta=eta,
                                                              R_input=R_QCD,
                                                              calculate_I=False)

        #combined FF
        #FF = self.__combined_FF(variation=variation, F1=TH.Zero(F_Wjets), F2=F_Wjets, channel=channel, SR=SR, prongs=prongs, eta=eta)

        return True

    def __checks(self):

        if not self.__tau_histo_loader.ready:
            self.__msg.warning("TauFakeFactorsCalc", "No file structure detected...")
            return False

        if not self.__tau_histo_loader.tau_holder_size:
            self.__msg.warning("TauFakeFactorsCalc", "No histograms collected by TauHolder in TauHistoLoader...")
            return False
        else:
            self.__msg.info(
                "TauFakeFactorsCalc", "%i histograms collected by TauHolder in TauHistoLoader, proceeding with the FF estimate..." %
                (self.__tau_histo_loader.tau_holder_size))

        return True

    def __calculate(self):

        if not self.__checks():
            self.__msg.warning("TauFakeFactorsCalc", "Cannot proceed with the FF estimate...")

        variations = [self.__nominal_name]

        channels = ["Ele", "Muo"]

        SRs = ["PreSel", "0jet", "1jet"]
        prongs = [[1], [3]]
        #eta_regions = [[0.0, 0.8], [0.8, 2.5]]
        eta_regions = [[0., 2.5]]
        #channels = ["MuTau"]
        #prongs = [ [1] ]
        #eta_regions = [ [0.0, 0.8] ]

        for v in variations:
            for c in channels:
                for s in SRs:
                    for p in prongs:
                        for e in eta_regions:
                            if not self.__calculate_core(v, c, s, p, e):
                                self.__msg.error("TauFakeFactorsCalc", "Unable to calculate FF!")
                                return False

        return True

    def __finalize(self):

        if not self.__calculate_ff:
            self.__msg.error("TauFakeFactorsCalc", "Problem in calculating FFs, cannot finalized job...")
            return False

        if not self.__tau_store.finalize():
            self.__msg.error("TauFakeFactorsCalc", "Unable to correctly close the final output file...")
            return False

        if self.__db_create_report:
            if self.__db_file_ready:
                self.__msg.info("TauFakeFactorsCalc", "FF DB file stored at '{}'".format(self.__db_file_name))
            else:
                self.__msg.error("TauFakeFactorsCalc", "Unable to correctly close dB file for FF results...")
            return False

        self.__msg.info("TauFakeFactorsCalc", "Done!")

        return True
