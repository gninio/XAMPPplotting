import ROOT, math
import XAMPPplotting.FileStructureHandler
from XAMPPplotting import TauLogger as TL
from XAMPPplotting.Utils import GetNbins, PrintHistogram, AddHistos, CreateCloneAndAdd, CreateCulumativeHisto, SameAxisBinning
from ClusterSubmission.Utils import id_generator
msg = TL.msg(2)


#get file structure
def FileStructure(Options):

    FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(Options)

    if not FileStructure:
        msg.error("FileStructure", "Unable to obtain FileStructure ...")

    if not issubclass(type(FileStructure), XAMPPplotting.FileStructureHandler.FileStructHandler):
        msg.fatal("FileStructure", "Error: Object is not FileStructure but '{}'...".format(type(self.file_structure)))

    return FileStructure


def IsTH1(obj=None):
    return IsRootTH1(obj) and IsRootObj(obj)


def IsRootTH1(obj=None):
    if not obj:
        msg.error("IsHisto", "Null input")
        print obj
        return None

    return obj.InheritsFrom("TH1")


def IsRootObj(obj=None):
    if not obj:
        msg.error("IsRootObj", "Null input")
        print obj
        return None

    #works for <ROOT.TH1D object at 0x3855660>
    #and for <ROOT.shared_ptr<TH1> object at 0x150ac8b0>
    return issubclass(type(obj), ROOT.TObject) or issubclass(type(obj.get()), ROOT.TObject)


def Multiply(histos=[]):

    if not histos:
        msg.error("Multiply", "Nothing to multiply")
        return None

    if len(histos) < 2:
        msg.warning("Multiply", "Nothing to multiply with just one input...")
        return histos[0]

    if not IsTH1(histos[0]):
        msg.error("Multiply", "First input is not TH1 ... ")
        return None

    h = histos[0].Clone(id_generator(23))

    for index, histo in enumerate(histos[1:], 0):
        if not IsTH1(histo):
            msg.error("Multiply", "Input {} is not TH1 ... Skipping.".format(index))
            continue

        h.Multiply(histo)

    return h


def Sum(h1=None, h2=None):

    if not IsTH1(h1):
        msg.error("Sum", "Input 1 is not TH1")
        return None

    if not IsTH1(h2):
        msg.error("Sum", "Input 2 is not TH1")
        return None

    h = h1.Clone(id_generator(23))
    h.Add(h2, 1)
    return h


def Subtract(h1=None, h2=None):

    if not IsTH1(h1):
        msg.error("Subtract", "1st Input is not TH1")
        return None

    if not IsTH1(h2):
        msg.error("Subtract", "2nd Input is not TH1")
        return None
    h = h1.Clone(id_generator(23))
    h.Add(h1, h2, -1)
    return h


def Divide(h1=None, h2=None, option=""):

    if not IsTH1(h1):
        msg.error("Divide", "Input 1 is not TH1")
        return None

    if not IsTH1(h2):
        msg.error("Divide", "Input 2 is not TH1")
        return None

    h = h1.Clone(id_generator(23))

    if not option:
        if not h.Divide(h2):
            msg.error("Divide", "Could not perform division")
    else:
        if not h.Divide(h1, h2, 1., 1., option):
            msg.error("Divide", "Could not perform division with option '{}'".format(option))
    return h


def Truncate(h, abs_truncate=False):
    if not IsTH1(h):
        msg.error("Truncate()", "Nothing has been given")
        exit(1)
    for i in range(1, GetNbins(h)):
        if h.GetBinContent(i) >= 0.: continue
        msg.warning("Truncate", "%s has negative content %f \pm %f in bin %d" % (h.GetName(), h.GetBinContent(i), h.GetBinError(i), i))
        cont_plus_err = h.GetBinContent(i) + h.GetBinError(i)
        if not abs_truncate and cont_plus_err > 0.:
            new_content = math.fabs(h.GetBinContent(i)) if cont_plus_err >= math.fabs(h.GetBinContent(i)) else cont_plus_err
            ### Move the bin to the cost of an extra uncertainty
            extra_sys = (new_content - h.GetBinContent(i)) / 2.
            new_error = math.sqrt(h.GetBinError(i) * h.GetBinError(i) + extra_sys * extra_sys)
            h.SetBinContent(i, new_content)
            h.SetBinError(i, new_error)
            msg.info("Truncate", "Will move the scale-factors within the uncertainties to %f \pm %f" % (new_content, new_error))
        else:
            msg.info("Truncate", "Will set it to 0 \pm 1.")
            h.SetBinContent(i, 0.)
            h.SetBinError(i, 1.)


def PrintInfo(objs=[]):

    for c, obj in enumerate(objs):
        if not obj:
            msg.debug("PrintInfo", "Input %i is null..." % (c))
            continue
        else:
            msg.debug("PrintInfo", "Input %i is not null. OK." % (c))

        if not IsTH1(obj):
            msg.debug("PrintInfo", "Input %i is not a TH1..." % (c))
            continue
        else:
            msg.debug("PrintInfo", "Input %i is a TH1. OK." % (c))

        msg.info("PrintInfo", "TH1 input %i integral %d..." % (c, obj.Integral()))


def SubtractDivide(h1=None, h2=None, h3=None, h4=None, option="", verbose=False):

    if verbose:
        PrintInfo([h1, h2, h3, h4])

    return Divide(Subtract(h1, h2), Subtract(h3, h4), option)


def SubtractEfficiency(h1=None, h2=None, h3=None, h4=None):

    henum = Subtract(h1, h2)
    hdenom = Subtract(h3, h4)

    return ROOT.TEfficiency(henum, hdenom)


def Complementary(h=None):

    if not IsTH1(h):
        msg.error("Complementary", "Input object is not TH1")
        return None

    g = h.Clone(id_generator(23))
    for i in xrange(1, GetNbins(h)):
        g.SetBinContent(i, max(0, 1. - h.GetBinContent(i)))
        g.SetBinError(i, h.GetBinError(i))

    return g


def Zero(h=None):
    if not IsTH1(h):
        msg.error("Unity", "Input object is not TH1")
        return None

    g = h.Clone(id_generator(23))
    g.Reset()
    return g


def Unity(h=None):

    if not IsTH1(h):
        msg.error("Unity", "Input object is not TH1")
        return None

    g = h.Clone(id_generator(23))

    for i in xrange(1, GetNbins(h)):
        g.SetBinContent(i, 1.)
        g.SetBinError(i, 0.)

    return g


def StatHistos(h=None):

    if not IsTH1(h):
        msg.error("StatHistos", "Input object is not TH1")
        return None

    hup = h.Clone(id_generator(23))
    hdo = h.Clone(id_generator(23))

    for i in xrange(1, GetNbins(h)):
        hup.SetBinContent(i, h.GetBinContent(i) + h.GetBinError(i))
        hdo.SetBinContent(i, h.GetBinContent(i) - h.GetBinError(i))

    return hup, hdo


def SetTo(h=None, x=0.):

    if not IsTH1(h):
        msg.error("SetTo", "Input object is not TH1")
        return None

    g = h.Clone(id_generator(23))

    for i in xrange(1, GetNbins(h)):
        g.SetBinContent(i, x)
        g.SetBinError(i, 0.)

    return g
