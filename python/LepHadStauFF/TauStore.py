import ROOT
from XAMPPplotting import TauLogger as TL
from XAMPPplotting import TauHelper as TH
from XAMPPplotting.Utils import extendAxis


class TauName(object):
    def __init__(self, measurement="", case="", systematic="", info=None, alt_obs_name=""):
        self.__measurement = measurement
        self.__case = case
        self.__systematic = systematic
        self.__info = info
        #form name
        eta, prongs = self.__stringify_eta_prongs(eta=self.__info.eta_range, prongs=self.__info.prongs)
        self.__observable = self.__info.observable if len(alt_obs_name) == 0 else alt_obs_name
        self.__name = "%s_%s_%s_%s_TauDef_%s_CR_%s_SR_%s_Eta_%s_Prong_%s_Observable_%s" % (
            self.__measurement, self.__case, self.__systematic, self.__info.channel, self.__info.definition, self.__info.control_region,
            self.__info.signal_region, eta, prongs, self.__observable)

    @property
    def name(self):
        return self.__name

    @property
    def observable(self):
        return self.__observable

    def __stringify_eta_prongs(self, eta=[], prongs=[]):
        if not eta or not prongs:
            return "None", "None"

        eta_str = ("%.1f_%.1f" % (eta[0], eta[1])).replace(".", "p") if len(eta) == 2 else "Incl"
        prongs_str = ''.join(str(p) for p in prongs)

        return eta_str, prongs_str


class TauStore(object):
    def __init__(self, name="histo holder", filename="TauFF.root", auxfilename="TauAux.root", extend_axis=True):
        self.__filename = filename
        self.__aux_filename = auxfilename
        #files
        self.__fileoption = "RECREATE"
        self.__rootfile = self.__root_file()
        self.__aux_rootfile = self.__aux_root_file()
        #verbosity
        self.__msg = TL.msg(2)
        #stats
        self.__counter = 0
        self.__aux_counter = 0
        self.__cached_names = list([])
        self.__aux_cached_names = list([])
        self.__extend_axis = extend_axis

    def __root_file(self):
        return ROOT.TFile(self.__filename, self.__fileoption)

    def __aux_root_file(self):
        return ROOT.TFile(self.__aux_filename, self.__fileoption)

    def __is_tobject(self, obj):
        if issubclass(type(obj), ROOT.TObject) or issubclass(type(obj.get()), ROOT.TObject):
            return True

        self.__msg.error("TauStore", "Unable to write a non-TObject object...")
        print type(obj)

        return False

    def __is_th(self, obj):
        return "TH1" in obj.IsA().GetName() or "TH2" in obj.IsA().GetName()

    def __is_histogram(self, obj):
        return self.__is_tobject(obj) and self.__is_th()

    def __rename(self, obj, tau_name):
        obj.SetName(tau_name.name)
        return True

    def __finalize_ff_histo(self, ff_histo, abs_truncate=False, TauInfo=None):
        TH.Truncate(ff_histo, abs_truncate=abs_truncate)
        ### Always assume that the x-axis is the pt axis
        if self.__extend_axis and TauInfo.observable.lower().find("pt") != -1 and ff_histo.GetXaxis().GetBinUpEdge(
                ff_histo.GetNbinsX()) < 6.5e3:
            return extendAxis(in_histo=ff_histo, axis=0, high_bin=6.5e3, atEnd=True)
        return ff_histo

    def __store_core(self, obj=None, tau_name=None):

        #basic checks
        if not obj:
            self.__msg.error("TauStore", "Null object...")
            return False

        if not tau_name:
            self.__msg.error("TauStore", "Null tau name...")
            return False

        if not self.__is_tobject(obj):
            self.__msg.error("TauStore", "Unable to write a non-TObject object...")
            return False

        #give a proper name
        if not self.__rename(obj, tau_name):
            self.__msg.error("TauStore", "Unable to rename object...")
            return False
        return True

    def store(self, obj=None, tau_name=None):

        #check
        if not self.__store_core(obj=obj, tau_name=tau_name):
            self.__msg.error("TauStore", "Unable prepare object %s for storing...", self.__filename)

        obj = self.__finalize_ff_histo(ff_histo=obj, abs_truncate=False, TauInfo=tau_name)
        #store
        if not self.__rootfile.cd():
            self.__msg.error("TauStore", "Unable to change directory for file %s ...", self.__filename)
        else:
            if obj.GetName() not in self.__cached_names:
                obj.Write()
                self.__counter += 1
                self.__cached_names.append(obj.GetName())
            else:
                self.__msg.verbose("TauStore", "Object %s already stored..." % (obj.GetName()))

    def aux_store(self, obj=None, tau_name=None):

        #check
        if not self.__store_core(obj=obj, tau_name=tau_name):
            self.__msg.error("TauStore", "Unable prepare auxiliary object %s for storing..." % (self.__filename))

        obj = self.__finalize_ff_histo(ff_histo=obj, abs_truncate=False, TauInfo=tau_name)
        #store
        if not self.__aux_rootfile.cd():
            self.__msg.error("TauStore", "Unable to change directory  for auxiliary file %s ..." % (self.__aux_filename))
        else:
            if obj.GetName() not in self.__aux_cached_names:
                obj.Write()
                self.__aux_counter += 1
                self.__aux_cached_names.append(obj.GetName())
            else:
                self.__msg.verbose("TauStore", "Aux object %s already stored..." % (obj.GetName()))

    def finalize(self):
        self.__rootfile.Save()
        self.__rootfile.Close()
        self.__msg.info("TauStore", "%i object(s) are sucessfully stored in file '%s' ..." % (self.__counter, self.__filename))

        self.__aux_rootfile.Save()
        self.__aux_rootfile.Close()
        self.__msg.info("TauStore",
                        "%i auxiliary object(s) are sucessfully stored in file '%s' ..." % (self.__aux_counter, self.__aux_filename))

        return True
