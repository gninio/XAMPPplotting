#! /usr/bin/env python
import logging
from XAMPPplotting.Defs import *
from XAMPPplotting.Utils import getSamplesToUse

m_FileStructPtr = None


######################################################################################
# The DSConfigSet class extract the stucture of each DSconfig given to the code
# It contains the Analyses, Systematics, Regions and Variables stored in the input
######################################################################################
class DSConfigSet(object):
    def __init__(self, DSConfigModule, Options, use_data_ref=False):
        self.__name = DSConfigModule["Name"]
        self.__RefSample = DSConfigModule["RefSample"]
        self.__DSConfigs = DSConfigModule["Configs"]
        self.__noSyst = Options.noSyst
        self.__noSignal = Options.noSignal
        self.__ExcludeSyst = Options.ExcludeSyst
        self.__Nominal = Options.nominalName
        self.__FoundNominal = False
        self.__Systematics = []
        self.__analyses = {}
        self.__lumi = 0.
        self.__GetFileStructure(Options, use_data_ref=use_data_ref)
        self.__noSyst = self.__noSyst or GetSystPairer().get_systematics().empty()

    def __GetFileStructure(self, Options, use_data_ref=False):
        # Load the input files first
        Smp = None
        for sample in self.DSConfigs():
            if self.__RefSample != "":
                if sample.Name == self.__RefSample:
                    Smp = sample
            elif Smp is None and not use_data_ref and (sample.SampleType == SampleTypes.Irreducible or sample.SampleType
                                                       == SampleTypes.Signal or sample.SampleType == SampleTypes.Reducible):
                Smp = sample
            elif Smp is None and use_data_ref and sample.SampleType == SampleTypes.Data:
                Smp = sample
            if sample.SampleType == SampleTypes.Data and self.__lumi < sample.Lumi:
                self.__lumi = sample.Lumi
            for F in sample.Filepaths:
                GetFileHandler().LoadFile(F)
        if Smp == None:
            logging.error("No Monte-Carlo was given for %s. " % (self.GetName()))
            return

        for F in sorted(Smp.Filepaths):
            self.__ObtainAnalyses(F, Options)
            break
        ##sort all variables alphabetically
        for ana, regions in self.__analyses.iteritems():
            for Variables in regions.itervalues():
                Variables = sorted(Variables)

    def __UseSystematic(self, Syst, Options):
        if (Syst == "Nominal" and self.__Nominal == "") or (self.__Nominal != "" and Syst == self.__Nominal):
            self.__Nominal = Syst
            self.__FoundNominal = True
            return False
        return len(Options.systematics) == 0 or Syst in Options.systematics

    def __ObtainAnalyses(self, InputFile, Options):
        structure = GetFileHandler().GetFileStructure(InputFile)
        for a in structure.get_analyses():
            if a.name() not in self.__analyses.iterkeys():
                self.__analyses[a.name()] = {}
            self.__Systematics += [s for s in a.get_systematics() if s not in self.__Systematics]
            for region in a.get_region_names():
                if len(Options.regions) > 0 and region not in Options.regions:
                    continue
                if region not in self.GetAnalysisRegions(a.name()):
                    self.__analyses[a.name()][region] = []
                self.__analyses[a.name()][region] += [
                    var for var in a.get_region(region).variables()
                    if (len(Options.var) == 0 or var in Options.var) and var not in self.__analyses[a.name()][region]
                ]

    def DSConfigs(self):
        return self.__DSConfigs

    def GetName(self):
        return self.__name

    def noSyst(self):
        return self.__noSyst or len(self.Systematics()) <= 1

    def noSignal(self):
        return self.__noSignal

    def Nominal(self):
        return self.__Nominal

    def ExcludeSyst(self):
        return self.__ExcludeSyst

    def GetAnalyses(self):
        Analyses = []
        for ana in self.__analyses.iterkeys():
            Analyses.append(ana)
        return Analyses

    def Systematics(self):
        return self.__Systematics

    def GetAnalysisRegions(self, analysis):
        Regions = []
        if not analysis in self.GetAnalyses():
            logging.warning("The DSconfig set %s does not contain any analysis %s." % (self.GetName(), str(analysis)))
            return Regions
        for R in self.__analyses[analysis].iterkeys():
            if len(R) > 0:
                Regions.append(R)
        Regions.sort()
        return Regions

    def GetVariables(self, analysis, region):
        Variables = []
        if region not in self.GetAnalysisRegions(analysis):
            logging.warning("The analysis %s in DSConfig set %s does not contain the event region %s" %
                            (self.GetName(), str(analysis), str(region)))
            return Variables
        return self.__analyses[analysis][region]

    def GetLumi(self):
        return self.__lumi


class FileStructHandler(object):
    def __init__(self, Options, use_data_ref=False):
        self.__noSignal = Options.noSignal
        self.__ExcludeSyst = Options.ExcludeSyst
        self.__Nominal = Options.nominalName
        SamplesToUse = getSamplesToUse(Options.config)
        if len(SamplesToUse) == 0:
            logging.error("No valid DSConfig has been provided. Bail out")
            exit(1)
        self.__ConfigModules = []
        self.__CurrentModule = -1
        #Analyze the structrure of each DSConfigSet
        for module in SamplesToUse.itervalues():
            self.__ConfigModules.append(DSConfigSet(module, Options, use_data_ref=use_data_ref))
        ### only nominal has been found in the files
        if GetSystPairer().get_systematics().size() == 1 and not Options.noSyst:
            logging.warning(
                'GetFileStructure() -- Your histograms do not contain any systematic variations! In this case, please use the "--noSyst" option in the future!'
            )
            Options.noSyst = True

        else:
            GetSystPairer().Print()
        if Options.noSyst: GetSystPairer().do_systematics(False)
        self.__noSyst = Options.noSyst

    def Next(self):
        if self.__CurrentModule + 1 < len(self.__ConfigModules):
            self.__CurrentModule += 1
            return True
        self.__CurrentModule = -1
        return False

    def GetConfigSet(self, module=None):
        if not module and self.__CurrentModule != -1:
            return self.__ConfigModules[self.__CurrentModule]
        for Analyzer in self.__ConfigModules:
            if not module or Analyzer.GetName() == module:
                return Analyzer
        logging.warning("No DSConfig module has been given called " + module)
        return None


class FileStructurePointer(object):
    def __init__(self):
        self.__file_handler = ROOT.XAMPP.FileHandler.getInstance()
        self.__syst_pairer = ROOT.XAMPP.SystematicPairer.getInstance()
        self.__file_struct = None

    def __del__(self):
        self.__file_struct = None
        self.__syst_pairer = None
        self.__file_handler.reset()

    def file_handler(self):
        return self.__file_handler

    def file_structure(self, options, use_data_ref=False):
        if not self.__file_struct: self.__file_struct = FileStructHandler(options, use_data_ref=use_data_ref)
        return self.__file_struct

    def syst_pairer(self):
        return self.__syst_pairer


def GetFileHandler():
    global m_FileStructPtr
    if m_FileStructPtr: return m_FileStructPtr.file_handler()
    m_FileStructPtr = FileStructurePointer()
    return GetFileHandler()


def GetSystPairer():
    global m_FileStructPtr
    if m_FileStructPtr: return m_FileStructPtr.syst_pairer()
    m_FileStructPtr = FileStructurePointer()
    return GetSystPairer()


def GetStructure(Options=None, use_data_ref=False):
    global m_FileStructPtr
    if m_FileStructPtr: return m_FileStructPtr.file_structure(Options, use_data_ref=use_data_ref)
    m_FileStructPtr = FileStructurePointer()
    return GetStructure(Options, use_data_ref=use_data_ref)


def ClearServices():
    global m_FileStructPtr
    if m_FileStructPtr: m_FileStructPtr = None

    from XAMPPplotting.CalculateLumiFromIlumicalc import m_prw_tool
    m_prw_tool = None

    from XAMPPplotting.WriteValuesIntoTH1 import m_histo_templater
    if m_histo_templater: m_histo_templater = None

    from XAMPPplotting.CheckMetaData import m_NormBase
    if m_NormBase: m_NormBase.resetDataBase()
    m_NormBase = None
