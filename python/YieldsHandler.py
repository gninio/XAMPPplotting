#! /usr/bin/env python
##
## @brief Facilities to handle the histogram yields
## Supported outputs: raw columns, latex tables, root files
## Future outputs: text format for the WriteValuesIntoTH1.py script
##
import os, copy, ROOT


# #
# @class PointYield
#
class PointYield(object):
    def __init__(self, xleft=0., xright=0., y=0., error=0., variable_precision=0):
        self.xleft = xleft
        self.xright = xright
        self.y = y
        self.error = error
        self.variable_precision = variable_precision

    @property
    def latex_summary(self):
        pre_lat = "%.{}f & %.{}f & %f & %f \\\ \n".format(self.variable_precision, self.variable_precision)
        return pre_lat % (self.xleft, self.xright, self.y, self.error)

    @property
    def raw_summary(self):
        pre_raw = "%.{}f  %.{}f  %f  %f\n".format(self.variable_precision, self.variable_precision)
        return pre_raw % (self.xleft, self.xright, self.y, self.error)

    @property
    def hbins_summary(self):
        return "hi"


# #
# @class Yields
#
class Yields(object):
    def __init__(self, th1_histo=None):
        self.__histo = th1_histo
        #
        self.__quantity_position = -1  #quantity position: now in last position
        self.__name = ''
        self.__name_ready = self.__make_name()
        self.__name_tokens = self.__name.split("_") if self.__name_ready else []
        self.__yields_list = []
        self.__precision = self.__get_precision()
        self.__ready = self.__make_yields_list()

    @classmethod
    def deep_copy(cls, class_instance):
        histo = copy.deepcopy(class_instance.__histo)  # if deepcopy is necessary
        return cls(histo)

    @property
    def ready(self):
        return self.__ready

    @property
    def name(self):
        return self.__name

    @property
    def unique_name(self):
        return self.__name.replace("\n", "").replace(" ", "")

    @name.setter
    def name(self, value):
        self.__name = value

    def name_replace(self, x1='', x2=''):
        if self.__name_ready:
            prename = self.__name
            prehistoname = self.__histo.GetName()
            if x1 in self.__name:
                name = self.__name.replace(x1, x2)
                self.__name = name
                self.__histo.SetName(name)
                if self.__name == prename:
                    print "Error: rename request didn't work for yields name ", prename
                if self.__histo.GetName() == prehistoname:
                    print "Error: rename request didn't work for yields name ", prename

    @property
    def histo(self):
        return self.__histo

    def __diagnose_histo_file(self, f=None):
        if not self.__histo:
            print "Error: cannot write null histo '%s' ..." % (self.__name)
            return False

        if not f:
            print "Error: cannot handle a null root file..."
            return False

        if not f.cd():
            print "Warning: cannot change directory to root file ..."
            return False
        return True

    def write_histo(self, rfile=None):

        if not self.__diagnose_histo_file(rfile):
            print "Error: bad histo or root file ..."
            return False

        self.__histo.Write(self.__name)

        return True

    def __is_root_histogram(self, obj):
        return issubclass(type(obj), ROOT.TObject) and "TH" in obj.IsA().GetName()

    def __make_name(self):
        if self.__is_root_histogram(self.__histo):
            self.__name = self.__histo.GetName()
            return True
        else:
            print "Warning: Yield input of not a histogram type:", type(self.__histo)
        return False

    def __make_yields_list(self):

        if not self.__histo:
            print "Error: histo object is null :", self.__histo

        if not self.__is_root_histogram(self.__histo):
            print "Warning: passed object is not a ROOT histogram :", self.__histo
            return False

        for b in xrange(1, self.__histo.GetNbinsX() + 1):
            lowx = self.__histo.GetBinLowEdge(b)
            bw = self.__histo.GetBinWidth(b)
            highx = lowx + bw
            bc = self.__histo.GetBinContent(b)
            er = self.__histo.GetBinError(b)
            self.__yields_list.append(PointYield(lowx, highx, bc, er, self.__precision))

        return True

    def get_name(self):
        return self.__name

    def __get_variable(self):  #assumes that the variable is the last field of the histo name
        return self.get_name().replace(' ', '').replace('\n', '').split('_')[-1]

    def __get_precision(self):
        var = self.__get_variable()

        if var.endswith('eta') or var.endswith("phi"):
            return '2'

        return '0'

    def get_raw_lines(self):
        return ''.join(e.raw_summary for e in self.__yields_list)

    def get_latex_lines(self):
        return ''.join(e.latex_summary for e in self.__yields_list)

    def get_hbins_lines(self):
        return ''.join(e.hbins_summary for e in self.__yields_list)


##
# @class YieldsHandler
#
class YieldsHandler(object):
    """
    Handles list of Yield objects
    """
    def __init__(self, analysis="", outdir="./", outfile="", raw_yields=False, latex_yields=False, hbins_yields=False, histo_yields=False):
        self.__yields_outdir = outdir
        self.__yields_outfile = outfile
        self.__yields_analysis = analysis
        self.__store_raw_yields = raw_yields
        self.__store_latex_yields = latex_yields
        self.__store_hbins_yields = hbins_yields
        self.__store_histo_yields = histo_yields
        self.__store_any_yield = raw_yields or latex_yields or hbins_yields or histo_yields
        self.__store_text_yield = raw_yields or latex_yields or hbins_yields
        #
        self.__point_yields_list = []  # these are PointYields
        self.__yields_raw_file = self.__get_file("txt") if self.__store_raw_yields else None
        self.__yields_latex_file = self.__get_file("tex") if self.__store_latex_yields else None
        self.__yields_hbins_file = self.__get_file("conf") if self.__store_hbins_yields else None
        self.__yields_histo_file = self.__get_file("root") if self.__store_histo_yields else None

    def __del__(self):
        print "Info: Yields stored as:"
        if self.__store_raw_yields:
            print "\t{}".format(self.__yields_raw_file.name)

        if self.__store_latex_yields:
            print "\t{}".format(self.__yields_latex_file.name)

        if self.__store_hbins_yields:
            print "\t{}".format(self.__yields_hbins_file.name)

        if self.__store_histo_yields:
            print "\t{}".format(self.__yields_histo_file.GetName())

        if self.__yields_raw_file:
            self.__yields_raw_file.close()

        if self.__yields_latex_file:
            self.__yields_latex_file.close()

        if self.__yields_hbins_file:
            self.__yields_hbins_file.close()

        if self.__yields_histo_file:
            self.__yields_histo_file.Close()

    def __get_file(self, ext='dat'):
        fname = os.path.join(self.__yields_outdir, self.__yields_outfile + "." + ext)

        if ext == 'root':
            return ROOT.TFile(fname, "RECREATE")
        return open(fname, "w")

    def __get_latex_variable(self, variable=''):
        var = variable.replace(' ', '').replace('\n', '').split('_')[-1]

        if var == "pt":
            return "$p_T(\\mathrm{GeV})$"
        elif var == "eta":
            return "$\\eta$"
        elif var == "phi":
            return "$\\phi$(rad)"
        elif var == "width":
            return "jet width"
        elif var == "ntrks":
            return "Jet $N_{\\mathrm{tracks}}$"

        return "Unrecognised variable"

    def __get_latex_table_top(self, name=""):
        variable = self.__get_latex_variable(name)
        return """\\begin{table}
\\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\\begin{center}
\\begin{tabular}{llll}\hline
\mc{2}{l}{%s range} & Scale Factor & Uncertainty\\\ \hline \n""" % (variable)

    def __get_latex_table_bottom(self, name=""):
        label = name.replace('\n', '')
        return """\hline
\end{tabular}
\end{center}
\caption{ <Particle> scale factors measured in <Analysis>.}
\label{tb:%s}
\end{table}\n\n""" % (label)

    def __store_text_yields(self):
        if not self.__store_text_yield:
            return True

        if not self.__point_yields_list:
            print "Warning: list of point yields is empty..."
            return False

        tex_text = ''
        raw_text = ''
        hbins_text = ''
        store_list = set([])
        for py in self.__point_yields_list:
            if not py.ready:
                print "Warning: PointYield with name %s is not ready and won't be stored..." % (py.name)

            if py.get_name() not in store_list:
                store_list.add(py.get_name())
            else:
                print "Info: avoiding multiple stores of ", py.get_name()

            if self.__store_raw_yields:
                raw_text += py.name + "\n" + py.get_raw_lines() + "\n"

            if self.__store_latex_yields:
                tex_text += self.__get_latex_table_top(py.get_name()) + py.get_latex_lines() + self.__get_latex_table_bottom(py.get_name())

            if self.__store_hbins_yields:
                hbins_text += py.name + py.get_raw_lines() + "\n"

        #store
        if self.__store_raw_yields:
            self.__yields_raw_file.write(raw_text)

        if self.__store_latex_yields:
            self.__yields_latex_file.write(tex_text)

        if self.__store_hbins_yields:
            self.__yields_hbins_file.write(hbins_text)

        return True

    def __store_histo_root_yields(self):
        if not self.__store_histo_yields:
            return True

        if not self.__point_yields_list:
            print "Warning: list of point yields is empty..."
            return False

        for py in self.__point_yields_list:
            if not py.ready:
                print "Warning: PointYield with name %s is not ready and won't be stored..." % (py.name)

            if not py.write_histo(rfile=self.__yields_histo_file):
                print "Warning: cannot store histo '%s'" % (py.name)

        return True

    def __point_exists_in_yields_list(self, yield_obj=None):
        for py in self.__point_yields_list:
            if py.unique_name == yield_obj.unique_name:
                return True
        return False

    def __append_to_point_yields_list(self, yield_obj=None):
        if not self.__point_exists_in_yields_list(yield_obj):
            self.__point_yields_list.append(yield_obj)
        else:
            print "Info: duplication is skipped ", yield_obj.name

    def append(self, yield_object=None):
        if yield_object:
            self.__append_to_point_yields_list(yield_object)

    def duplicate(self, yield_object=None, source="", targets=[]):
        if yield_object:
            for target in targets:
                y = Yields.deep_copy(yield_object)
                y.name_replace(source, target)
                self.__point_yields_list.append(y)

    def store(self):
        return self.__store_text_yields() and self.__store_histo_root_yields()
