import os, commands, time, argparse, logging
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPplotting.FileUtils import ReadInputConfig, ReadMetaInputs
from ClusterSubmission.Utils import CheckConfigPaths, id_generator, WriteList, ReadListFromFile, IsROOTFile, setup_engine, setupBatchSubmitArgParser, AppendToList


class ClusterFakeFactorSubmit(object):
    def __init__(
            self,
            cluster_engine=None,
            input_files=[],
            noSyst=False,
            # Timing information
            runTime='24:00:00',
            runMem=1400):
        self.__cluster_engine = cluster_engine
        self.__scheduled_jobs = 0
        self.__noSyst = noSyst
        self.__input_files = input_files
        self.__run_time = runTime
        self.__vmem = runMem
        self.PrepareInputs()

    def PrepareInputs(self):
        if len(self.__input_files) == 0:
            logging.error("No Input given")
            return False
        if not self.engine().submit_hook(): return False
        samples = []
        output = []
        for sample in self.__input_files:

            samples += ["{}".format(sample)]

            output += [sample.split("/")[-1]]

        AppendToList(samples, self.input_samples())

        AppendToList(output, self.tmp_file_name_cfg())
        self.__scheduled_jobs += len(self.__input_files)

    def run_time(self):
        return self.__run_time

    def memory(self):
        return self.__vmem

    def input_samples(self):
        return "{}/InSamples.conf".format(self.engine().config_dir())

    def engine(self):
        return self.__cluster_engine

    def n_sheduled(self):
        return self.__scheduled_jobs

    def tmp_file_name_cfg(self):
        return "%s/out_fileNames.conf" % (self.engine().config_dir())

    def submit(self):
        if not self.engine().submit_build_job():
            print "no build job submitted"
            return False

        if not self.engine().submit_array(script="XAMPPplotting/scripts/FourLepton/ApplyFakefactorOnBatch.sh",
                                          mem=self.memory(),
                                          run_time=self.run_time(),
                                          env_vars=[("InputSamples", self.input_samples()), ("OutCfg", self.tmp_file_name_cfg()),
                                                    ("OutDir", self.engine().out_dir()), ("treeName", self.engine().out_dir()),
                                                    ("FakefactorFiles", self.engine().out_dir())],
                                          array_size=self.n_sheduled()):

            return False
        return self.engine().finish()


if __name__ == "__main__":
    parser = setupBatchSubmitArgParser()

    parser.add_argument("--inputPath", help="the input histfittertrees", required=True)
    parser.add_argument("--treeName", help="the name of the tree", default="FourleptonTree_Nominal")
    parser.add_argument("--FakefactorFiles", help="Folder containing the fakefactor histograms", default="/ptmp/mpp/maren/Fakefactor/")
    parser.add_argument("--addScalefactor", help="apply scalefactors to the fakefactors", action='store_true', default=False)
    parser.add_argument("--faketypes",
                        help="specify the faketypes that should be used",
                        nargs='+',
                        default=["LF", "HF", "Gluon", "Elec", "Conv"])
    parser.add_argument("--processes", help="specify the processes used for the fakefactors", nargs='+', default=["ttbar", "Zjets"])

    parser.add_argument('--RunTime', help='Changes the RunTime of the analysis Jobs', default='07:59:59')
    parser.add_argument('--vmem', help='Changes the virtual memory needed by each jobs', type=int, default=2000)

    parser.add_argument("--noSyst", help="Disable the systematics on the fly", action='store_true', default=False)
    parser.add_argument("--CreateVRHistos", action='store_true', default=False)
    parser.add_argument("--outDirVRHistos",
                        help="Specify an output folder where the root files containing VR-histos get stored",
                        default='/ptmp/mpp/maren/ReducibleBkgHistos/')

    options = parser.parse_args()

    cluster_engine = setup_engine(options)

    input_files = sorted([I for I in CheckConfigPaths([options.inputPath], filetype="root")])

    listofcmds = "{}/ListofCmds.txt".format(cluster_engine.config_dir())

    cmd = "python {}/../BUILD/source/XAMPPplotting/python/DataDrivenAnalysis/ApplyFakeFactors_forRedBkg.py   --treeName {} --outDir {} --FakefactorFiles {} ".format(
        cluster_engine.config_dir(), options.treeName, cluster_engine.out_dir(), options.FakefactorFiles)
    if options.addScalefactor:
        cmd += "--addScalefactor"

    if options.CreateVRHistos:
        cmd += " --CreateVRHistos "
        cmd += " --outDirVRHistos " + options.outDirVRHistos
    print cmd
    WriteList(["{} --inputFile {}".format(cmd, sample) for sample in input_files], listofcmds)

    if not cluster_engine.submit_build_job():
        logging.error("Submission failed")
        exit(1)

    cluster_engine.submit_array(script="ClusterSubmission/Run.sh",
                                mem=options.vmem,
                                env_vars=[("ListOfCmds", listofcmds)],
                                run_time=options.RunTime,
                                array_size=len(ReadListFromFile(listofcmds)))
    cluster_engine.submit_clean_all(hold_jobs=[cluster_engine.job_name()])
    cluster_engine.finish()

    #~ submit_job = ClusterFakeFactorSubmit(cluster_engine=cluster_engine,
    #~ input_files=sorted([I for I in CheckConfigPaths([options.inputPath], filetype="root")]),
    #~ noSyst=options.noSyst,
    #~ runTime=options.RunTime,
    #~ runMem=options.vmem)

    #~ submit_job.submit()
