#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-09/Rel20p7/"

C_Name = DSConfigName("Release 20.7")
SignalPath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-09/Rel20p7/"
DataPath = Path

Periods = [Cfg[:Cfg.rfind(".")] for Cfg in os.listdir(Path) if Cfg.startswith("data")]
LUMI = 36.1

# Samples
Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="data",
                name="data",
                filepath=["%s/%s.root" % (DataPath, P) for P in Periods],
                sampletype=SampleTypes.Data)

ZZ = DSconfig(colour=ROOT.kAzure - 4,
              label="ZZ",
              name="ZZ",
              filepath=[
                  "%s/Sherpa222_ZZ.root" % (Path),
                  "%s/Sherpa_ggZZnoH.root" % (Path),
              ],
              sampletype=SampleTypes.Irreducible,
              TheoUncert=0.1)
Higgs = DSconfig(
    colour=ROOT.kViolet - 9,
    label="Higgs",
    name="VH/H",
    filepath=[
        "%s/PowHegPy8_WH.root" % (Path),
        "%s/PowHegPy8_ZH.root" % (Path),
        "%s/Higgs_VBF.root" % (Path),
        "%s/Higgs_ggF.root" % (Path),
        #"%s/aMcAtNloPy8_ttH.root" % (Path),
    ],
    sampletype=SampleTypes.Irreducible)
VVV = DSconfig(colour=ROOT.kMagenta - 9,
               label="VVV",
               name="VVV",
               filepath=["%s/Sherpa221_VVV.root" % (Path)],
               sampletype=SampleTypes.Irreducible)
TwoFakes = DSconfig(colour=ROOT.kYellow,
                    label="2-fakes",
                    name="TwoFakes",
                    filepath=[
                        "%s/MGPy8EG_HT_Zmumu.root" % (Path),
                        "%s/MGPy8EG_NP_Zee.root" % (Path),
                        "%s/MGPy8EG_NP_Ztautau.root" % (Path),
                        "%s/PowHegPy8_Wjets.root" % (Path),
                        "%s/PowHegPy6_top.root" % (Path),
                    ],
                    sampletype=SampleTypes.Reducible)
#ttbar = DSconfig(colour=ROOT.TColor.GetColor(102, 252, 245),
#                 label="t#bar{t}",
#                 name="ttbar",
#                 filepath=[
#                     "%s/PowHegPy6_top.root" % (Path),
#                 ],
#                 sampletype=SampleTypes.Reducible)
ttV = DSconfig(colour=ROOT.kTeal - 5,
               label="t#bar{t}Z(WW)",
               name="ttV",
               filepath=[
                   "%s/MGPy8EG_ttZ.root" % (Path),
                   "%s/aMCNLO_tWZ.root" % (Path),
                   "%s/MGPy8EG_MultiTop.root" % (Path),
                   "%s/MGPy8EG_ttWW.root" % (Path),
                   "%s/MGPy8EG_ttWZ.root" % (Path),
               ],
               sampletype=SampleTypes.Irreducible)

OneFakeRed = DSconfig(
    colour=ROOT.TColor.GetColor(20, 54, 208),
    label="1-fakes",
    name="one-fakes",
    filepath=[
        "%s/Sherpa221_WZ.root" % (Path),
        "%s/MGPy8EG_ttW.root" % (Path),
        #"%s/Reducible.root" % (Path),
    ],
    sampletype=SampleTypes.Reducible)

#C1C1_800_200_LLE12k = DSconfig (colour=ROOT.kMagenta , label="m_{#tilde{#chi}^{0}_{1}}=800 GeV" , name="C1C1_800_200_LLE12k" , filepath=SignalPath+"C1C1_800_200_LLE12k.root", sampletype=SampleTypes.Signal)
#C1C1_1000_200_LLE12k = DSconfig (colour=ROOT.kBlack , label="m_{#tilde{#chi}^{#pm}_{1}}=1 TeV, #lambda_{12k}" , name="C1C1_1000_200_LLE12k" , filepath=SignalPath+"C1C1_1000_200_LLE12k.root", sampletype=SampleTypes.Signal)

#C1C1_1000_100_LLEi33 = DSconfig(colour=ROOT.kBlue + 2,
#                                label="m_{#tilde{#chi}^{#pm}_{1}}=1200 GeV, #lambda_{i33}",
#                                name="Wino_1200_600_LLEi33",
#                                filepath=SignalPath + "Wino_1200_600_LLEi33.root",
#                                sampletype=SampleTypes.Signal)

#GGM = DSconfig(
#    colour=ROOT.kBlue + 2,
#    label="GGM",
#    name="GGM",
#    filepath=[
#        "%s/GGMHinoZh50_200.root" % (Path),
#    ],
#    sampletype=SampleTypes.Signal)

#
SignalPath = '/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-09/Rel20p7/'
VVh_1000_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='VVh_1000_100_LLEi33',
                               label='VVh_1000_100_LLEi33',
                               filepath=SignalPath + '/VVh_1000_100_LLEi33.root')
VVh_1000_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_1000_10_LLEi33',
                              label='VVh_1000_10_LLEi33',
                              filepath=SignalPath + '/VVh_1000_10_LLEi33.root')
VVh_1000_400_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='VVh_1000_400_LLEi33',
                               label='VVh_1000_400_LLEi33',
                               filepath=SignalPath + '/VVh_1000_400_LLEi33.root')
VVh_1000_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_1000_50_LLEi33',
                              label='VVh_1000_50_LLEi33',
                              filepath=SignalPath + '/VVh_1000_50_LLEi33.root')
VVh_1000_870_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='VVh_1000_870_LLEi33',
                               label='VVh_1000_870_LLEi33',
                               filepath=SignalPath + '/VVh_1000_870_LLEi33.root')
VVh_1100_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='VVh_1100_100_LLEi33',
                               label='VVh_1100_100_LLEi33',
                               filepath=SignalPath + '/VVh_1100_100_LLEi33.root')
VVh_1100_400_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='VVh_1100_400_LLEi33',
                               label='VVh_1100_400_LLEi33',
                               filepath=SignalPath + '/VVh_1100_400_LLEi33.root')
VVh_1100_800_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='VVh_1100_800_LLEi33',
                               label='VVh_1100_800_LLEi33',
                               filepath=SignalPath + '/VVh_1100_800_LLEi33.root')
VVh_600_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_600_100_LLEi33',
                              label='VVh_600_100_LLEi33',
                              filepath=SignalPath + '/VVh_600_100_LLEi33.root')
VVh_600_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVh_600_10_LLEi33',
                             label='VVh_600_10_LLEi33',
                             filepath=SignalPath + '/VVh_600_10_LLEi33.root')
VVh_600_300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_600_300_LLEi33',
                              label='VVh_600_300_LLEi33',
                              filepath=SignalPath + '/VVh_600_300_LLEi33.root')
VVh_600_470_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_600_470_LLEi33',
                              label='VVh_600_470_LLEi33',
                              filepath=SignalPath + '/VVh_600_470_LLEi33.root')
VVh_600_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVh_600_50_LLEi33',
                             label='VVh_600_50_LLEi33',
                             filepath=SignalPath + '/VVh_600_50_LLEi33.root')
VVh_700_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_700_100_LLEi33',
                              label='VVh_700_100_LLEi33',
                              filepath=SignalPath + '/VVh_700_100_LLEi33.root')
VVh_700_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVh_700_10_LLEi33',
                             label='VVh_700_10_LLEi33',
                             filepath=SignalPath + '/VVh_700_10_LLEi33.root')
VVh_700_200_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_700_200_LLEi33',
                              label='VVh_700_200_LLEi33',
                              filepath=SignalPath + '/VVh_700_200_LLEi33.root')
VVh_700_400_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_700_400_LLEi33',
                              label='VVh_700_400_LLEi33',
                              filepath=SignalPath + '/VVh_700_400_LLEi33.root')
VVh_700_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVh_700_50_LLEi33',
                             label='VVh_700_50_LLEi33',
                             filepath=SignalPath + '/VVh_700_50_LLEi33.root')
VVh_700_570_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_700_570_LLEi33',
                              label='VVh_700_570_LLEi33',
                              filepath=SignalPath + '/VVh_700_570_LLEi33.root')
VVh_800_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_800_100_LLEi33',
                              label='VVh_800_100_LLEi33',
                              filepath=SignalPath + '/VVh_800_100_LLEi33.root')
VVh_800_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVh_800_10_LLEi33',
                             label='VVh_800_10_LLEi33',
                             filepath=SignalPath + '/VVh_800_10_LLEi33.root')
VVh_800_200_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_800_200_LLEi33',
                              label='VVh_800_200_LLEi33',
                              filepath=SignalPath + '/VVh_800_200_LLEi33.root')
VVh_800_300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_800_300_LLEi33',
                              label='VVh_800_300_LLEi33',
                              filepath=SignalPath + '/VVh_800_300_LLEi33.root')
VVh_800_400_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_800_400_LLEi33',
                              label='VVh_800_400_LLEi33',
                              filepath=SignalPath + '/VVh_800_400_LLEi33.root')
VVh_800_500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_800_500_LLEi33',
                              label='VVh_800_500_LLEi33',
                              filepath=SignalPath + '/VVh_800_500_LLEi33.root')
VVh_800_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVh_800_50_LLEi33',
                             label='VVh_800_50_LLEi33',
                             filepath=SignalPath + '/VVh_800_50_LLEi33.root')
VVh_800_600_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_800_600_LLEi33',
                              label='VVh_800_600_LLEi33',
                              filepath=SignalPath + '/VVh_800_600_LLEi33.root')
VVh_800_670_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_800_670_LLEi33',
                              label='VVh_800_670_LLEi33',
                              filepath=SignalPath + '/VVh_800_670_LLEi33.root')
VVh_900_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_900_100_LLEi33',
                              label='VVh_900_100_LLEi33',
                              filepath=SignalPath + '/VVh_900_100_LLEi33.root')
VVh_900_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVh_900_10_LLEi33',
                             label='VVh_900_10_LLEi33',
                             filepath=SignalPath + '/VVh_900_10_LLEi33.root')
VVh_900_200_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_900_200_LLEi33',
                              label='VVh_900_200_LLEi33',
                              filepath=SignalPath + '/VVh_900_200_LLEi33.root')
VVh_900_300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_900_300_LLEi33',
                              label='VVh_900_300_LLEi33',
                              filepath=SignalPath + '/VVh_900_300_LLEi33.root')
VVh_900_400_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_900_400_LLEi33',
                              label='VVh_900_400_LLEi33',
                              filepath=SignalPath + '/VVh_900_400_LLEi33.root')
VVh_900_500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_900_500_LLEi33',
                              label='VVh_900_500_LLEi33',
                              filepath=SignalPath + '/VVh_900_500_LLEi33.root')
VVh_900_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVh_900_50_LLEi33',
                             label='VVh_900_50_LLEi33',
                             filepath=SignalPath + '/VVh_900_50_LLEi33.root')
VVh_900_600_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_900_600_LLEi33',
                              label='VVh_900_600_LLEi33',
                              filepath=SignalPath + '/VVh_900_600_LLEi33.root')
VVh_900_700_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_900_700_LLEi33',
                              label='VVh_900_700_LLEi33',
                              filepath=SignalPath + '/VVh_900_700_LLEi33.root')
VVh_900_770_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVh_900_770_LLEi33',
                              label='VVh_900_770_LLEi33',
                              filepath=SignalPath + '/VVh_900_770_LLEi33.root')
