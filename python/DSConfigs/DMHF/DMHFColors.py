from XAMPPplotting.Defs import *

sigColors = {
    '300_c1': ROOT.kMagenta,
    '20_c1': 12,
}
bkgColors = {
    'Z': ROOT.kYellow - 7,
    'W': ROOT.kGreen + 2,
    'ttbar': ROOT.kRed - 7,
    'ttV': ROOT.kMagenta + 1,
    'ttGamma': 909,
    'singleTop': ROOT.kAzure + 1,
    'dibosons': ROOT.kAzure - 5,
    'QCD': 0,
    'VGamma': ROOT.kAzure - 5,
    'ttbar_Herwigpp': ROOT.kOrange + 7,
    'ttbar_radLo': ROOT.kAzure + 6,
    'ttbar_radHi': ROOT.kBlue - 6,
    'ttbar_Sherpa': ROOT.kOrange,
    'singleTop_Herwigpp': ROOT.kGreen + 6,
    'singleTop_radLo': ROOT.kGreen + 7,
    'singleTop_radHi': ROOT.kGreen + 8,
    'singleTop_DS': ROOT.kGreen + 9,
}
