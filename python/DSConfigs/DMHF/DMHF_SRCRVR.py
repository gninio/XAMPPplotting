#! /usr/bin/env python
from XAMPPplotting.Stop0LColors import *

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-22/dmhf_srcrvr_v2/'
# BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-22/dmhf_ttgamma/' # for ttgamma CR

# Data
# Data = DSconfig(lumi=36.1,colour=ROOT.kBlack,name="Data",label="Data",filepath=BasePath+"Data_Period_Run2_Input.root",sampletype=SampleTypes.Data)

# Signal MC
# DM_TTpseudo_20_c1 = DSconfig(colour=ROOT.kGreen,name="DM_TTpseudo_20_c1",label="(m_{a},m_{#chi})=(20,1) GeV",filepath=BasePath+"DM_TTpseudo_20_c1_a821_r7676_p2888_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenTriangleUp)
# DM_TTpseudo_300_c1 = DSconfig(colour=ROOT.kRed,name="DM_TTpseudo_300_c1",label="(m_{a},m_{#chi})=(300,1) GeV",filepath=BasePath+"DM_TTpseudo_300_c1_a821_r7676_p2888_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenCircle)
# DM_TTscalar_20_c1 = DSconfig(colour=ROOT.kGreen,name="DM_TTscalar_20_c1",label="(m_{#phi},m_{#chi})=(20,1) GeV",filepath=BasePath+"DM_TTscalar_20_c1_a821_r7676_p2888_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenTriangleUp)
# DM_TTscalar_300_c1 = DSconfig(colour=ROOT.kBlue,name="DM_TTscalar_300_c1",label="(m_{#phi},m_{#chi})=(300,1) GeV",filepath=BasePath+"DM_TTscalar_300_c1_a821_r7676_p2888_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenTriangleDown)

DE_tt_c1_M400 = DSconfig(colour=ROOT.kRed,
                         name="DE_tt_c1_M400",
                         label="(m_{M},m_{#varphi})=(400,0.1) GeV (c_{1}=1)",
                         filepath=BasePath + "DE_tt_c1_M400_Input.root",
                         sampletype=SampleTypes.Signal,
                         markerstyle=ROOT.kOpenCircle)
DE_tt_c2_M600 = DSconfig(colour=ROOT.kRed,
                         name="DE_tt_c2_M600",
                         label="(m_{M},m_{#varphi})=(600,0.1) GeV (c_{2}=1)",
                         filepath=BasePath + "DE_tt_c2_M600_Input.root",
                         sampletype=SampleTypes.Signal,
                         markerstyle=ROOT.kOpenCircle)

# Background MC
ttV = DSconfig(colour=bkgColors['ttV'],
               name="ttV",
               label="t#bar{t}+V",
               filepath=BasePath + "ttV_Input.root",
               sampletype=SampleTypes.Irreducible)
# ttgamma = DSconfig(colour=bkgColors['ttGamma'],name="ttGamma",label="t#bar{t}+#gamma",filepath=BasePath+"ttGamma_Input.root",sampletype=SampleTypes.Irreducible)
Diboson = DSconfig(colour=bkgColors['dibosons'],
                   name="Diboson",
                   label="Diboson",
                   filepath=BasePath + "Diboson_Input.root",
                   sampletype=SampleTypes.Reducible)
singleTop = DSconfig(colour=bkgColors['singleTop'],
                     name="singleTop",
                     label="Single Top",
                     filepath=BasePath + "singleTop_Input.root",
                     sampletype=SampleTypes.Reducible)
ttbar = DSconfig(colour=bkgColors['ttbar'],
                 name="ttbar",
                 label="t#bar{t}",
                 filepath=BasePath + "ttbar_Input.root",
                 sampletype=SampleTypes.Reducible)
Wjets = DSconfig(colour=bkgColors['W'],
                 name="Wjets",
                 label="W+jets",
                 filepath=BasePath + "Wjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
Zjets = DSconfig(colour=bkgColors['Z'],
                 name="Zjets",
                 label="Z+jets",
                 filepath=BasePath + "Zjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
# SinglePhoton = DSconfig(colour=ROOT.kViolet+5,name="SinglePhoton",label="#gamma+jets",filepath=BasePath+"SinglePhoton_Input.root",sampletype=SampleTypes.Reducible)
# Vgamma = DSconfig(colour=bkgColors['VGamma'],name="Vgamma",label="#gamma+V",filepath=BasePath+"PhotonV_Input.root",sampletype=SampleTypes.Irreducible)
