#! /usr/bin/env python
from XAMPPplotting.Stop0LColors import *  #use DMHF colors for thesis

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-21/stop_significances/'

# Signal MC
TT_directTT_1000_1 = DSconfig(colour=sigColors['1000_1'],
                              name="TT_directTT_1000_1",
                              label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(1000,1) GeV",
                              filepath=BasePath + "TT_directTT_1000_1_a821_r7676_Input.root",
                              sampletype=SampleTypes.Signal,
                              markerstyle=ROOT.kOpenCircle)
TT_directTT_800_1 = DSconfig(colour=sigColors['800_1'],
                             name="TT_directTT_800_1",
                             label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(800,1) GeV",
                             filepath=BasePath + "TT_directTT_800_1_a821_r7676_Input.root",
                             sampletype=SampleTypes.Signal,
                             markerstyle=ROOT.kOpenTriangleUp)
# TT_directTT_400_212 = DSconfig(colour=sigColors['400_212'],name="TT_directTT_400_212",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(400,212) GeV",filepath=BasePath+"TT_directTT_400_212_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_300_127 = DSconfig(colour=sigColors['300_127'],name="TT_directTT_300_127",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(300,127) GeV",filepath=BasePath+"TT_directTT_300_127_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_400_227 = DSconfig(colour=sigColors['500_327'],name="TT_directTT_400_227",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(400,227) GeV",filepath=BasePath+"TT_directTT_400_227_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_500_327 = DSconfig(colour=sigColors['500_327'],name="TT_directTT_500_327",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(500,327) GeV",filepath=BasePath+"TT_directTT_500_327_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_350_177 = DSconfig(colour=sigColors['350_177'],name="TT_directTT_350_177",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(350,177) GeV",filepath=BasePath+"TT_directTT_350_177_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenTriangleUp)
# TT_directTT_450_277 = DSconfig(colour=sigColors['450_277'],name="TT_directTT_450_277",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(450,277) GeV",filepath=BasePath+"TT_directTT_450_277_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenCircle)
# TT_directTT_600_300 = DSconfig(colour=sigColors['600_300'],name="TT_directTT_600_300",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,300) GeV",filepath=BasePath+"TT_directTT_600_300_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenTriangleUp)
# TT_directTT_700_400 = DSconfig(colour=sigColors['700_400'],name="TT_directTT_700_400",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(700,400) GeV",filepath=BasePath+"TT_directTT_700_400_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenCircle)

# GG_1700_400 = DSconfig(colour=sigColors['GG_1700_400'],name="GG_1700_400",label="(m_{#tilde{g}},m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(1700,400,395) GeV",filepath=BasePath+"GG_directGtc5_1700_400_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenCircle)

# TT_onestepBB_400_100_50 = DSconfig(colour=sigColors['400_100_50'],name="TT_onestepBB_400_100_50",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(400,100,50) GeV",filepath=BasePath+"TT_onestepBB_400_100_50_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenCircle)
# TT_mixedBT_450_100_50 = DSconfig(colour=sigColors['450_100_50_mixed'],name="TT_mixedBT_450_100_50",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(450,100,50) GeV (mixed)",filepath=BasePath+"TT_mixedBTMGPy8EG_450_100_50_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_onestepBB_700_100_50 = DSconfig(colour=sigColors['700_100_50'],name="TT_onestepBB_700_100_50",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(700,100,50) GeV",filepath=BasePath+"TT_onestepBB_700_100_50_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenCircle)
# TT_mixedBT_700_100_50 = DSconfig(colour=sigColors['700_100_50_mixed'],name="TT_mixedBT_700_100_50",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(700,100,50) GeV (mixed)",filepath=BasePath+"TT_mixedBTMGPy8EG_700_100_50_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenTriangleUp)
# TT_onestepBB_700_200_100 = DSconfig(colour=ROOT.kGreen-2,name="TT_onestepBB_700_200_100",label="#splitline{(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})}{=(700,200,100)GeV}",filepath=BasePath+"TT_onestepBB_700_200_100_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_mixedBT_700_200_100 = DSconfig(colour=ROOT.kGreen-2,name="TT_mixedBT_700_200_100",label="#splitline{(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})}{=(700,200,100)GeV mixed}",filepath=BasePath+"TT_mixedBTMGPy8EG_700_200_100_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# Background MC
ttV = DSconfig(colour=bkgColors['ttV'],
               name="ttV",
               label="t#bar{t}+V",
               filepath=BasePath + "ttV_Input.root",
               sampletype=SampleTypes.Irreducible)
# ttgamma = DSconfig(colour=bkgColors['ttGamma'],name="ttGamma",label="t#bar{t}+#gamma",filepath=BasePath+"ttGamma_Input.root",sampletype=SampleTypes.Irreducible)
Diboson = DSconfig(colour=bkgColors['dibosons'],
                   name="Diboson",
                   label="Diboson",
                   filepath=BasePath + "DibosonNew_Input.root",
                   sampletype=SampleTypes.Reducible)
singleTop = DSconfig(colour=bkgColors['singleTop'],
                     name="singleTop",
                     label="Single Top",
                     filepath=BasePath + "singleTop_Input.root",
                     sampletype=SampleTypes.Reducible)
ttbar = DSconfig(colour=bkgColors['ttbar'],
                 name="ttbar",
                 label="t#bar{t}",
                 filepath=BasePath + "ttbar_Input.root",
                 sampletype=SampleTypes.Reducible)
Wjets = DSconfig(colour=bkgColors['W'],
                 name="Wjets",
                 label="W+jets",
                 filepath=BasePath + "Wjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
Zjets = DSconfig(colour=bkgColors['Z'],
                 name="Zjets",
                 label="Z+jets",
                 filepath=BasePath + "Zjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
