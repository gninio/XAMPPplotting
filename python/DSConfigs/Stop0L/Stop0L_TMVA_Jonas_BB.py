#! /usr/bin/env python
from XAMPPplotting.Defs import *

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-06-12/tmva_apply_SRAB//'

# Data_Period_Run2_Input = DSconfig(name='Data_Period_Run2_Input',filepath=BasePath+'Data_Period_Run2_Input.root',sampletype=SampleTypes.Data,lumi=36.1)

# Diboson_Input = DSconfig(name='Diboson_Input',filepath=BasePath+'Diboson_Input.root',sampletype=SampleTypes.Reducible)
DibosonNew_Input = DSconfig(name='DibosonNew_Input', filepath=BasePath + 'DibosonNew_Input.root', sampletype=SampleTypes.Reducible)
# others = DSconfig(name='others',filepath=BasePath+'others',sampletype=SampleTypes.Reducible)
singleTop_Input = DSconfig(name='singleTop_Input', filepath=BasePath + 'singleTop_Input.root', sampletype=SampleTypes.Reducible)
ttbar_Input = DSconfig(name='ttbar_Input', filepath=BasePath + 'ttbar_Input.root', sampletype=SampleTypes.Reducible)
ttV_Input = DSconfig(name='ttV_Input', filepath=BasePath + 'ttV_Input.root', sampletype=SampleTypes.Reducible)
Wjets_Sherpa221_Input = DSconfig(name='Wjets_Sherpa221_Input',
                                 filepath=BasePath + 'Wjets_Sherpa221_Input.root',
                                 sampletype=SampleTypes.Reducible)
Zjets_Sherpa221_Input = DSconfig(name='Zjets_Sherpa221_Input',
                                 filepath=BasePath + 'Zjets_Sherpa221_Input.root',
                                 sampletype=SampleTypes.Reducible)

TT_onestepBB_400_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_400_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_400_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_400_300_150_a821_r7676_Input = DSconfig(name='TT_onestepBB_400_300_150_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_400_300_150_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_400_390_195_a821_r7676_Input = DSconfig(name='TT_onestepBB_400_390_195_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_400_390_195_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_450_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_450_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_450_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_450_400_200_a821_r7676_Input = DSconfig(name='TT_onestepBB_450_400_200_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_450_400_200_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_500_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_500_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_500_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_500_200_100_a821_r7676_Input = DSconfig(name='TT_onestepBB_500_200_100_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_500_200_100_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_500_300_150_a821_r7676_Input = DSconfig(name='TT_onestepBB_500_300_150_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_500_300_150_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_500_400_200_a821_r7676_Input = DSconfig(name='TT_onestepBB_500_400_200_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_500_400_200_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_500_490_245_a821_r7676_Input = DSconfig(name='TT_onestepBB_500_490_245_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_500_490_245_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_550_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_550_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_550_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_550_200_100_a821_r7676_Input = DSconfig(name='TT_onestepBB_550_200_100_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_550_200_100_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_550_300_150_a821_r7676_Input = DSconfig(name='TT_onestepBB_550_300_150_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_550_300_150_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_550_400_200_a821_r7676_Input = DSconfig(name='TT_onestepBB_550_400_200_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_550_400_200_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_550_500_250_a821_r7676_Input = DSconfig(name='TT_onestepBB_550_500_250_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_550_500_250_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_600_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_600_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_600_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_600_200_100_a821_r7676_Input = DSconfig(name='TT_onestepBB_600_200_100_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_600_200_100_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_600_300_150_a821_r7676_Input = DSconfig(name='TT_onestepBB_600_300_150_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_600_300_150_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_600_400_200_a821_r7676_Input = DSconfig(name='TT_onestepBB_600_400_200_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_600_400_200_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_600_500_250_a821_r7676_Input = DSconfig(name='TT_onestepBB_600_500_250_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_600_500_250_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_600_590_295_a821_r7676_Input = DSconfig(name='TT_onestepBB_600_590_295_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_600_590_295_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_650_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_650_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_650_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_650_200_100_a821_r7676_Input = DSconfig(name='TT_onestepBB_650_200_100_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_650_200_100_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_650_300_150_a821_r7676_Input = DSconfig(name='TT_onestepBB_650_300_150_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_650_300_150_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_650_400_200_a821_r7676_Input = DSconfig(name='TT_onestepBB_650_400_200_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_650_400_200_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_650_500_250_a821_r7676_Input = DSconfig(name='TT_onestepBB_650_500_250_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_650_500_250_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_650_600_300_a821_r7676_Input = DSconfig(name='TT_onestepBB_650_600_300_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_650_600_300_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_700_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_700_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_700_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_700_200_100_a821_r7676_Input = DSconfig(name='TT_onestepBB_700_200_100_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_700_200_100_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_700_300_150_a821_r7676_Input = DSconfig(name='TT_onestepBB_700_300_150_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_700_300_150_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_700_400_200_a821_r7676_Input = DSconfig(name='TT_onestepBB_700_400_200_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_700_400_200_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_700_500_250_a821_r7676_Input = DSconfig(name='TT_onestepBB_700_500_250_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_700_500_250_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_700_600_300_a821_r7676_Input = DSconfig(name='TT_onestepBB_700_600_300_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_700_600_300_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_750_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_750_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_750_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_750_200_100_a821_r7676_Input = DSconfig(name='TT_onestepBB_750_200_100_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_750_200_100_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_750_300_150_a821_r7676_Input = DSconfig(name='TT_onestepBB_750_300_150_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_750_300_150_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_750_400_200_a821_r7676_Input = DSconfig(name='TT_onestepBB_750_400_200_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_750_400_200_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_750_500_250_a821_r7676_Input = DSconfig(name='TT_onestepBB_750_500_250_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_750_500_250_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_750_600_300_a821_r7676_Input = DSconfig(name='TT_onestepBB_750_600_300_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_750_600_300_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_800_100_50_a821_r7676_Input = DSconfig(name='TT_onestepBB_800_100_50_a821_r7676_Input',
                                                    filepath=BasePath + 'TT_onestepBB_800_100_50_a821_r7676_Input.root',
                                                    sampletype=SampleTypes.Signal)
TT_onestepBB_800_200_100_a821_r7676_Input = DSconfig(name='TT_onestepBB_800_200_100_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_800_200_100_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_800_300_150_a821_r7676_Input = DSconfig(name='TT_onestepBB_800_300_150_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_800_300_150_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_800_400_200_a821_r7676_Input = DSconfig(name='TT_onestepBB_800_400_200_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_800_400_200_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_800_500_250_a821_r7676_Input = DSconfig(name='TT_onestepBB_800_500_250_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_800_500_250_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
TT_onestepBB_800_600_300_a821_r7676_Input = DSconfig(name='TT_onestepBB_800_600_300_a821_r7676_Input',
                                                     filepath=BasePath + 'TT_onestepBB_800_600_300_a821_r7676_Input.root',
                                                     sampletype=SampleTypes.Signal)
