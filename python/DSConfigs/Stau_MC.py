#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.FileUtils import ReadInputConfig, ResolvePath
from XAMPPplotting.CheckMetaData import GetNormalizationDB
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
from XAMPPplotting.TauOptions import TauOptions

BasePath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-26/lephad_ff/"
V_Jets_Generators = {
    "Sherpa": {
        "Zjets": [
            BasePath + "Sherpa221_Zee.root",
            BasePath + "Sherpa221_Zmumu.root",
            BasePath + "Sherpa221_Ztautau.root",
        ],
        "Wjets": [
            BasePath + "Sherpa221_Wenu.root",
            BasePath + "Sherpa221_Wmunu.root",
            BasePath + "Sherpa221_Wtaunu.root",
        ],
        "Label": "Sh"
    },
    "MG5": {
        "Zjets": [BasePath + "MG5Py8_Zee.root", BasePath + "MG5Py8_Zmumu.root", BasePath + "MG5Py8_Ztautau.root"],
        "Wjets": [BasePath + "MGPy8EG_Wenu.root", BasePath + "MGPy8EG_Wmunu.root", BasePath + "MGPy8EG_Wtaunu.root"],
        "Label": "MG5",
    },
    "PowHeg": {
        "Zjets": [
            BasePath + "PowHegPy8_Zee.root", BasePath + "PowHegPy8_Zmumu.root", BasePath + "PowHegPy8_Ztautau.root",
            BasePath + "PowHegPy8_DYee.root", BasePath + "PowHegPy8_DYmumu.root", BasePath + "PowHegPy8_DYtautau.root"
        ],
        "Wjets": [
            BasePath + "PowHegPy8_Wenu.root", BasePath + "PowHegPy8_Wmunu.root", BasePath + "PowHegPy8_Wtaunu.root",
            BasePath + "PowHegPy8_DYenu.root", BasePath + "PowHegPy8_DYmunu.root", BasePath + "PowHegPy8_DYtaunu.root"
        ],
        "Label":
        "PH",
    }
}

gen_to_pick = "Sherpa"
try:
    gen_from_opt = TauOptions().instance.generator
    if len(gen_from_opt) > 0: gen_to_pick = gen_from_opt
except:
    pass

ConfigPath = ResolvePath("XAMPPplotting/InputConf/LepHadStau/Data/")
Periods = [Cfg[:Cfg.rfind(".")] for Cfg in os.listdir(BasePath) if Cfg.find("data") == 0]
ROOT_Files = []
lumi = 0
for P in Periods:
    ROOT_Files += ReadInputConfig("%s/%s.conf" % (ConfigPath, P))

#lumi = 138.892760024
#for Run in GetNormalizationDB(ROOT_Files).GetRunNumbers():
#    lumi += CalculateRecordedLumi(Run)

lumi = 138.248833741
#print lumi
#exit(1)
Data = DSconfig(colour=ROOT.kBlack,
                fillstyle=0,
                name="Data",
                label="data",
                filepath=["%s/%s.root" % (BasePath, P) for P in Periods],
                sampletype=SampleTypes.Data,
                lumi=lumi)

VV = DSconfig(colour=ROOT.kRed + 3,
              fillstyle=0,
              name="Sherpa_VV",
              label="VV",
              filepath=[
                  BasePath + "Sherpa222_VV.root",
                  BasePath + "Higgs.root",
              ],
              sampletype=SampleTypes.Irreducible)
Zjets = DSconfig(
    colour=ROOT.kBlue - 6,
    #                  fillstyle = 0,
    name="Zjets",
    label="Z+jets (%s)" % (V_Jets_Generators[gen_to_pick]["Label"]),
    filepath=V_Jets_Generators[gen_to_pick]["Zjets"],
    sampletype=SampleTypes.Irreducible)

Wjets = DSconfig(
    colour=ROOT.kOrange,
    #                  fillstyle = 0,
    name="Wjets",
    label="W+jets (%s)" % (V_Jets_Generators[gen_to_pick]["Label"]),
    filepath=V_Jets_Generators[gen_to_pick]["Wjets"],
    sampletype=SampleTypes.Irreducible)

Top = DSconfig(
    colour=ROOT.kGreen + 2,
    name="PowHegPy8_top",
    label="top",
    filepath=[
        BasePath + "PowHegPy8_ttbar_incl.root",
        BasePath + "PowHegPy8_ttbar_MET_sliced.root",
        BasePath + "PowHegPy8_ttbar_HT_sliced.root",
        BasePath + "PowHegPy8_single_top_s_chan.root",
        BasePath + "PowHegPy8_single_top_t_chan.root",
        #BasePath + "PowHegPy8_single_top_Wt_chan_incl.root",
        BasePath + "MG5_aMCatNLO_Py8_ttV_tV_tVV.root",
        BasePath + "MG5Py8_multi_t.root",
    ],
    sampletype=SampleTypes.Irreducible)
