#!/usr/bin/env python
import os
from XAMPPplotting.Defs import *
ROOT.gStyle.SetOptStat(0)
BasePath = '/ptmp/mpp/pgadow/monoS/plotting/monoSbb_phenopaper'

# Specify samples
# Signal MC
# ds_signal = os.path.join(BasePath, "monoSbb-plot_zp1100_dm100_hs90.root")
# monoSbb_zp1100_dm100_hs90 = DSconfig(
#     name="monoSbb_zp1100_dm100_hs90",
#     label="(m_{Z'},m_{hs})=(1100,90)",
#     colour=ROOT.kOrange,
#     filepath=ds_signal,
#     sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "MonoSbb_dsid600004_zp1100_dm100_hs70.root")
monoSbb_zp1100_dm100_hs70 = DSconfig(name="monoSbb_zp1100_dm100_hs70",
                                     label="(m_{Z'},m_{hs})=(1100,70)",
                                     colour=ROOT.kRed,
                                     filepath=ds_signal,
                                     sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "MonoSbb_dsid600030_zp1100_dm100_hs50.root")
monoSbb_zp1100_dm100_hs50 = DSconfig(name="monoSbb_zp1100_dm100_hs50",
                                     label="(m_{Z'},m_{hs})=(1100,50)",
                                     colour=ROOT.kBlue,
                                     filepath=ds_signal,
                                     sampletype=SampleTypes.Signal)
