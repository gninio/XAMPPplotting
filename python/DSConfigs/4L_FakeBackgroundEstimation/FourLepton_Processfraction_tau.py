import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi

from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

PATH = "/ptmp/mpp/maren/Cluster/OUTPUT/2019-11-18/4L_ProcFrac_tau/"

ttbar = DSconfig(
    colour=ROOT.kBlue - 2,
    name="ttbar",
    label="top",
    filepath=[PATH + "PowHegPy8_ttbar_incl.root"],  #please use only PowhegPy_top.root when xsec for single top is OK
    sampletype=SampleTypes.Irreducible)

Zjets = DSconfig(
    colour=ROOT.kViolet - 9,
    name="Zjets",
    label="Z+Jets",
    filepath=[
        PATH + "PowHegPy8_Zee.root", PATH + "PowHegPy8_Zmumu.root", PATH + "PowHegPy8_Ztautau.root"
        #PATH + "Sherpa221_Zee.root",
        #PATH + "Sherpa221_Zmumu.root",
        #PATH + "Sherpa221_Ztautau.root"
    ],
    sampletype=SampleTypes.Irreducible)
