import os, sys
from XAMPPplotting import TauLogger as TL


class Year(object):
    def __init__(self, year=""):
        self.__year = year
        self.__periods = []

    @property
    def year(self):
        return self.__year

    def add(self, period):
        if period:
            self.__periods.append(period)

    def show(self):
        print "%s periods = %i" % (self.__year, len(self.__periods))


class TauLumiCalc(object):
    def __init__(self, name="lumi calculation", data_path="", conf_path="", years=[]):
        "Lumic calculation"
        self.__msg = TL.msg(2)
        self.__years = years
        self.__name = name
        self.__data_path = data_path
        self.__conf_path = conf_path
        self.__lumi = 0

    @property
    def lumi(self):
        return self.__lumi

    @property
    def lumi_ifb(self):
        return self.__lumi * 1e-3

    def __get_data_config_files(self):
        self.__msg.info("LumiCalc", "Creating unified conf file...")
        data_root_path = self.__data_path
        data_conf_path = self.__conf_path

        #check data root path
        if not os.path.isdir(data_root_path):
            self.__msg.error("LumiCalc", "Data root path '%s' not found ..." % (data_root_path))
            return "", ""

        # get physical path to conf files
        # e.g.
        # input: XAMPPplotting/InputConf/LepHadStau/V19/Data/
        # actual:  $WorkDir_DIR/data/XAMPPplotting/InputConf/LepHadStau/V19/Data
        physical_data_conf_path = os.path.join(os.environ["WorkDir_DIR"], "data", data_conf_path)
        if not os.path.isdir(physical_data_conf_path):
            self.__msg.error("LumiCalc", "Physical conf path '%s' not detected ..." % (physical_data_conf_path))
            return "", ""
        else:
            self.__msg.info("LumiCalc", "Physical conf path '%s' successfully found ..." % (physical_data_conf_path))

        # glob stuff
        import glob

        all_config_files = sorted(x.split("/")[-1] for x in glob.glob(os.path.join(physical_data_conf_path, "*.conf")))

        actual_config_files = sorted(
            [os.path.basename(x).replace("root", "conf") for x in glob.glob(os.path.join(data_root_path, "*data*.root"))])
        self.__msg.info("LumiCalc", "All data conf files found corresponding to existing root files: %s" % (actual_config_files))

        #check periods might be missing from histo production
        unavailable_config_files = []
        for cfile in all_config_files:
            if cfile not in actual_config_files:
                unavailable_config_files.append(cfile)

        if unavailable_config_files:
            self.__msg.warning(
                "LumiCalc", "Not produced periods '%s'. Total missing %i (%i)" %
                (unavailable_config_files, len(all_config_files) - len(actual_config_files), len(unavailable_config_files)))
        else:
            self.__msg.info("LumiCalc", "All periods successfully found ...")

        selected_conf_files = []
        years = []
        for conf_file in actual_config_files:
            for year in self.__years:
                #year is considered
                if year in conf_file:
                    selected_conf_files.append(conf_file)
                    #add years first
                    year_found = False
                    for entry in years:
                        if entry.year == year:
                            year_found = True
                            break
                    if not year_found:
                        years.append(Year(year))
                    #add periods
                    for entry in years:
                        if entry.year == year:
                            entry.add(conf_file)
                            break
                    #done with this year

        self.__msg.info("LumiCalc", "Selected data conf files found corresponding to existing root files: %s" % (selected_conf_files))
        if len(actual_config_files) > len(selected_conf_files):
            self.__msg.warning("LumiCalc",
                               "Only %i data config files selected out of %i" % (len(selected_conf_files), len(actual_config_files)))
        else:
            self.__msg.info("LumiCalc", "%i data config files selected out of %i" % (len(selected_conf_files), len(actual_config_files)))

        self.__msg.info("LumiCalc", "Selected periods:")
        for year in years:
            year.show()

        #create a unique conf list
        self.__msg.info("LumiCalc", "Reading input conf files...")
        all_root_lines = []
        skipped_root_lines = []
        for conf_file in selected_conf_files:
            physical_conf_file = os.path.join(physical_data_conf_path, conf_file)
            if not os.path.isfile(physical_conf_file):
                self.__msg.warning("LumiCalc", "Conf file '%s' not existing!" % (physical_conf_file))
                continue

            lines = [line.rstrip('\n') for line in open(physical_conf_file)]
            for line in lines:
                if "Input" in line and "#" in line:
                    skipped_root_lines.append(line)
                    continue
                if "Input" in line and ".root" in line:
                    all_root_lines.append(line)

        #skipped files
        self.__msg.info("LumiCalc", "Number of skipped/commented inputs: %i" % (len(skipped_root_lines)))

        #check if root files are picked up
        if not all_root_lines:
            self.__msg.warning("LumiCalc", "Could not pick up any root files from the existing conf files...")
            return "", ""

        #create a unique conf file from the list above
        physical_output_data_conf = os.path.join(physical_data_conf_path, "all_data.conf")
        with open(physical_output_data_conf, 'w') as fp:
            fp.write("\n".join(all_root_lines))
            return physical_output_data_conf, data_conf_path + "all_data.conf"
        #done

    def calculate(self):
        self.__msg.info("LumiCalc", "Calculating total lumi...")

        PhysicalBigDataConfigFile, BigDataConfigFile = self.__get_data_config_files()

        if not PhysicalBigDataConfigFile or not BigDataConfigFile:
            self.__msg.error("LumiCalc", "Invalid conf files...")
            return False

        self.__msg.info(
            "LumiCalc",
            "Big Data Config file: %s\n\t\t    Big Data Config file (physical): %s" % (BigDataConfigFile, PhysicalBigDataConfigFile))

        #pass a big conf data file
        self.__msg.info("LumiCalc", "Loading libs...")
        import XAMPPplotting.CheckMetaData as CMD
        from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
        from XAMPPplotting.FileUtils import ReadInputConfig
        self.__msg.info("LumiCalc", "Accumulating lumi...")
        for Run in CMD.GetNormalizationDB(ReadInputConfig(BigDataConfigFile)).GetRunNumbers():
            self.__lumi += CalculateRecordedLumi(Run)

        self.__msg.info("LumiCalc", "Total lumi: %f" % (self.__lumi))

        #keep big conf data file aside for later inpsection
        if os.path.isfile(PhysicalBigDataConfigFile):
            import shutil
            shutil.copy2(PhysicalBigDataConfigFile, self.__data_path)
            os.remove(PhysicalBigDataConfigFile)
            for ifile in os.listdir(self.__data_path):
                if ".conf" in ifile:
                    self.__msg.info("LumiCalc", "Big data conf file kept in %s" % (os.path.join(self.__data_path, ifile)))

        return True
