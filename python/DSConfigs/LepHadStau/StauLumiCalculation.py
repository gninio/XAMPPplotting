from XAMPPplotting import TauLumiCalc as TLC


def check(method):
    if not method:
        print "Unable to load ", method
        exit(1)


##########################################

date = "2019-06-19"

folder = "Histos_Opt_2019_06_19_13_45_30"

years = ["data15", "data16", "data17", "data18"]

data_conf_path = "XAMPPplotting/InputConf/LepHadStau/Data/"

main_root_files_path = "/ptmp/mpp/zenon/Cluster/OUTPUT/%s/%s/" % (date, folder)

print "Data", main_root_files_path
#localSetupPyAMI
#voms-proxy-init -voms atlas
lc = TLC.TauLumiCalc("Lumi calculation", data_path=main_root_files_path, conf_path=data_conf_path, years=years)

check(lc.calculate())
lumi = lc.lumi_ifb
print "Lumi", lumi
