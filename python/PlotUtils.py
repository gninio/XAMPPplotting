import ROOT, math, logging
from Defs import *
import Utils
import PlottingHistos
from Utils import GetNbins, ReplaceStringInHistoAxis, GetHorizontalLine, RemoveSpecialChars, AddHistos, CheckHisto, GetPlotRangeX, SoverBerror


class PlotUtils(object):
    def __init__(self, size=18, status="Internal", lumilabel="", lumi=1., sqrts="13", normalizedToUnity=False):
        self.__Status = status
        self.__Size = size
        self.Lumi = lumi  # in fb^-1
        self.LumiLabel = lumilabel  # as preferred by the user
        self.SqrtS = sqrts
        self.VerticalCanvasSplit = 0.4
        self.__Canvas = None
        self.__Pad1 = None
        self.__Pad2 = None
        self.__Styling = None
        self.__RatStyling = None
        self.__Legend = None
        self.__NLegEnt = 0
        self.__normalizedToUnity = normalizedToUnity
        self.__Objects = []

    def DrawTLatex(self, x=0., y=0., text="", size=-1, font=43, align=11, color=1, ndc=True):
        tex = ROOT.TLatex()
        tex.SetTextAlign(align)
        if size != -1: tex.SetTextSize(size)
        else: tex.SetTextSize(self.__Size)
        tex.SetTextFont(font)
        tex.SetTextColor(color)
        if ndc: tex.SetNDC()
        tex.DrawLatex(x, y, text)
        self.__Objects.append(tex)

    def DrawAtlas(self, x, y, align=11):
        self.DrawTLatex(x, y, "#font[72]{ATLAS} %s" % self.__Status, self.__Size, 43, align)

    def DrawLumiSqrtS(self, x, y, align=11):
        if self.__normalizedToUnity:
            self.DrawTLatex(x, y, "#sqrt{s} = %s TeV" % (self.SqrtS), self.__Size, 43, align)
        elif self.Lumi >= 999999999:
            self.DrawTLatex(x, y, "#sqrt{s} = %s TeV, ?? fb^{-1}" % (self.SqrtS), self.__Size, 43, align)
        elif self.LumiLabel != "":
            self.DrawTLatex(x, y, "#sqrt{s} = %s TeV, %s " % (self.SqrtS, self.LumiLabel), self.__Size, 43, align)
        elif self.Lumi < 1.e-3:
            lumiToPrint = "%.0f" % (self.Lumi * 1e6)
            self.DrawTLatex(x, y, "#sqrt{s} = %s TeV, %s nb^{-1}" % (self.SqrtS, lumiToPrint), self.__Size, 43, align)
        elif self.Lumi >= 1.:
            lumiToPrint = "%.1f" % (self.Lumi)
            self.DrawTLatex(x, y, "#sqrt{s} = %s TeV, %s fb^{-1}" % (self.SqrtS, lumiToPrint), self.__Size, 43, align)
        elif self.Lumi < 1.:
            lumiToPrint = "%.0f" % (self.Lumi * 1e3)
            self.DrawTLatex(x, y, "#sqrt{s} = %s TeV, %s pb^{-1}" % (self.SqrtS, lumiToPrint), self.__Size, 43, align)

    def DrawSqrtS(self, x, y, align=11):
        self.DrawTLatex(x, y, "#sqrt{s} = %s TeV" % (self.SqrtS), self.__Size, 43, align)

    def DrawRegionLabel(self, analysis, channel, x, y, align=11):
        self.DrawTLatex(x, y, channel, self.__Size, 43, align)

    def CreateLegend(self, x1, y1, x2, y2, textsize=22):
        self.__NLegEnt
        if self.__Legend:
            logging.warning("There already exists a legend. Will delete the old one")
            self.__NLegEnt = 0
        self.__Legend = ROOT.TLegend(x1, y1, x2, y2)
        self.__Legend.SetFillStyle(0)
        self.__Legend.SetBorderSize(0)
        self.__Legend.SetTextFont(43)
        self.__Legend.SetTextSize(textsize)
        self.__Legend.SetNColumns(1)
        return self.__Legend

    def DrawPlotLabels(self, xCoord, yCoord, labelName, analysis="analysis", NoATLAS=False, ySpacing=0.08):
        if not NoATLAS:
            self.DrawAtlas(xCoord, yCoord)
            yCoord -= ySpacing
        self.DrawLumiSqrtS(xCoord, yCoord)
        yCoord -= ySpacing
        if len(labelName) > 0:
            self.DrawRegionLabel(analysis, labelName, xCoord, yCoord)

    def GetLegend(self, x1=0, y1=1., x2=1., y2=1.):
        if not self.__Legend: return CreateLegend(x1, y1, x2, y2)
        return self.__Legend

    def AddToLegend(self, Items, Style=""):
        try:
            for I in Items:
                try:
                    Style = I.GetLegendDrawStyle() if Style == "" else Style
                except:
                    pass
                self.__AddItemToLegend(I, Style)
        except:
            try:
                Style = Items.GetLegendDrawStyle() if Style == "" else Style
            except:
                pass
            self.__AddItemToLegend(Items, Style)

    def __AddItemToLegend(self, Item, Style):
        try:
            # if not Item.isTGraph():
            self.__Legend.AddEntry(Item.GetHistogram().get(), Item.GetHistogram().GetTitle(), Style)
        except:
            self.__Legend.AddEntry(Item, Item.GetTitle(), Style)
        self.__NLegEnt += 1

    def DrawLegend(self, NperCol=6, NCol=None):
        if not self.__Legend:
            logging.warning("No legend has been defined yet")
            return
        N = self.__NLegEnt
        Col = int((N - N % NperCol) / NperCol + (N % NperCol > 0))
        if Col < 1: Col = 1
        if NCol: Col = NCol
        self.__Legend.SetNColumns(Col)
        self.__Legend.Draw()

    def Prepare1PadCanvas(self, cname, width=800, height=600, isQuad=False):
        if isQuad: height = width
        if self.__Canvas:
            if self.__Canvas.GetName() != cname: self.__Canvas = None
            else:
                loging.info("Already found a canvas named: " + cname)
                return self.__Canvas
        self.__Canvas = ROOT.TCanvas(cname, cname, width, height)
        self.__Canvas.cd()

    def GetCanvas(self):
        if not self.__Canvas:
            logging.warning("No Canvas has been created yet. Please call CreateXPadCanvas() before")
        return self.__Canvas

    def Prepare2PadCanvas(self, cname, width=800, height=800, isQuad=False):
        self.Prepare1PadCanvas(cname, width, height, isQuad)

        self.__Pad1 = ROOT.TPad("p1_" + cname, cname, 0.0, self.VerticalCanvasSplit - 0.03, 1.0, 1.0)
        self.__Pad2 = ROOT.TPad("p2_" + cname, cname, 0.0, 0.0, 1.0, self.VerticalCanvasSplit - 0.03)

        self.__Pad1.SetBottomMargin(0.03)  # set to 0 for space between top and bottom pad
        self.__Pad1.SetTopMargin(0.09)
        self.__Pad1.Draw()
        self.__Pad2.SetTopMargin(0.01)  # set to 0 for space between top and bottom pad
        self.__Pad2.SetBottomMargin(0.35)
        self.__Pad2.SetGridy()
        self.__Pad2.Draw()

    def GetTopPad(self):
        return self.__Pad1

    def GetBottomPad(self):
        return self.__Pad2

    def PrepareSemiLogBinning(self, histo_ref, splitval, DynRangeTop, minval):

        drawspace_y = 1. - ROOT.gPad.GetTopMargin() - ROOT.gPad.GetBottomMargin()

        split_level = ROOT.gPad.GetBottomMargin() + splitval * drawspace_y

        p_sub_1 = ROOT.TPad("p_sub_1_" + ROOT.gPad.GetName(), ROOT.gPad.GetName(), 0.0, split_level, 1.0, 1.0)
        p_sub_2 = ROOT.TPad("p_sub_2_" + ROOT.gPad.GetName(), ROOT.gPad.GetName(), 0.0, 0.0, 1.0, split_level)

        self.Objects.append(p_sub_1)
        self.Objects.append(p_sub_2)
        # self.Objects.append(p_sub_3)

        p_sub_1.SetBottomMargin(0)
        p_sub_1.SetTopMargin(ROOT.gPad.GetTopMargin() / (1. - split_level))
        p_sub_1.SetBorderMode(0)
        p_sub_1.Draw()
        p_sub_1.SetLogy(False)
        p_sub_2.SetTopMargin(0)
        p_sub_2.SetBottomMargin(ROOT.gPad.GetBottomMargin() / split_level)
        p_sub_2.SetBorderMode(0)
        p_sub_2.SetLogy(True)
        p_sub_2.SetTicks(0, 1)
        p_sub_2.Draw()
        # p_sub_3.SetBottomMargin(0)
        # p_sub_3.SetTopMargin(0.0)
        # p_sub_3.SetBorderMode(0)
        # p_sub_3.Draw()
        h1 = histo_ref.Clone("ForUpper" + histo_ref.GetName())
        h2 = histo_ref.Clone("ForLower" + histo_ref.GetName())
        self.Objects.append(h1)
        self.Objects.append(h2)
        h1.GetYaxis().SetLabelSize(histo_ref.GetYaxis().GetLabelSize() / (1. - split_level))
        h2.GetYaxis().SetLabelSize(histo_ref.GetYaxis().GetLabelSize() / (split_level))
        h1.GetYaxis().SetTitleSize(histo_ref.GetYaxis().GetTitleSize() / (1. - split_level))
        h2.GetYaxis().SetTitleSize(histo_ref.GetYaxis().GetTitleSize() / (split_level))
        h1.GetYaxis().SetTitleOffset(1.1 * histo_ref.GetYaxis().GetTitleOffset() * (1. - split_level))
        h2.GetYaxis().SetTitleOffset(1.1 * histo_ref.GetYaxis().GetTitleOffset() * (split_level))
        h2.GetYaxis().SetTitle("")
        h2.GetYaxis().SetNoExponent()
        #         h2.GetYaxis().SetMoreLogLabels()

        # dynrange = histo_ref.GetMaximum() - histo_ref.GetMinimum()
        split = DynRangeTop * histo_ref.GetMaximum()

        h1.GetYaxis().SetRangeUser(split, histo_ref.GetMaximum())
        h2.GetYaxis().SetRangeUser(minval, split)
        h2.GetYaxis().SetNdivisions(4)

        return [p_sub_1, p_sub_2, h1, h2]

    def CreateSplitTopPad(self, histo_ref, splitval, y_start_upper, y_start_lower, y_end_lower):
        drawspace_y = 1. - ROOT.gPad.GetTopMargin() - ROOT.gPad.GetBottomMargin()
        split_level = ROOT.gPad.GetBottomMargin() + splitval * drawspace_y
        p_sub_1 = ROOT.TPad("p_sub_1_" + ROOT.gPad.GetName(), ROOT.gPad.GetName(), 0.0, split_level, 1.0, 1.0)
        p_sub_2 = ROOT.TPad("p_sub_2_" + ROOT.gPad.GetName(), ROOT.gPad.GetName(), 0.0, 0.0, 1.0, split_level)
        p_sub_3 = ROOT.TPad("p_sub_3_" + ROOT.gPad.GetName(), ROOT.gPad.GetName(), 0.0, split_level - 0.03, 1.0, split_level + 0.03)
        self.Objects.append(p_sub_1)
        self.Objects.append(p_sub_2)
        self.Objects.append(p_sub_3)
        p_sub_1.SetBottomMargin(0)
        p_sub_1.SetTopMargin(0.09 / (1. - split_level))
        p_sub_1.SetBorderMode(0)
        p_sub_1.Draw()
        p_sub_2.SetTopMargin(0)
        p_sub_2.SetBottomMargin(0)
        p_sub_2.SetBorderMode(0)
        p_sub_2.Draw()
        p_sub_3.SetBottomMargin(0)
        p_sub_3.SetTopMargin(0.0)
        p_sub_3.SetBorderMode(0)
        p_sub_3.Draw()
        h1 = histo_ref.Clone("ForUpper" + histo_ref.GetName())
        h2 = histo_ref.Clone("ForLower" + histo_ref.GetName())
        self.Objects.append(h1)
        self.Objects.append(h2)
        h1.GetYaxis().SetLabelSize(histo_ref.GetYaxis().GetLabelSize() / (1. - split_level))
        h2.GetYaxis().SetLabelSize(histo_ref.GetYaxis().GetLabelSize() / (split_level))
        h1.GetYaxis().SetTitleSize(histo_ref.GetYaxis().GetTitleSize() / (1. - split_level))
        h2.GetYaxis().SetTitleSize(histo_ref.GetYaxis().GetTitleSize() / (split_level))
        h1.GetYaxis().SetTitleOffset(1.1 * histo_ref.GetYaxis().GetTitleOffset() * (1. - split_level))
        h2.GetYaxis().SetTitleOffset(1.1 * histo_ref.GetYaxis().GetTitleOffset() * (split_level))
        h2.GetYaxis().SetTitle("")
        pad = ROOT.gPad

        p_sub_3.cd()
        l1 = ROOT.TLine()
        l1.SetLineWidth(1)
        l1.DrawLineNDC(p_sub_3.GetLeftMargin(), 0, p_sub_3.GetLeftMargin(), 0.33)
        l1.DrawLineNDC(p_sub_3.GetLeftMargin(), 0.66, p_sub_3.GetLeftMargin(), 1)
        l1.SetLineWidth(1)
        l1.DrawLineNDC(p_sub_3.GetLeftMargin() - 0.02, 0.66 - 0.25, p_sub_3.GetLeftMargin() + 0.02, 0.66 + 0.25)
        l1.DrawLineNDC(p_sub_3.GetLeftMargin() - 0.02, 0.33 - 0.25, p_sub_3.GetLeftMargin() + 0.02, 0.33 + 0.25)

        l1.SetLineWidth(1)
        l1.DrawLineNDC(1. - p_sub_3.GetRightMargin(), 0, 1. - p_sub_3.GetRightMargin(), 0.33)
        l1.DrawLineNDC(1. - p_sub_3.GetRightMargin(), 0.66, 1. - p_sub_3.GetRightMargin(), 1)
        l1.SetLineWidth(1)
        l1.DrawLineNDC(1. - p_sub_3.GetRightMargin() - 0.02, 0.66 - 0.25, 1. - p_sub_3.GetRightMargin() + 0.02, 0.66 + 0.25)
        l1.DrawLineNDC(1. - p_sub_3.GetRightMargin() - 0.02, 0.33 - 0.25, 1. - p_sub_3.GetRightMargin() + 0.02, 0.33 + 0.25)

        pad.cd()

        h1.GetYaxis().SetRangeUser(y_start_upper, h1.GetMaximum())
        h2.GetYaxis().SetRangeUser(y_start_lower, y_end_lower)
        h2.GetYaxis().SetNdivisions(4)

        return [p_sub_1, p_sub_2, h1, h2]

    def GetMaxYSpace(self, h):

        ymax = 1.
        ymin = 0.
        if h.InheritsFrom("TGraphAsymmErrors"):
            ymax = max([h.GetY()[i] for i in range(h.GetN())])
            ymin = min([h.GetY()[i] for i in range(h.GetN())])
        else:
            ymax = h.GetMaximum()
            ymin = h.GetMinimum()

        yrange = ymax - ymin

        besty1 = 0.
        besty2 = 0.
        deltamax = 0.

        nsamples = 20.
        for y1 in range(int(nsamples)):
            level_1 = ymin + yrange / nsamples * y1
            for y2 in range(int(nsamples)):
                level_2 = level_1 + (ymax - level_1) / nsamples * y2
                valid = True
                for p in range(GetNbins(h)):
                    y = 0.
                    if h.InheritsFrom("TGraphAsymmErrors"):
                        y = h.GetY()[p]
                    else:
                        # skip underflow
                        if p == 0:
                            continue
                        y = h.GetBinContent(p)

                    if y < level_2 and y > level_1:
                        valid = False
                        break
                if valid:
                    delta = level_2 - level_1
                    if delta > deltamax:
                        besty1 = level_1
                        besty2 = level_2
                        deltamax = delta
        # print "Largest hole is %.2f between %.4f and %.4f"%(deltamax,besty1,besty2)
        if deltamax > 0.6 * yrange and besty1 < 0.9 and yrange > 0.15:
            # print "I will split the canvas"
            return [besty1, besty2]
        else:
            return []

    def GetMaxYSpaceList(self, li):
        ymax = 1.
        ymin = 0.
        for h in li:
            edges = self.GetMaxYSpace(h)
            if len(edges) == 0:
                ys = [h.GetY()[p] for p in range(GetNbins(h))]
                if min(ys) < ymax:
                    ymax = min(ys)
            else:
                if edges[0] > ymin:
                    ymin = edges[0]
                if edges[1] < ymax:
                    ymax = edges[1]
        if ymin < 0.2:
            return []
        return [ymin, ymax]

    def AdaptLabelsTopPad(self, histos):
        labelscalefact = 1. / (1. - self.VerticalCanvasSplit)
        for hist in histos:
            hist.GetXaxis().SetTitleSize(labelscalefact * hist.GetXaxis().GetTitleSize())
            hist.GetYaxis().SetTitleSize(labelscalefact * hist.GetYaxis().GetTitleSize())
            hist.GetXaxis().SetLabelSize(labelscalefact * hist.GetXaxis().GetLabelSize())
            hist.GetYaxis().SetLabelSize(labelscalefact * hist.GetYaxis().GetLabelSize())
            hist.GetXaxis().SetTitleOffset(1. / labelscalefact * hist.GetXaxis().GetTitleOffset())
            hist.GetYaxis().SetTitleOffset(1. / labelscalefact * hist.GetYaxis().GetTitleOffset())

    def AdaptLabelsBottomPad(self, histos, scale=1.):
        if self.VerticalCanvasSplit == 0:
            labelscalefact = 1.
        else:
            labelscalefact = 1. / (scale * self.VerticalCanvasSplit)
        for hist in histos:
            hist.GetXaxis().SetTitleSize(labelscalefact * hist.GetXaxis().GetTitleSize())
            hist.GetYaxis().SetTitleSize(labelscalefact * hist.GetYaxis().GetTitleSize())
            hist.GetXaxis().SetLabelSize(labelscalefact * hist.GetXaxis().GetLabelSize())
            hist.GetYaxis().SetLabelSize(labelscalefact * hist.GetYaxis().GetLabelSize())
            hist.GetXaxis().SetTitleOffset(2.2 / labelscalefact * hist.GetXaxis().GetTitleOffset())
            hist.GetYaxis().SetTitleOffset(1.05 / labelscalefact * hist.GetYaxis().GetTitleOffset())

    def GetFancyAxisRanges(self, Samples, Options, doRatio=False):
        if doRatio and len(Options.LowerPadRanges) == 2:
            return Options.LowerPadRanges[0], Options.LowerPadRanges[1]

        elif len(Options.UpperPadRanges) == 2:
            return Options.UpperPadRanges[0], Options.UpperPadRanges[1]

        if len(Samples) == 0:
            logging.warning("Shit no samples are given.. Richard Paker attacks")
            return 0, 0
        x_range = GetPlotRangeX(Samples[0])
        if x_range == 0.:
            logging.warning("Invalid histogram given")
            return 0, 0

        ymin = 1.e25
        ymax = -1.e25
        bin_contents = [(1, -1.e25), (1, -1.e25), (1, -1.e25)]

        for Histo in Samples:
            if not doRatio:
                ymax = max([0, Histo.GetMaximum(), ymax])
                ymin = min([0 if not Options.doLogY else Histo.GetMinimum(1.e-3), ymin])
                ## take the respective maxima at three points across the range
                low = self._maxInSector(Histo, 1, Histo.GetNbinsX() / 3)
                mid = self._maxInSector(Histo, Histo.GetNbinsX() / 3 + 1, 2 * Histo.GetNbinsX() / 3)
                high = self._maxInSector(Histo, 2 * Histo.GetNbinsX() / 3 + 1, Histo.GetNbinsX())
                if low[1] > bin_contents[0][1]: bin_contents[0] = low
                if mid[1] > bin_contents[1][1]: bin_contents[1] = mid
                if high[1] > bin_contents[2][1]: bin_contents[2] = high
            else:

                max_val = max([Histo.GetBinContent(i) for i in range(1, Histo.GetNbinsX())])
                min_val = min([Histo.GetBinContent(i) for i in range(1, Histo.GetNbinsX())])

                #mean = sum([Histo.GetBinContent(i) for i in range(1, Histo.GetNbinsX())]) / Histo.GetNbinsX()
                #sigma2 = sum([Histo.GetBinContent(i)**2 for i in range(1, Histo.GetNbinsX())]) / Histo.GetNbinsX() - mean**2
                #sigma = math.sqrt(sigma2)
                #ymax = max(mean + 1.66 * sigma, ymax)
                #y_min_band = max(mean - 1.66 * sigma, 0)
                ymax = max(max_val * 1.05, ymax)
                ymin = min(min_val * 0.95, ymin)

        if ymax == 0:
            logging.warning("PlotUtils: WHAAT?! Empty histos?")
            return 0, 0

        if not doRatio:
            ##### check shape of histogram for legend position
            if Options.doLogY:
                ymin = max(math.fabs(ymin)**0.5, 1e-4)
                ymax = 1e3 * ymax
            else:
                ### Formula to calculate the slope of the three points
                ##     D = N *sum (x*x) - sum( x) ** 2
                ##     t = ( sum(x*x)* sum(y) - sum(x) * sum(x*y) ) / D
                ##     m = (N*sum(x*y) - sum(x)sum(y) ) / D
                N = len(bin_contents)
                X = [x[0] for x in bin_contents]
                Y = [y[1] for y in bin_contents]
                D = N * sum([x**2 for x in X]) - sum(X)**2
                m = (N * sum([bin_contents[i][0] * bin_contents[i][1] for i in range(N)]) - sum(X) * sum(Y)) / D
                t = (sum([x**2 for x in X]) * sum(Y) - sum(X) * sum([bin_contents[i][0] * bin_contents[i][1] for i in range(N)])) / D
                ### The distribution peaks on the left hand-site of the spectrum
                if m < 0.:
                    ymax *= 1.5
                else:
                    ymax *= 1.7
        else:
            ymax = min(ymax, 4.)
        if hasattr(Options, 'ShapeComp') and Options.ShapeComp:
            ymax *= 0.5
        return ymin, ymax

    def _maxInSector(self, histo, start, end):
        max_bin = start
        for i in range(start, end):
            if histo.GetBinContent(max_bin) < histo.GetBinContent(i): max_bin = i
        return histo.GetXaxis().GetBinCenter(max_bin) / GetPlotRangeX(histo), histo.GetBinContent(max_bin)

    def drawStyling(self, Template, ymin, ymax, TopPad=True, RemoveLabel=False):
        try:
            self.__Styling = Template.GetHistogram().Clone("Style" + Template.GetHistogram().GetName())
        except:
            self.__Styling = Template.Clone("Style" + Template.GetName())
        self.__Styling.Reset()
        #        ReplaceStringInHistoAxis(self.__Styling)

        self.__Styling.SetMinimum(ymin)
        self.__Styling.SetMaximum(ymax)

        if RemoveLabel == True:
            self.__Styling.GetXaxis().SetLabelOffset(10.)
        if TopPad:
            self.AdaptLabelsTopPad([self.__Styling])
        self.__Styling.Draw("AXIS")

    def drawRatioStyling(self, Template, ymin, ymax, RatioLabel="Data / MC", NDivY_l1=7, NDivY_l2=5):
        try:
            self.__RatStyling = Template.GetHistogram().Clone("RatioStyle" + Template.GetHistogram().GetName())
        except:
            self.__RatStyling = Template.Clone("RatioStyle" + Template.GetName())
        self.__RatStyling.Reset()
        #        ReplaceStringInHistoAxis(self.__RatStyling)

        self.__RatStyling.SetMinimum(max(ymin, 0.))
        self.__RatStyling.SetMaximum(ymax)
        self.__RatStyling.GetYaxis().SetNdivisions(NDivY_l1, NDivY_l2, 0)

        self.__RatStyling.GetYaxis().SetTitle(RatioLabel)
        self.AdaptLabelsBottomPad([self.__RatStyling])
        self.__RatStyling.GetYaxis().SetMaxDigits(3)
        self.__RatStyling.Draw("AXIS")
        l = GetHorizontalLine(Template, 1., "L", ROOT.kDashed)
        l.Draw("sameHIST")
        l2 = GetHorizontalLine(Template, 1 - 2. / 3. * (ymax - 1), "L2", ROOT.kDotted)
        l2.Draw("sameHIST")
        l3 = GetHorizontalLine(Template, 1 + 2. / 3. * (ymax - 1), "L3", ROOT.kDotted)
        l3.Draw("sameHIST")
        self.__Objects.extend([l, l2, l3])

    def drawRatioErrors(self, SumBG, DrawSyst=False, DrawSysLegend=False):
        if DrawSyst:
            SystDn, SystUp = SumBG.GetSystErrorBands()
            RatSystDn = SystDn.Clone("RatSystDn_" + SystDn.GetName())
            RatSystUp = SystUp.Clone("RatSystDn_" + SystUp.GetName())
            self.__Objects += [RatSystDn, RatSystUp]
        try:
            Stat = SumBG.GetHistogram().Clone("RatStat_" + SumBG.GetHistogram().GetName())
        except AttributeError:
            Stat = SumBG.Clone("RatStat_" + SumBG.GetName())
        Stat.SetMarkerStyle(1)
        Stat.SetMarkerSize(0)
        self.__Objects.append(Stat)
        for i in range(0, Stat.GetNbinsX() + 1):
            if Stat.GetBinContent(i) > 0:
                if DrawSyst:
                    RatSystDn.SetBinContent(i, RatSystDn.GetBinContent(i) / Stat.GetBinContent(i))
                    RatSystUp.SetBinContent(i, RatSystUp.GetBinContent(i) / Stat.GetBinContent(i))
                Stat.SetBinError(i, Stat.GetBinError(i) / Stat.GetBinContent(i))
            else:
                if DrawSyst:
                    RatSystDn.SetBinContent(i, 1.)
                    RatSystUp.SetBinContent(i, 1.)
                Stat.SetBinError(i, 1.)
            Stat.SetBinContent(i, 1)

        Stat.SetFillColor(1)
        Stat.SetLineColor(1)
        Stat.SetMarkerStyle(1)
        Stat.SetMarkerSize(1)
        Stat.SetFillStyle(3354)
        Stat.Draw("sameE2")
        Stat.SetTitle("MC Stat")

        SysLegendHisto = None
        if DrawSyst:
            GraphSyst, GraphSystStat = self.__CreateAsymmGraphs(Stat, RatSystDn, RatSystUp)
            y_min = max([GraphSystStat.GetEYlow()[i] for i in range(GraphSystStat.GetN())])
            y_max = max([GraphSystStat.GetEYhigh()[i] for i in range(GraphSystStat.GetN())])
            self.__RatStyling.SetMaximum(max(self.__RatStyling.GetMaximum(), min(4., (1. + y_max) * 1.05)))
            self.__RatStyling.SetMinimum(min(self.__RatStyling.GetMinimum(), max(0., (1. - y_min) * 0.95)))

            GraphSystStat.SetMarkerStyle(1)
            GraphSystStat.SetMarkerSize(0)
            GraphSystStat.SetFillColorAlpha(ROOT.kBlack, 0.2)
            GraphSystStat.SetFillStyle(3244)

            GraphSystStat.Draw("sameE2")
            GraphSystStat.SetTitle("MC Sys #oplus Stat")
            SysLegendHisto = GraphSystStat.GetHistogram()
            SysLegendHisto.SetFillColorAlpha(ROOT.kBlack, 0.2)
            SysLegendHisto.SetFillStyle(3244)
            self.__Objects += [SysLegendHisto, GraphSystStat]
        if DrawSysLegend and SysLegendHisto:  # to be decided: what happens for stat only?
            ### Global numbers
            x_leg_min = 0.35
            x_leg_max = 0.75
            y_leg_min = 0.38
            y_leg_max = 0.45

            lege = ROOT.TLegend(x_leg_min, y_leg_min, x_leg_max, y_leg_max)
            self.__Objects.append(lege)
            lege.SetFillStyle(0)
            lege.SetBorderSize(0)
            lege.SetTextFont(43)
            lege.SetTextSize(24)
            lege.AddEntry(Stat, Stat.GetTitle(), "F")
            lege.AddEntry(SysLegendHisto, SysLegendHisto.GetTitle(), "F")
            lege.SetNColumns(2)
            lege.SetColumnSeparation(0.002)
            lege.Draw()

    def drawRatioSignificanceErrors(self, signal, background):
        try:
            Sig = signal.GetHistogram().Clone("RatStatSignal_" + signal.GetHistogram().GetName())
        except AttributeError:
            Sig = signal.Clone("RatStatSignal_" + signal.GetName())
        try:
            Bkg = background.GetHistogram().Clone("RatStat_" + background.GetHistogram().GetName())
        except AttributeError:
            Bkg = background.Clone("RatStat_" + background.GetName())
        Sig.SetMarkerStyle(1)
        Sig.SetMarkerSize(0)
        self.__Objects.append(Sig)
        for i in range(0, Sig.GetNbinsX() + 1):
            if Bkg.GetBinContent(i) > 0:
                Sig.SetBinError(i, SoverBerror(Sig.GetBinContent(i), Bkg.GetBinContent(i), Sig.GetBinError(i), Bkg.GetBinError(i)))
            else:
                Sig.SetBinError(i, 1.)
            Sig.SetBinContent(i, 1)

        Sig.SetFillColor(1)
        Sig.SetLineColor(1)
        Sig.SetMarkerStyle(1)
        Sig.SetMarkerSize(1)
        Sig.SetFillStyle(3354)
        Sig.Draw("sameE2")
        Sig.SetTitle("MC Stat")

    # This Function shall Draw the SumBG with Systematic variations
    def drawSumBg(self, SumBG, DrawSyst=False, DrawOnStack=True):
        h_SumBg = SumBG.GetHistogram().Clone("Style of" + SumBG.GetHistogram().GetName())
        if not DrawOnStack:
            h_SumBg.SetMarkerColor(h_SumBg.GetLineColor())
            h_SumBg.SetLineColor(ROOT.kGray + 3)
        h_SumBg.SetLineWidth(2)
        h_SumBg.DrawCopy("HISTsame")
        if DrawSyst:
            SystDn, SystUp = SumBG.GetSystErrorBands()
            GraphSyst, GraphSystStat = self.__CreateAsymmGraphs(SumBG.GetHistogram(), SystDn, SystUp)
            GraphSystStat.SetMarkerStyle(1)
            GraphSystStat.SetMarkerSize(0)
            GraphSystStat.SetMarkerColor(ROOT.kWhite)
            GraphSystStat.SetFillColorAlpha(ROOT.kBlack, 0.1)
            GraphSystStat.SetFillStyle(3244)
            GraphSystStat.Draw("sameE2")
            self.__Objects += [GraphSystStat]
        h_SumBg.SetFillColor(1)
        h_SumBg.SetMarkerStyle(1)
        h_SumBg.SetMarkerSize(1)
        h_SumBg.SetFillStyle(3354)
        h_SumBg.Draw("sameE2")
        self.__Objects += [h_SumBg]

    def __CreateAsymmGraphs(self, Central, Dn, Up):
        Syst = ROOT.TGraphAsymmErrors()
        SystStat = ROOT.TGraphAsymmErrors()
        self.__Objects += [SystStat, Syst]
        for i in range(0, GetNbins(Central)):
            self.__CreateGraphPoint(Syst, Central, i)
            self.__CreateGraphPoint(SystStat, Central, i)
            Syst.SetPointEYhigh(Syst.GetN() - 1, Up.GetBinContent(i))
            Syst.SetPointEYlow(Syst.GetN() - 1, Dn.GetBinContent(i))
            StatError = Central.GetBinError(i)**2
            SystStatUp = math.sqrt(StatError + Up.GetBinContent(i)**2)
            SystStatDn = math.sqrt(StatError + Dn.GetBinContent(i)**2)
            SystStat.SetPointEYhigh(SystStat.GetN() - 1, SystStatUp)
            SystStat.SetPointEYlow(SystStat.GetN() - 1, SystStatDn)
        return Syst, SystStat

    def __CreateGraphPoint(self, Graph, Histo, bin):
        LowEdge = Histo.GetXaxis().GetBinLowEdge(bin)
        Central = Histo.GetXaxis().GetBinCenter(bin)
        HighEdge = Histo.GetXaxis().GetBinUpEdge(bin)
        Graph.SetPoint(Graph.GetN(), Central, Histo.GetBinContent(bin))
        Graph.SetPointEXhigh(Graph.GetN() - 1, HighEdge - Central)
        Graph.SetPointEXlow(Graph.GetN() - 1, Central - LowEdge)

    def saveHisto(self, name, outputfiletypes):
        ### split the name into out_dir and file_name
        Path = RemoveSpecialChars(name)
        if name.find("/") != -1:
            out_dir = name[:name.rfind("/") + 1]
            file_name = name[name.rfind("/") + 1:]
            Path = out_dir + RemoveSpecialChars(file_name)
        if "png" in outputfiletypes:
            # need a 'pdf' file for using 'convert' to create a 'png'
            if not "pdf" in outputfiletypes:
                outputfiletypes.append("pdf")
            a, b = outputfiletypes.index('pdf'), outputfiletypes.index('png')
            outputfiletypes[b], outputfiletypes[a] = outputfiletypes[a], outputfiletypes[b]
        for otype in outputfiletypes:
            if otype != "png":
                self.__Canvas.SaveAs("%s.%s" % (Path, otype))
            else:
                # due to problems in png creation, convert pdfs to pngs manually
                os.system("convert -density 300 %s.pdf %s.png" % (Path, Path))

    def CreateHaloHistogram(self, Histo):
        Histo.SetMarkerStyle(ROOT.kFullDotLarge)
        Histo.SetMarkerColor(Histo.GetLineColor())
        Halo = Histo.Clone("%s_Halo" % (Histo.GetName()))
        Halo.SetMarkerColorAlpha(ROOT.kWhite, 0.7)
        Halo.SetLineColorAlpha(ROOT.kWhite, 0.7)
        Halo.SetMarkerSize(Halo.GetMarkerSize() * 1.25)
        Halo.SetLineWidth(Halo.GetLineWidth() + 2)
        return Halo


class Stack(object):
    def __init__(self, Histograms, SetFillColour=True):
        self.__StackedH = []
        for h in range(1, len(Histograms) + 1):
            self.Add(Histograms[-h], SetFillColour)

    def Draw(self, Opt):
        DrOpt = Opt
        # We want to draw the last histogram of the stack first followed by the rest
        for i in range(0, len(self.__StackedH)):
            self.__StackedH[i].Draw(DrOpt)
            if "same" not in DrOpt and "SAME" not in DrOpt:
                DrOpt = "same" + DrOpts

    def GetNbinsX(self):
        return self.GetHistogram().GetNbinsX() if self.GetHistogram() else None

    def GetXaxis(self):
        return self.GetHistogram().GetXaxis() if self.GetHistogram() else None

    def GetBinContent(self, i):
        return self.GetHistogram().GetBinContent(i) if self.GetHistogram() else -1.e23

    def GetHistogram(self):
        if len(self.__StackedH) == 0:
            logging.warning("No histograms are in the Stack defined yet")
            return None
        return self.__StackedH[-1]

    def Add(self, Histo, SetFillColour):
        ToStack = None
        try:
            ToStack = Histo.GetHistogram().Clone("ToStack_" + Histo.GetHistogram().GetName())
        except:
            ToStack = Histo.Clone("ToStack_" + Histo.GetName())
        if SetFillColour == True:
            ToStack.SetFillColor(ToStack.GetLineColor())
        ToStack.SetMarkerSize(0)
        for AlStck in self.__StackedH:
            AddHistos(AlStck, ToStack)
        self.__StackedH.append(ToStack)

    def GetHistogramStack(self):
        return self.__StackedH


def draw2DHisto(HistoSets, Options, DrawData=True, PlotType="MC", SummaryFile="AllMCPlots"):

    ROOT.gStyle.SetPaintTextFormat(".3f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetPalette(ROOT.kAvocado)

    pu = PlotUtils(status=Options.label)
    pu.Size = 24
    ToDraw = []

    DefaultSet = HistoSets[0]
    pu.Lumi = DefaultSet.GetLumi()

    if not Options.skipBkgComposition2D: ToDraw += DefaultSet.GetBackgrounds()
    ToDraw += DefaultSet.GetSignals()
    if Options.skipBkgComposition2D or len(DefaultSet.GetBackgrounds()) > 1:
        ToDraw.append(DefaultSet.GetSummedBackground())
    if DrawData == True: ToDraw += DefaultSet.GetData()

    Drawn = False
    for Sample in ToDraw:
        if not CheckHisto(Sample):
            continue
        PlotName = RemoveSpecialChars("%s_2D_%s_%s_%s_%s" %
                                      (PlotType, Sample.GetName(), Sample.GetAnalysis(), Sample.GetRegion(), Sample.GetVariableName()))
        pu.Prepare1PadCanvas(PlotName, 800, 600, Options.quadCanvas)
        can = pu.GetCanvas()
        if Sample.GetMaximum() < 1.e-12: continue
        histo = Sample.GetHistogram()
        histo.GetZaxis().SetTitleOffset(1.2 * histo.GetZaxis().GetTitleOffset())
        histo.GetYaxis().SetTitleOffset(0.8 * histo.GetYaxis().GetTitleOffset())
        histo.GetZaxis().SetLabelSize(histo.GetYaxis().GetLabelSize())
        histo.GetZaxis().SetTitleSize(histo.GetYaxis().GetTitleSize())
        histo.GetZaxis().SetTitle("Fraction of events")
        if len(histo.GetYaxis().GetTitle()) > 30:
            histo.GetYaxis().SetTitleSize(0.86 * histo.GetYaxis().GetTitleSize())
            histo.GetYaxis().SetTitleOffset(1.15 * histo.GetYaxis().GetTitleOffset())
        if Options.doLogY: can.SetLogz()
        histo.SetLineColor(ROOT.kBlack)
        histo.SetContour(2000)
        histo.Draw("colz l" if not Options.show2DText else "colz texte")

        can.SetTopMargin(0.15)
        can.SetRightMargin(0.18)
        can.SetLeftMargin(0.12)
        if not Options.noATLAS:
            pu.DrawAtlas(can.GetLeftMargin(), 0.92)
        pu.DrawLumiSqrtS(can.GetLeftMargin(), 0.87)
        pu.DrawTLatex(0.4, 0.87, Sample.GetTitle())
        if Options.regionLabel != '':
            pu.DrawRegionLabel("analysis", Options.regionLabel, 0.6, 0.92)
        logstr = "" if not Options.doLogY else "_LogY"
        if not Options.summaryPlotOnly: pu.saveHisto("%s/%s%s" % (Options.outputDir, PlotName, logstr), Options.OutFileType)
        pu.DrawTLatex(0.5, 0.965,
                      "%s in %s (%s, %s analysis)" % (Sample.GetVariableName(), Sample.GetRegion(), Sample.GetName(), Sample.GetAnalysis()),
                      0.03, 52, 22)
        pu.saveHisto("%s/%s_%s_%s%s" % (Options.outputDir, SummaryFile, Sample.GetAnalysis(), Sample.GetRegion(), logstr), ["pdf"])
        Drawn = True
    return Drawn
