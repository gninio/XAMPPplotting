#! /usr/bin/env python
import logging
################################################################################################################
# ## functions to draw histogram labels in dependence of the actual signal-model.                              #
# ## this can be used for plots over a signalgrid where the axis labels depend on the actual signalsamples.    #
# ## Be aware to name the signalsamples properly in the DSconfig.                                              #
# ## Feel free to extend the dictionaries.                                                                     #
################################################################################################################

MassDic = {}
MassDic["GG_LLE"] = {}
MassDic["GG_LLE"][1] = "m_{#tilde{g}} [GeV]"
MassDic["GG_LLE"][2] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"
MassDic["GG_N2_SLN1"] = {}
MassDic["GG_N2_SLN1"][4] = "m_{#tilde{g}} [GeV]"
MassDic["GG_N2_SLN1"][5] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"
MassDic["C1C1_LLE"] = {}
MassDic["C1C1_LLE"][1] = "m_{#tilde{#chi}^{#pm}_{1}} [GeV]"
MassDic["C1C1_LLE"][2] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"
MassDic["TT_onestepBB"] = {}
MassDic["TT_onestepBB"][2] = "m_{#tilde{t}} [GeV]"
MassDic["TT_onestepBB"][3] = "m_{#tilde{#chi}^{#pm}_{1}} [GeV]"
MassDic["TT_onestepBB"][4] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"
MassDic["TT_directTT"] = {}
MassDic["TT_directTT"][3] = "m_{#tilde{t}} [GeV]"
MassDic["TT_directTT"][4] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"
MassDic["LV_LLE"] = {}
MassDic["LV_LLE"][1] = "m_{#tilde{l}^{#pm}}/ m_{#tilde{#nu}} [GeV]"
MassDic["LV_LLE"][2] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"

MassDic["C1N2_LLE"] = {}
MassDic["C1N2_LLE"][1] = "m_{#tilde{#chi}^{#pm}_{1}}/ m_{#tilde{#chi}^{0}_{2}} [GeV]"
MassDic["C1N2_LLE"][2] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"

MassDic["Wino_LLE"] = {}
#MassDic["Wino_LLE"][1] = "m_{#tilde{#chi}^{#pm}_{1}}/ m_{#tilde{#chi}^{0}_{2}} [GeV]"
MassDic["Wino_LLE"][1] = "m_{#tilde{W}} [GeV]"
MassDic["Wino_LLE"][2] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"

MassDic["VVz_LLE"] = {}
#MassDic["VVz_LLE"][1] = "m_{#tilde{#chi}^{#pm}_{1}}/ m_{#tilde{#chi}^{0}_{2}} [GeV]"
MassDic["VVz_LLE"][1] = "m_{#tilde{W}} [GeV]"
MassDic["VVz_LLE"][2] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"

MassDic["VVh_LLE"] = {}
#MassDic["VVh_LLE"][1] = "m_{#tilde{#chi}^{#pm}_{1}}/ m_{#tilde{#chi}^{0}_{2}} [GeV]"
MassDic["VVh_LLE"][1] = "m_{#tilde{W}} [GeV]"
MassDic["VVh_LLE"][2] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"

MassDic["GGM_Higgsino"] = {}
MassDic["GGM_Higgsino"][1] = "m_{#tilde{H}} [GeV]"
MassDic["GGM_Higgsino"][2] = "BR(#tilde{#chi}^{0}_{1}#rightarrow Z#tilde{G})"

MassDic["StauStau"] = {}
MassDic["StauStau"][1] = "m_{#tilde{#tau}} [GeV]"
MassDic["StauStau"][2] = "m_{#tilde{#chi}^{0}_{1}} [GeV]"

ProcessDic = {
    "GG_LLE": "pp#rightarrow#tilde{g}#tilde{g}; #tilde{g}#rightarrowqq#tilde{#chi}^{0}_{1}; #tilde{#chi}^{0}_{1}#rightarrowl^{+}l^{-}#nu",
    "GG_N2_SLN1": "pp#rightarrow#tilde{g}#tilde{g}; #tilde{g}#rightarrowqq(ll/#nu#nu)#tilde{#chi}^{0}_{1}",
    "C1C1_LLE":
    "pp#rightarrow#tilde{#chi}^{+}_{1}#tilde{#chi}^{-}_{1}#rightarrowW^{+}#tilde{#chi}^{0}_{1}W^{-}#tilde{#chi}_{1}^{0}; #tilde{#chi}^{0}_{1} #rightarrowl^{+}l^{-}#nu",
    "TT_onestepBB":
    "pp#rightarrow#tilde{t}#tilde{t}; #tilde{t}#rightarrow#tilde{#chi}^{#pm}_{1}+b; #tilde{#chi}^{#pm}_{1}#rightarrow#tilde{#chi}^{0}_{1}+t",
    "TT_directTT": "pp#rightarrow#tilde{t}#tilde{t}; #tilde{t}#rightarrow#tilde{#chi}^{0}_{1}+t; t#rightarrow b+W",
    "Wino_LLE":
    "pp#rightarrow#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{#mp}_{1}/#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{0}_{2}; #tilde{#chi}_{1}^{#pm}#rightarrow W^{#pm}#tilde{#chi}_{1}^{0}; #tilde{#chi}^{0}_{2}#rightarrow#tilde{#chi}_{1}^{0}Z/h; #tilde{#chi}^{0}_{1} #rightarrowl^{+}l^{-}#nu",
    "VVz_LLE":
    "pp#rightarrow#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{#mp}_{1}/#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{0}_{2}; #tilde{#chi}_{1}^{#pm}#rightarrow W^{#pm}#tilde{#chi}_{1}^{0}; #tilde{#chi}^{0}_{2}#rightarrow#tilde{#chi}_{1}^{0}Z; #tilde{#chi}^{0}_{1} #rightarrowl^{+}l^{-}#nu",
    "VVh_LLE":
    "pp#rightarrow#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{#mp}_{1}/#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{0}_{2}; #tilde{#chi}_{1}^{#pm}#rightarrow W^{#pm}#tilde{#chi}_{1}^{0}; #tilde{#chi}^{0}_{2}#rightarrow#tilde{#chi}_{1}^{0}h; #tilde{#chi}^{0}_{1} #rightarrowl^{+}l^{-}#nu",
    "LV_LLE":
    "pp#rightarrow#tilde{#nu}#tilde{#nu}/#tilde{l}^{#pm}#tilde{#nu}/#tilde{l}^{#pm}#tilde{l}^{#mp}; #tilde{l}^{#pm}#rightarrow #tilde{#chi}_{1}^{0}l^{#pm};  #tilde{#nu}#rightarrow#tilde{#chi}_{1}^{0}#nu; #tilde{#chi}^{0}_{1} #rightarrowl^{+}l^{-}#nu",
    "GGM_Higgsino":
    "pp#rightarrow#tilde{#chi}^{+}_{1}#tilde{#chi}^{-}_{1}/#tilde{#chi}^{#pm}_{1}#tilde{#chi}^{0}_{2}; #tilde{#chi}^{#pm}_{1}#rightarrow #tilde{#chi}^{0}_{1}W^{#pm}; #tilde{#chi}^{0}_{2}#rightarrow#tilde{#chi}^{0}_{1}Z^{*}; #tilde{#chi}^{0}_{1}#rightarrow#tilde{G} Z/h",
    "StauStau": "pp#rightarrow#tilde{#tau}#tilde{#tau}, #tilde{#tau}#rightarrow#tau#tilde{#chi}^{0}_{1}"
}


def GetLabelKey(Parametervektor):
    if isinstance(Parametervektor, str): Parametervektor = Parametervektor.split("_")
    #############################################################################################################################
    # ## this function reads the name of the first signalsample in the given DSconfig and returns the key for the dictionaries. #
    # ## Feel free to extend the list.                                                                                          #
    #############################################################################################################################
    if "GG" in Parametervektor and len([x for x in Parametervektor if str(x).find("LLE") != -1]):  #RPV-Gluino
        LabelKey = "GG_LLE"
    elif "GG" in Parametervektor and "N2" in Parametervektor and "SLN1" in Parametervektor:  #RPC-Gluino
        LabelKey = "GG_N2_SLN1"
    elif "C1C1" in Parametervektor and len([x for x in Parametervektor if str(x).find("LLE") != -1]):  #RPV-Chargino
        LabelKey = "C1C1_LLE"
    elif "Wino" in Parametervektor and len([x for x in Parametervektor if str(x).find("LLE") != -1]):  #RPV-Chargino
        LabelKey = "Wino_LLE"
    elif "VVz" in Parametervektor and len([x for x in Parametervektor if str(x).find("LLE") != -1]):  ### RPV wino model with ninotwo->Z
        LabelKey = "VVz_LLE"
    elif "VVh" in Parametervektor and len([x for x in Parametervektor if str(x).find("LLE") != -1]):  ### RPV wino model with ninotwo->h
        LabelKey = "VVh_LLE"
    elif "GGMHinoZh50" in Parametervektor or "GGM" in Parametervektor:
        LabelKey = "GGM_Higgsino"
    elif "TT" in Parametervektor and "onestepBB" in Parametervektor:
        LabelKey = "TT_onestepBB"
    elif "TT" in Parametervektor and "directTT" in Parametervektor:
        LabelKey = "TT_directTT"
    elif ("VVz" in Parametervektor or "C1N2" in Parametervektor) and len([x for x in Parametervektor if str(x).find("LLE") != -1]):
        return "C1N2_LLE"
    elif "LV" in Parametervektor and len([x for x in Parametervektor if str(x).find("LLE") != -1]):
        return "LV_LLE"
    elif "StauStau" in Parametervektor:
        return "StauStau"
    else:
        LabelKey = ""
        logging.warning("Unknown model %s" % (Parametervektor))
    return LabelKey


def DrawXaxisMassLabel(Histo, i, Parametervektor):
    if GetLabelKey(Parametervektor) in MassDic.iterkeys():
        if i in MassDic[GetLabelKey(Parametervektor)].iterkeys():
            Histo.GetXaxis().SetTitle(MassDic[GetLabelKey(Parametervektor)][i])


def DrawYaxisMassLabel(Histo, j, Parametervektor):
    if GetLabelKey(Parametervektor) in MassDic.iterkeys():
        if j in MassDic[GetLabelKey(Parametervektor)].iterkeys():
            Histo.GetYaxis().SetTitle(MassDic[GetLabelKey(Parametervektor)][j])


def GetParticleName(Histo, i, Parametervektor):
    if GetLabelKey(Parametervektor) in MassDic.iterkeys():
        if i in MassDic[GetLabelKey(Parametervektor)].iterkeys():
            return MassDic[GetLabelKey(Parametervektor)][i]
        else:
            return "_-4_"
    else:
        return "_-4_"


def DrawProcessLabel(PlotUtil, Parametervektor, x=0.6, y=0.88, size=0.03, font=52):
    #Draws the actual model. Default position is at the top of the histogram.
    if GetLabelKey(Parametervektor) in ProcessDic:
        PlotUtil.DrawTLatex(x, y, ProcessDic[GetLabelKey(Parametervektor)], size, font)
