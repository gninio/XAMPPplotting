#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import ClusterSubmission.Utils
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.SignificancePlots import CreateSiggiHistos
import XAMPPplotting.FileStructureHandler


def drawProcessFraction(Options, analysis, region, truth_labels=[], useSignal=True, bonus_str=""):

    try:
        Proccess_Frac = [
            CreateHistoSets(Options, analysis, region, "N_%s_%s" % (label, "Signal" if useSignal else "Loose"),
                            UseData=False)[0].GetSummedBackground() for label in truth_labels
        ]
    except:
        return
    Colors = [
        ROOT.TColor.GetColor(111, 224, 5),
        ROOT.TColor.GetColor(162, 145, 94),
        ROOT.TColor.GetColor(210, 104, 23),
        #ROOT.TColor.GetColor(102, 252, 245),
        ROOT.TColor.GetColor(245, 82, 253),
        ROOT.TColor.GetColor(20, 54, 208),
        ROOT.kRed,
        ROOT.kYellow,
        ROOT.kOrange + 1,
    ]
    for i, Proc in enumerate(Proccess_Frac):
        Proc.SetTitle(truth_labels[i])
        Proc.SetLineColor(Colors[i])
        Proc.SetFillColor(Colors[i])

    if Options.doLogY: bonus_str += "_LogY"
    canvasName = "ProcessFrac_%s_%s%s" % (region, "Signal" if useSignal else "Loose", bonus_str)

    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)

    pu.Prepare1PadCanvas(canvasName, 800, 600, Options.quadCanvas)
    pu.GetCanvas().cd()
    if Options.doLogY: pu.GetCanvas().SetLogy()

    pu.CreateLegend(0.5, 0.7, 0.9, 0.9)

    stk = Stack(Proccess_Frac)
    pu.AddToLegend(stk.GetHistogramStack(), Style="FL")

    ymin = 0
    ymax = max([H.GetMaximum() for H in stk.GetHistogramStack()]) * 1.5

    if ymax < 1.e-3: return
    Styling = Proccess_Frac[0].GetHistogram().Clone("styling")
    Styling.GetXaxis().SetTitle("Signal" if useSignal else "Loose")
    Styling.GetXaxis().SetBinLabel(1, "e")
    pu.drawStyling(Styling, ymin, ymax, RemoveLabel=False, TopPad=False)
    stk.Draw("SAMEHIST")

    pu.DrawLegend(NperCol=3)
    pu.DrawPlotLabels(0.195, 0.88, region, analysis, Options.noATLAS)
    pu.saveHisto("%s/%s" % (Options.outputDir, canvasName), Options.OutFileType)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='This script draws the process fractions together in one single plot"',
                                     prog='VariableChoice',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.set_defaults(outputDir="Process_Fractions/")
    parser.set_defaults(lumi=140)

    PlottingOptions = parser.parse_args()
    ClusterSubmission.Utils.CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(PlottingOptions)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    truth_labels = ["Real", "HF", "LF", "Unmatched", "CONV", "Gluon", "Elec"]
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            drawProcessFraction(PlottingOptions, ana, region, truth_labels, useSignal=False, bonus_str="")
            drawProcessFraction(PlottingOptions, ana, region, truth_labels, useSignal=True, bonus_str="")
