#
# print xsec versus signal mass in text files
# useful for the upper limits calculation in HistFitter
#
import argparse


class model(object):
    def __init__(
            self,
            dsid=0,
            nlsp=0,
            lsp=0,
            #
            xsec=0.0):
        self.__dsids = [dsid]
        self.__nlsp = nlsp
        self.__lsp = lsp
        self.__xsec = xsec
        self.__additions = 0

    def __eq__(self, other):
        return self.__nlsp == other.__nlsp and self.__lsp == other.__lsp

    def has_dsid(self, dsid):
        return (dsid in self.__dsids)

    def add_dsid(self, dsid):
        self.__dsids.append(dsid)

    def set_xsec(self, xsec=0.):
        self.__xsec = xsec

    def add_xsec(self, xsec=0.):
        self.__xsec += xsec
        self.__additions += 1

    def get_xsec(self):
        if self.__additions:
            return self.__xsec
        else:
            print "Warning, empty xsec..."
            return 0.

    def display(self):
        print "%s\t%i\t%i\t%.8f" % (' '.join(str(e) for e in set(self.__dsids)), self.__nlsp, self.__lsp, self.get_xsec())

    def write(self, f=None):
        if f:
            f.write("%i\t%i\t%.8f\n" % (self.__nlsp, self.__lsp, self.__xsec))


# main

parser = argparse.ArgumentParser(description='Options')

parser.add_argument('--input-files', '-f', help='Input xsec file(s)', nargs='+', default=[], required=True)

parser.add_argument('--output-file', '-o', help='Output xsec file', default="results.dat")

parser.add_argument('--lle', '-l', help='LLE coupling', default="12k")

parser.add_argument('--add-using-mass-points', '-m', help='Compare using mass points, otherwise the DSID', default=False)

exclude = ["direct"]

options = parser.parse_args()

models = []

coupling = "LLE" + options.lle

Lines = []

#join files - cache them into a list
for ifile in options.input_files:
    print "Scanning file '%s' ..." % (ifile)
    with open(ifile, "r") as inputf:
        Lines += inputf.readlines()

for line in Lines:
    if line.replace(" ", "").startswith("#"):  # 404200.MGPy8EG_A14N23LO_GG_1400_10_LLE12k

        #desired coupling
        if not coupling in line:
            continue

        #excluded models
        skip = False
        for ex in exclude:
            if ex in line:
                skip = True
            if skip:
                continue

        #read off
        dsid = int(line.replace("#", "").replace(" ", "").split(".")[0])
        params = line.rstrip().split("_")
        nlsp = int(params[3])
        lsp = int(params[4])

        mod = model(dsid=dsid, nlsp=nlsp, lsp=lsp)
        exists = False
        place = -1
        for position, imod in enumerate(models):
            place = position
            if imod == mod:
                exists = True
                break

        if exists:
            models[place].add_dsid(dsid)
        else:
            models.append(mod)

    else:  # 404200   2   0.0252977000   1.  1.  0.2091630000
        tokens = filter(None, line.rstrip().split(" "))  # get rid of empty tokens
        num = int(tokens[0])
        xsec = float(tokens[2])
        for m in models:
            if m.has_dsid(num):
                m.add_xsec(xsec)

with open(options.output_file, "w") as fout:
    for m in models:
        m.display()
        m.write(fout)

print "Check your results in '%s'" % (options.output_file)
