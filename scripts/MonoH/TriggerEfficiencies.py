#!/usr/bin/env python

###################################################################

# Calculation of trigger efficiencies for data and MC
#
# Script running on ntuples containing only trigger information, MetmuInvis and # b-tags
# Reduced ntuples produced with :
#
# XAMPPplotting/data/RunConf/MonoH/RunConfig_TriggerCalib.conf
# XAMPPplotting/data/TreeConf/MonoH/TriggerCalibNtuples.conf
#
# Events are selected with muon triggers and without MET / MPT cut
# Apart from that same cuts are used as in 1L CR Resolved

###################################################################

from argparse import ArgumentParser
parser = ArgumentParser(description='calculate MET trigger efficiencies')
parser.add_argument("-i", "--InputFolder", help="give path to reduced ntuples", default="")
parser.add_argument("-o", "--OutputFolder", help="where output files should be stored", default=".")
args = parser.parse_args()

import os, math, array, time
from ROOT import *
from TriggerDefs import *


# Apply weights on MC events
def getWeights(sample):
    if not (sample.startswith("data") or sample.startswith("Data")):
        weights = "*NormWeight*JetWeightJVT*JetWeightBTag*MuoWeight*MuonTriggerWeight*prwWeight"
    else:
        weights = ""
    return weights


# in case trees are produced with systematics:
def GetNominalTree(rootInputFile):
    keys = rootInputFile.GetListOfKeys()
    for key in keys:
        if key.GetName().endswith("Nominal"):
            return key.GetName()
        else:
            continue


# Calculate trigger efficiencies for each data and MC file
def GetEfficienciesPerProcess(inputFile, histoDir):
    rootFile = TFile.Open(args.InputFolder + "/" + inputFile)
    treeName = GetNominalTree(rootFile)
    tree = rootFile.Get(treeName)
    process = treeName.replace("_Nominal", "")
    h_all = {}
    h_btag_all = {}
    h_trig = {}
    h_btag_trig = {}
    roothisto = TFile(process + ".root", "RECREATE")
    # loop over all MET triggers
    for trig in trigger:
        if not (process.startswith("data")):
            h_all_name = "h_all_MC_" + triggerNames[trig]
            h_btag_all_name = "h_btag_all_MC_" + triggerNames[trig]
            h_trig_name = "h_trig_MC_" + triggerNames[trig]
            h_btag_trig_name = "h_btag_trig_MC_" + triggerNames[trig]
        else:
            h_all_name = "h_all_data_" + triggerNames[trig]
            h_btag_all_name = "h_btag_all_data_" + triggerNames[trig]
            h_trig_name = "h_trig_data_" + triggerNames[trig]
            h_btag_trig_name = "h_btag_trig_data_" + triggerNames[trig]

        # create histograms for efficiency curves
        h_all[trig] = TH1F(h_all_name, h_all_name, nBins[trig], 50., 300.)
        h_btag_all[trig] = TH1F(h_btag_all_name, h_btag_all_name, nBins[trig], 50., 300.)
        h_trig[trig] = TH1F(h_trig_name, h_trig_name, nBins[trig], 50., 300.)
        h_btag_trig[trig] = TH1F(h_btag_trig_name, h_btag_trig_name, nBins[trig], 50., 300.)

        ###########################################################################################
        # Plotting MetmuInvis separately for different runs, depending on which MET trigger was used
        # *_all_* : No trigger pass requirement
        # *_trig_* : MET trigger must have fired
        # *_btag_ *: Selecting only events with 1 or two b-tags (needed for systematics)
        ###########################################################################################

        tree.Project(h_all_name, "Met_muInvis", "(%s== 1) %s " % (triggerRuns[trig], getWeights(process)))
        tree.Project(h_btag_all_name, "Met_muInvis",
                     "(%s== 1 && (IsCR1_Resolved_1b == 1 || IsCR1_Resolved_2b == 1)) %s " % (triggerRuns[trig], getWeights(process)))
        tree.Project(h_trig_name, "Met_muInvis", "(%s == 1) %s " % (trig, getWeights(process)))
        tree.Project(h_btag_trig_name, "Met_muInvis",
                     "(%s == 1 && (IsCR1_Resolved_1b == 1 || IsCR1_Resolved_2b == 1)) %s " % (trig, getWeights(process)))

        # Write efficiencies into rootfile
        h_all[trig].Write()
        h_btag_all[trig].Write()
        h_trig[trig].Write()
        h_btag_trig[trig].Write()

    roothisto.Close()
    os.system("mv " + process + ".root " + histoDir)
    return roothisto


# merge all MC background / data files into one
def haddHistos(histoDir):
    MCProcessString = ""
    dataProcessString = ""
    for inFile in os.listdir(histoDir):
        if not (inFile.startswith("data") or inFile.startswith("Data")):
            MCProcessString = MCProcessString + histoDir + "/" + inFile + " "
        else:
            dataProcessString = dataProcessString + histoDir + "/" + inFile + " "
    MChaddCommand = "hadd " + histoDir + "/MC_histos.root " + MCProcessString
    datahaddCommand = "hadd " + histoDir + "/data_histos.root " + dataProcessString
    os.system(MChaddCommand)
    os.system(datahaddCommand)


# Plot efficiency curves for data and MC
def PlotEfficiencies(inputFile1, inputFile2):
    rootFile1 = TFile.Open(inputFile1)
    rootFile2 = TFile.Open(inputFile2)
    c = {}
    region_label = {}
    lumi_label = {}
    # file where to store the efficiency histograms
    EffRootFile = TFile("efficiencies.root", "RECREATE")
    for trig in trigger:
        for name in ["_", "_btag_"]:

            # efficiecy plots only for all b-tag categories
            # information for 1/2 b-tags only written to rootfiles
            if (name == "_"):
                c[trig] = TCanvas("c_" + trig, "c_" + trig, 10, 32, 668, 643)

            # Data to MC ratio in lower pad
            pad1 = TPad("pad1", "pad1", .001, .185, .995, .995)
            pad2 = TPad("pad1", "pad2", .001, .001, .995, .3)
            pad1.Draw()
            pad2.Draw()
            pad1.SetBottomMargin(0.15)
            pad1.SetTopMargin(0.1)
            pad1.cd()

            h_all_MC = rootFile1.Get("h" + name + "all_MC_" + triggerNames[trig])
            h_all_data = rootFile2.Get("h" + name + "all_data_" + triggerNames[trig])
            h_trig_MC = rootFile1.Get("h" + name + "trig_MC_" + triggerNames[trig])
            h_trig_data = rootFile2.Get("h" + name + "trig_data_" + triggerNames[trig])

            #####################################################
            # use binomial errors because events of h_trig_MC are
            # also part of h_all_MC --> histograms correlated
            #####################################################

            h_trig_MC.Divide(h_trig_MC, h_all_MC, 1., 1., "B")

            ## For data: Use TEfficiency class
            ## (not applicable to MC events because the weights cannot be handled properly in efficiency calculation)

            ## h_trig_data = TEfficiency(h_trig_data, h_all_data)
            h_trig_data.Divide(h_trig_data, h_all_data, 1., 1., "B")

            ########################
            # Plotting the efficiencies
            ########################

            #h_trig_MC.SetMarkerColor(kRed)
            h_trig_MC.SetLineColor(kBlue)
            h_trig_MC.GetYaxis().SetTitle("Efficiency")
            h_trig_MC.GetYaxis().SetLabelSize(0.04)
            h_trig_MC.GetYaxis().SetRangeUser(-0.01, 1.05)
            h_trig_MC.SetMarkerStyle(1)
            h_trig_MC.SetLineWidth(2)
            h_trig_MC.Draw("same")

            h_trig_data.SetMarkerColor(kBlack)
            h_trig_data.SetLineColor(kBlack)
            h_trig_data.SetMarkerStyle(20)
            h_trig_data.SetMarkerSize(1)
            h_trig_data.SetLineWidth(2)
            h_trig_data.Draw("same")

            # draw vertical line at MET = 150 GeV --> start of SR
            line = TLine(150, -0.01, 150, 1.05)
            line.SetLineStyle(2)
            line.SetLineWidth(2)
            line.Draw("same")

            SetAtlasLabel()
            SetLumiLabel(trig)
            SetRegionLabel(trig)
            SetPeriodLabel(trig)
            legend = SetLegend(h_trig_MC, h_trig_data)
            legend.Draw("same")

            # Drawing the ratio plot

            pad2.SetTopMargin(0.)
            pad2.SetBottomMargin(0.47)
            pad2.cd()
            h_ratio = h_trig_data.Clone("h_ratio")

            # Divide without "B" as data and MC are uncorrelated
            h_ratio.Divide(h_ratio, h_trig_MC)

            h_ratio.GetXaxis().SetTitle("E_{T}^{miss}, #mu invis. [GeV] ")
            h_ratio.GetXaxis().SetTitleFont(42)
            h_ratio.GetXaxis().SetTitleSize(0.14)
            h_ratio.GetXaxis().SetLabelSize(0.13)
            h_ratio.GetYaxis().SetLabelSize(0.11)
            h_ratio.GetYaxis().SetRangeUser(0.5, 1.15)
            h_ratio.GetYaxis().SetTitle("Data / MC")
            h_ratio.GetYaxis().SetTitleSize(0.12)
            h_ratio.GetYaxis().SetTitleOffset(0.5)
            h_ratio.GetYaxis().SetNdivisions(6)
            h_ratio.SetLineWidth(1)
            h_ratio.Draw("P")
            lineAtOne = TLine(50, 1, 300, 1)
            lineAtOne.SetLineStyle(2)
            lineAtOne.Draw("same")
            lineAt150 = TLine(150, 0.5, 150, 1.15)
            lineAt150.SetLineStyle(2)
            lineAt150.SetLineWidth(2)
            lineAt150.Draw("same")
            # save histograms to root file
            h_trig_MC.Write()
            h_trig_data.Write()

            # save plots
            if (name == "_"):
                c[trig].Print("efficiecy_%s.pdf" % (triggerNames[trig]))
    EffRootFile.Close()


# TODO : add option to not fill efficiency histos and not to hadd


def main():
    # activate for all histos the storage of the sum of squares of errors
    TH1.SetDefaultSumw2()

    if not os.path.exists("RootHistosPerProcess"):
        os.makedirs("RootHistosPerProcess")
    for i, inFile in enumerate(os.listdir(args.InputFolder)):
        if not inFile.endswith(".root"): continue
        GetEfficienciesPerProcess(inFile, "RootHistosPerProcess")
    haddHistos("RootHistosPerProcess")
    PlotEfficiencies("RootHistosPerProcess/MC_histos.root", "RootHistosPerProcess/data_histos.root")

    # move histos to directory
    directory = args.OutputFolder
    if not os.path.exists(directory):
        os.makedirs(directory)
    subdirectory1 = directory + "/TriggerEfficiencies_" + time.strftime("%d-%m-%Y") + "/pdf"
    subdirectory2 = directory + "/TriggerEfficiencies_" + time.strftime("%d-%m-%Y") + "/root"
    if not os.path.exists(subdirectory1):
        os.makedirs(subdirectory1)
    if not os.path.exists(subdirectory2):
        os.makedirs(subdirectory2)
    cmd_mv1 = "mv efficiecy* " + subdirectory1
    cmd_mv2 = "mv efficiencies.root " + subdirectory2
    os.system(cmd_mv1)
    os.system(cmd_mv2)

    # remove this line once histogram recycling is possible
    os.system("rm -r RootHistosPerProcess")


if __name__ == '__main__':
    main()
