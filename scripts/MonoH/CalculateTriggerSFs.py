#!/usr/bin/env python

##########################################################

# Calculation the scale factors for each MET trigger
#
# Using efficiency histograms from TriggerEfficiencies.py as input

##########################################################

from argparse import ArgumentParser
parser = ArgumentParser(description='calculate MET trigger SF')
parser.add_argument("-i", "--InputFolder", help="give path to reduced ntuples", default=".")
parser.add_argument("-o", "--OutputFolder", help="where SF plots should be stored", default=".")
parser.add_argument("-t", "--TextFolder", help="where text files should be stored", default=".")
parser.add_argument("-b", "--bTags", help="calculate also SFs for events with 1 and 2 b-tags", action='store_true', default=False)
args = parser.parse_args()

from ROOT import *
from TriggerDefs import *
import os
gROOT.ForceStyle()

Math.MinimizerOptions.SetDefaultMaxFunctionCalls(10000)
opt = Math.MinimizerOptions()
opt.SetDefaultMinimizer("Minuit2")
opt.SetDefaultStrategy(0)

if not os.path.exists(args.OutputFolder):
    os.makedirs(args.OutputFolder)
if not os.path.exists(args.TextFolder):
    os.makedirs(args.TextFolder)

# Storing the fit parameters in text files
txtNames = {}
txtNames_btag = {}
for trig in trigger:
    txtNames[trig] = triggerNames[trig] + "_nominal_MonoH_Rel21.txt"
    txtNames_btag[trig] = triggerNames[trig] + "_12btag_MonoH_Rel21.txt"

# efficiency file from TriggerEfficiencies.py
rootfile = TFile.Open(args.InputFolder + "/root/efficiencies.root")

# activate for all histos the storage of the sum of squares of errors
TH1.SetDefaultSumw2()

h_MC = {}
h_btag_MC = {}
h_data = {}
h_btag_data = {}
h_SF = {}
h_btag_SF = {}
c = {}

##########################################
# Using error function for SF determination
# Fit range: 120 GeV < MET < 300 GeV
##########################################

h_ratio = {}
h_btag_ratio = {}
for trig in trigger:
    fitErf = TF1("myFit1T1_erf", " [0] * (1 + TMath::Erf( (x-[1]) / [2] ) ) + [3]", 100., 300.)
    fitErf_btag = TF1("myFit1T1_erfbTag", "[0] * (1 + TMath::Erf( (x-[1]) / [2] ) ) + [3]", 100., 300.)
    fitErf.SetLineColor(kBlue)
    fitErf_btag.SetLineColor(kRed)
    c[trig] = TCanvas()
    h_MC[trig] = rootfile.Get("h_trig_MC_" + triggerNames[trig])
    h_data[trig] = rootfile.Get("h_trig_data_" + triggerNames[trig])
    h_ratio[trig] = h_MC[trig].Clone("h_ratio_" + triggerNames[trig])
    # Divide without "B" as data and MC are uncorrelated
    h_ratio[trig].Divide(h_data[trig], h_MC[trig])

    #############################
    # Plotting the data / MC ratio
    #############################

    h_ratio[trig].GetYaxis().SetRangeUser(0.55, 1.01)
    h_ratio[trig].GetXaxis().SetRangeUser(50., 300)
    h_ratio[trig].GetXaxis().SetTitle("E_{T}^{miss}, #mu invis. [GeV] ")
    h_ratio[trig].GetYaxis().SetTitle("Scale Factor")
    h_ratio[trig].SetMarkerColor(kBlue)
    h_ratio[trig].SetMarkerStyle(1)
    h_ratio[trig].SetLineWidth(2)
    h_ratio[trig].SetLineColor(kBlue)
    h_ratio[trig].Draw("E1")
    line = TLine(150, 0.55, 150, 1.01)
    line.SetLineStyle(2)
    line.SetLineWidth(2)
    line.Draw()
    SetAtlasLabel()
    SetLumiLabel(trig)
    SetRegionLabel(trig)
    SetPeriodLabel(trig)

    #####################################################
    # Get the scale factor by fitting the ratio histogram
    # E: Perform better Errors estimation using Minos technique
    # M: Improve fit results. It uses the IMPROVE command of TMinuit
    # S: The result of the fit is returned in the TFitResultPtr
    #####################################################

    if (trig == "IsTrigHLT_xe110_pufit_L1XE55_and_CorrectRun"):
        fitErf.SetParameter(0, 8040.)
        fitErf.SetParameter(1, -446.)
        fitErf.SetParameter(2, 193.)
        fitErf.SetParameter(3, -16078.)
        accessFit = h_ratio[trig].Fit(fitErf, "SV", "", 100., 300.)
    elif (trig == "IsTrigHLT_xe110_mht_L1XE50_and_CorrectRun"):
        fitErf.SetParameter(0, 8129.)
        fitErf.SetParameter(1, -440)
        fitErf.SetParameter(2, 184)
        fitErf.SetParameter(3, -16257)
        accessFit = h_ratio[trig].Fit(fitErf, "SV", "", 100., 300.)
    elif (trig == "IsTrigHLT_xe90_mht_L1XE50_and_CorrectRun"):
        fitErf.SetParameter(0, 8000.)
        fitErf.SetParameter(1, -500.)
        fitErf.SetParameter(2, 200.)
        fitErf.SetParameter(3, -16000)
        accessFit = h_ratio[trig].Fit(fitErf, "SV", "", 100., 280.)
    else:
        fitErf.SetParameter(0, 7500)
        fitErf.SetParameter(1, -515.)
        fitErf.SetParameter(2, 200.)
        fitErf.SetParameter(3, -15000)
        accessFit = h_ratio[trig].Fit(fitErf, "SV", "", 100., 300.)
    fitErf.Draw("same")
    chi2 = accessFit.Chi2()
    NDF = accessFit.Ndf()
    # "good" fit: chi2 / NDF ~ 1

    print "chi2 = ", chi2
    print "NDF = ", NDF
    print "chi2 / NDF = ", chi2 / NDF
    covMatrix = accessFit.GetCovarianceMatrix()
    accessFit.Print("V")
    print "status = ", int(accessFit)

    chi2NDF_label = TLatex()
    chi2NDF_label.SetNDC()
    chi2NDF_label.SetTextFont(43)
    chi2NDF_label.SetTextSize(18)

    # write fit parameters into text file
    # TODO: Find a smarter way to do this than writing always str(...)

    txtLine = str(accessFit.Parameter(0)) + " " + str(accessFit.Parameter(1)) + " " + str(accessFit.Parameter(2)) + " " + str(accessFit.Parameter(3)) + " " + \
    str(accessFit.ParError(0)) + " " + str(accessFit.ParError(1)) + " " + str(accessFit.ParError(2)) + " " + str(accessFit.ParError(3)) + " " + \
    str(covMatrix(0, 0)) + " " + str(covMatrix(0, 1)) + " " + str(covMatrix(0, 2)) + " " + str(covMatrix(0, 3)) + " " + \
    str(covMatrix(1, 0)) + " " + str(covMatrix(1, 1)) + " " + str(covMatrix(1, 2)) + " " + str(covMatrix(1, 3)) + " " + \
    str(covMatrix(2, 0)) + " " + str(covMatrix(2, 1)) + " " + str(covMatrix(2, 2)) + " " + str(covMatrix(2, 3)) + " " + \
    str(covMatrix(3, 0)) + " " + str(covMatrix(3, 1)) + " " + str(covMatrix(3, 2)) + " " + str(covMatrix(3, 3)) + " " + \
    str(chi2) + " " + str(NDF)
    txtFile = open(args.TextFolder + "/" + txtNames[trig], "w")
    txtFile.write(txtLine)
    txtFile.close()

    if not args.bTags:
        # draw confidence intervals only when 1/2 b-tag curve is not plotted
        confIntervals = TGraphErrors(199)
        for i in range(101, 300):
            confIntervals.SetPoint(i - 101, i, fitErf(i))

        TVirtualFitter.GetFitter().SetPrecision(0.01)
        # CL = 0.68 = 1 sigma
        TVirtualFitter.GetFitter().GetConfidenceIntervals(confIntervals, 0.68)

        confIntervals.SetMarkerStyle(1)
        confIntervals.SetMarkerColor(kBlack)
        confIntervals.Draw("same")
        chi2NDF_label.SetTextColor(kBlack)
        chi2NDF_label.DrawLatex(0.55, 0.34, "#chi^{2} / NDF = " + str("%.2f" % chi2) + "/" + str(NDF))
        chi2NDF_label.DrawLatex(0.55, 0.4, "nominal fit with 1#sigma uncertainty")

        # save plots
        c[trig].Print(args.OutputFolder + "/SF_" + triggerNames[trig] + ".pdf")

    else:
        # plot also SF curve in addition to SF curve with all b-tags
        h_btag_MC[trig] = rootfile.Get("h_btag_trig_MC_" + triggerNames[trig])
        h_btag_data[trig] = rootfile.Get("h_btag_trig_data_" + triggerNames[trig])
        h_btag_ratio[trig] = h_btag_data[trig].Clone("h_btag_ratio_" + triggerNames[trig])

        # use standard Divide function without "B", because data and MC are uncorrelated
        h_btag_ratio[trig].Divide(h_btag_data[trig], h_btag_MC[trig])

        h_btag_ratio[trig].SetMarkerColor(kRed)
        h_btag_ratio[trig].SetMarkerStyle(1)
        h_btag_ratio[trig].SetLineWidth(1)
        h_btag_ratio[trig].SetLineColor(kRed)
        h_btag_ratio[trig].Draw("e1 same")

        # fitting performed in the same way for all-b-tag events
        if (trig == "IsTrigHLT_xe70_mht_and_CorrectRun"):
            fitErf_btag.SetParameter(0, 10.)
            fitErf_btag.SetParameter(1, -80.)
            fitErf_btag.SetParameter(2, 123.)
            fitErf_btag.SetParameter(3, -20.)
            accessFit_btag = h_btag_ratio[trig].Fit(fitErf_btag, "SV", "", 100., 300.)
        elif (trig == "IsTrigHLT_xe90_mht_L1XE50_and_CorrectRun"):
            fitErf_btag.SetParameter(0, 5000.)
            fitErf_btag.SetParameter(1, -500.)
            fitErf_btag.SetParameter(2, 200.)
            fitErf_btag.SetParameter(3, -10000.)
            accessFit_btag = h_btag_ratio[trig].Fit(fitErf_btag, "SV", "", 100., 300.)
        else:
            fitErf_btag.SetParameter(0, 5000.)
            fitErf_btag.SetParameter(1, -500.)
            fitErf_btag.SetParameter(2, 200.)
            fitErf_btag.SetParameter(3, -10000.)
            accessFit_btag = h_btag_ratio[trig].Fit(fitErf_btag, "SV", "", 100., 300.)

        chi2_btag = accessFit_btag.Chi2()
        NDF_btag = accessFit_btag.Ndf()
        print "chi2_btag = ", chi2_btag
        print "NDF_btag = ", NDF_btag
        print "chi2_btag / NDF_btag = ", chi2_btag / NDF_btag
        covMatrix_btag = accessFit_btag.GetCovarianceMatrix()
        accessFit_btag.Print("V")
        fitErf_btag.Draw("same")
        chi2NDF_label.SetTextColor(kBlue)
        chi2NDF_label.DrawLatex(0.55, 0.4, "nominal fit, all b-tags")
        chi2NDF_label.DrawLatex(0.55, 0.35, "#chi^{2} / NDF = " + str("%.2f" % chi2) + " / " + str(NDF))
        chi2NDF_label.SetTextColor(kRed)
        chi2NDF_label.DrawLatex(0.55, 0.29, "nominal fit, 1 and 2 b-tags")
        chi2NDF_label.DrawLatex(0.55, 0.24, "#chi^{2} / NDF = " + str("%.2f" % chi2_btag) + " / " + str(NDF_btag))

        # for 1/2 b-tags: Store only fit parameter 0 and 1 in text file
        txtLine_btag = str(accessFit_btag.Parameter(0)) + " " + str(accessFit_btag.Parameter(1)) + " " + str(
            accessFit_btag.Parameter(2)) + " " + str(accessFit_btag.Parameter(3))
        txtFile_btag = open(args.TextFolder + "/" + txtNames_btag[trig], "w")
        txtFile_btag.write(txtLine_btag)
        txtFile_btag.close()

        # save plots
        c[trig].Print(args.OutputFolder + "/SF_btags_" + triggerNames[trig] + ".pdf")
