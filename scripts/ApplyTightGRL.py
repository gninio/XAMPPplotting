from ClusterSubmission.Utils import WriteList, MakePathResolvable, CreateDirectory, ResolvePath
from ClusterSubmission.PeriodRunConverter import getGRL
import os, argparse, logging


def setupTightGRLParser():
    parser = argparse.ArgumentParser(
        description="Simple script reading out an incfg-dir and creating a new set of in_cfgs where the tight GRL is applied on data")

    parser.add_argument("--in_cfg_dir", help="Directory having all the input configs", required=True)
    parser.add_argument("--out_cfg_dir", help="Final directory", default="TightGRL")
    parser.add_argument("--tight_grl_json",
                        help="Location of the json file containing the paths to the tight GRLs",
                        default="ClusterSubmission/GRL_Tight.json")
    return parser


if __name__ == '__main__':
    options = setupTightGRLParser().parse_args()

    CreateDirectory(options.out_cfg_dir, False)

    in_dir = ResolvePath(options.in_cfg_dir)
    if not in_dir: exit(1)

    import_in_dir = MakePathResolvable(options.in_cfg_dir)
    if len(import_in_dir) == 0: import_in_dir = in_dir
    for cfg in os.listdir(in_dir):

        WriteList([
            "####################################################################################################",
            "# This is an automated input config file to apply the Tight GRL on your data for cross-checks.",
            "# The application only affects data not Monte Carlo. Reweighting of the pile-up distributions",
            "# is neither possible or neeeded.", "# The cross-check is required in the SUSY full analysis review process (FAR):",
            "#   -- https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DataPreparationCheckListForPhysicsAnalysis",
            "# Information about the GRL can be found here:",
            "#   -- https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun2#2015_13_TeV_pp_data_taking_summa",
            "####################################################################################################",
            "Import %s/%s" % (import_in_dir, cfg)
        ] + ["GRL %s" % (g) for g in getGRL(flavour='GRL', config=options.tight_grl_json)], options.out_cfg_dir + "/" + cfg)
