import os, argparse, logging
from ClusterSubmission.Utils import ResolvePath, WriteList, ExecuteCommands, CreateDirectory, id_generator
from CreateFakeFactorEstimate import fake_weight_snippet, pair_id_antid_regions
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script to create the fake-factor config for application")
    parser.add_argument("--inCfgDir", help="Input config location", default="XAMPPplotting/InputConf/LepHadStau/V23/")
    parser.add_argument("--useSherpa", help="Run the closure test with sherpa instead of PowHeg", action="store_true", default=False)
    parser.add_argument("--histCfg",
                        help="Histo config to use for the test",
                        default="XAMPPplotting/HistoConf/LepHadStau/Histos_MainAnalysis.conf")
    parser.add_argument("--runCfgDir", help="Where are the run configs stored", default="XAMPPplotting/RunConf/LepHadStau/Regions/")
    parser.add_argument("--out_dir", help="Final directory where the files are piped to", default="closure_test/")

    options = parser.parse_args()

    ### Find the input cfg_dir
    in_cfg_dir = ResolvePath(options.inCfgDir)
    if not in_cfg_dir: exit(1)
    ### Then the run_cfg dir
    run_cfg_dir = ResolvePath(options.runCfgDir)
    if not run_cfg_dir: exit(1)

    cfgs_to_use = []
    #### Assemble the Config files
    for x in os.listdir(in_cfg_dir):

        process = x[x.rfind("_") + 1:x.rfind(".")]
        ### No W+jets sample
        if len([flav for flav in ["enu", "munu", "taunu"] if x.find(flav) != -1]) == 0: continue
        if not x.endswith(".conf"): continue
        if options.useSherpa and x.find("Sherpa") == -1: continue
        elif not options.useSherpa and x.find("PowHeg") == -1: continue

        cfgs_to_use += [options.inCfgDir + "/" + x]

    final_in_cfg = WriteList(["Import %s" % (cfg) for cfg in cfgs_to_use], "%s/tmp/W_jets.conf" % (options.out_dir))

    closure_cfg_dir = run_cfg_dir + "/closureTest/"
    CreateDirectory(closure_cfg_dir, True)

    raw_jobs = []
    closure_jobs = []
    cmds = []
    ### Assemble the pass and failure regions
    for Fail, Pass in pair_id_antid_regions(run_cfg_dir):
        if "QCD" in Fail: continue
        if "IsoSS" in Fail: continue
        closure_content = WriteList(["define ClosureTest"] + fake_weight_snippet(Fail, Pass) +
                                    ["EvCut region IsRegion_%s = 1" % (Fail), "noSyst"],
                                    "%s/%s_Closure.conf" % (closure_cfg_dir, Fail[:Fail.rfind("TauID") - 1]))
        raw_content = WriteList(
            fake_weight_snippet(Fail, Pass) + ["EvCut region IsRegion_%s = 1" % (Pass), "noSyst"],
            "%s/%s_Raw.conf" % (closure_cfg_dir, Fail[:Fail.rfind("TauID") - 1]))

        out_closure = id_generator(10)
        out_raw = id_generator(10)
        cmds += [
            "WriteDefaultHistos -i %s -r %s -h %s -o %s/tmp/%s.root" %
            (final_in_cfg, closure_content, options.histCfg, options.out_dir, out_closure),
            "WriteDefaultHistos -i %s -r %s -h %s -o %s/tmp/%s.root" %
            (final_in_cfg, raw_content, options.histCfg, options.out_dir, out_raw),
        ]
        raw_jobs += [out_raw]
        closure_jobs += [out_closure]
    ### Execute the histogram writing
    ExecuteCommands(cmds, 4)
    ### Hadd the final files
    hadd_cmd_raw = "hadd -f %s/raw_mc.root %s" % (options.out_dir, " ".join(["%s/tmp/%s.root" % (options.out_dir, raw)
                                                                             for raw in raw_jobs]))
    hadd_cmd_closure = "hadd -f %s/reweighted_mc.root %s" % (options.out_dir, " ".join(
        ["%s/tmp/%s.root" % (options.out_dir, closure) for closure in closure_jobs]))

    os.system(hadd_cmd_raw)
    os.system(hadd_cmd_closure)
    os.system("rm -rf %s/tmp/" % (options.out_dir))
