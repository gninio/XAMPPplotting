#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.Utils import CreateRatioHistos, setupBaseParser

from ClusterSubmission.Utils import CreateDirectory, id_generator
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.FileStructureHandler import GetStructure
from XAMPPplotting import TauInfo as TI
import ROOT


##
###
####   Simple script to plot the QCD region and the SR together in one stack
###
##
def drawComparison(Options, analysis, SR, QCD, variable, bonus_str=""):
    HistToDraw = []
    ratios = []

    FullHistoSet_SR = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, analysis, SR, variable, UseData=True)
    FullHistoSet_QCD = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, analysis, QCD, variable, UseData=True)

    if not FullHistoSet_SR or not FullHistoSet_SR[0].CheckHistoSet() or not FullHistoSet_QCD or not FullHistoSet_QCD[0].CheckHistoSet():
        print "WARNING: Skip variables"
        return False

    QCD_Set = FullHistoSet_QCD[0]
    SR_Set = FullHistoSet_SR[0]

    QCD_data = QCD_Set.GetData()
    QCD_data.SetFillColor(ROOT.kGray)
    QCD_data.SetLineColor(ROOT.kGray)
    QCD_data.SetTitle("QCD")

    SR_Set.GetSummedBackground().Add(QCD_data)

    DefaultSet = SR_Set
    pu = PlotUtils(status=Options.label, size=24, lumi=DefaultSet.GetLumi(), lumilabel=Options.lumi_label)

    if len(DefaultSet.GetData()) == 0:
        logging.error("This is DataMCPlots. Please provide a datasample.")
        return False

    if DefaultSet.isTH2():
        return False

    # Create the TCanvas
    logstr = "" if not Options.doLogY else "_LogY"

    if Options.noRatio:
        pu.Prepare1PadCanvas("DataMC_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
        if Options.doLogY:
            pu.GetCanvas().SetLogy()
    else:
        pu.Prepare2PadCanvas("DataMC_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
        pu.GetTopPad().cd()
        if Options.doLogY:
            pu.GetTopPad().SetLogy()

    pu.CreateLegend(Options.LegendXCoords[0], Options.LegendYCoords[0], Options.LegendXCoords[1], Options.LegendYCoords[1])

    ### We need to copy the list to a new one otherwise strange things are going to happen
    datasamples = [D for D in DefaultSet.GetData()]

    # Create Stack containing all the backgrounds in it
    stk = Stack(DefaultSet.GetBackgrounds() + QCD_data)

    for Data in datasamples:
        Halo = pu.CreateHaloHistogram(Data.GetHistogram())
        HistToDraw.append((Halo, "same"))
        HistToDraw.append((Data, "same"))
        HistList.append(Data)

    # draw the stack in order to get the histogram for the axis ranges
    HistList.append(DefaultSet.GetSummedBackground())
    ymin, ymax = pu.GetFancyAxisRanges(HistList, Options)

    if math.fabs(ymin) <= 1.e-10 and math.fabs(ymax) <= 1.e-10:
        logging.info("Night time %.2f day time %.2f... Skip plot" % (ymin, ymax))
        return False
    pu.drawStyling(DefaultSet.GetSummedBackground(), ymin, ymax, RemoveLabel=not Options.noRatio, TopPad=not Options.noRatio)

    stk.Draw("sameHist")
    pu.drawSumBg(DefaultSet.GetSummedBackground(), (not Options.noSyst and not Options.noUpperPanelSyst))
    for H, Opt in HistToDraw:
        H.Draw(Opt)

    ### Add the histograms to legend
    pu.AddToLegend(datasamples, Style="PL")
    pu.AddToLegend(stk.GetHistogramStack(), Style="FL")
    pu.AddToLegend(DefaultSet.GetSignals(), Style="L")
    pu.DrawLegend(NperCol=Options.EntriesPerLegendColumn)

    pu.DrawPlotLabels(0.18, 0.83, Options.regionLabel if len(Options.regionLabel) > 0 else region, analysis, Options.noATLAS)

    #no ratio
    if not Options.noRatio:
        pu.GetTopPad().RedrawAxis()
        pu.GetBottomPad().cd()
        pu.GetBottomPad().SetGridy()
        ratios += CreateRatioHistos(DefaultSet.GetData(), DefaultSet.GetSummedBackground())
        ymin, ymax = pu.GetFancyAxisRanges(ratios, Options, doRatio=True)
        pu.drawRatioStyling(DefaultSet.GetSummedBackground(), ymin, ymax, "Data/MC")
        pu.drawRatioErrors(DefaultSet.GetSummedBackground(), not Options.noSyst, not Options.noRatioSysLegend)

        for r in ratios:
            r.Draw("same")
            r.Draw("sameE0")

    ROOT.gPad.RedrawAxis()

    PreString = region
    if analysis != "":
        PreString = analysis + "_" + region
    if not Options.summaryPlotOnly:
        pu.saveHisto("%s/DataMC_%s_%s%s" % (Options.outputDir, PreString, var, logstr), Options.OutFileType)

    if not Options.noRatio:
        pu.GetTopPad().cd()
    pu.DrawTLatex(0.5, 0.94, "%s%s in %s (%s analysis)" % (var, logstr, region, analysis), 0.04, 52, 22)

    pu.saveHisto("%s/AllDataMCPlots%s" % (Options.outputDir, bonusstr), ["pdf"])

    return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script produces MC only histograms from tree files. For more help type \"python MCPlots.py -h\"',
        prog='TauFF_Contributions',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.set_defaults(outputDir="TauFF_Composition")
    PlottingOptions = parser.parse_args()
    CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)
    qcd_mapping = [
        ("Chan_Ele_CR_QCD_SR_PreSel_TauID_fail", "Chan_Ele_SR_PreSel_TauID_fail"),
        ("Chan_Muo_CR_QCD_SR_PreSel_TauID_fail", "Chan_Muo_SR_PreSel_TauID_fail"),
        ("Chan_Ele_CR_QCD_SR_0jets_TauID_fail", "Chan_Ele_SR_0jets_TauID_fail"),
        ("Chan_Muo_CR_QCD_SR_0jets_TauID_fail", "Chan_Muo_SR_0jets_TauID_fail"),
        ("Chan_Ele_CR_QCD_SR_1jets_TauID_fail", "Chan_Ele_SR_1jets_TauID_fail"),
        ("Chan_Muo_CR_QCD_SR_1jets_TauID_fail", "Chan_Muo_SR_1jets_TauID_fail"),
    ]

    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for sr, qcd in qcd_mapping:
            for variable in FileStructure.GetConfigSet().GetVariables(ana, sr):
                tau_info = TI.TauInfo(ana, region, variable, "data")
                if not tau_info.is_ff_ready: continue
                drawComparison(PlottingOptions, ana, sr, qcd, variable)
