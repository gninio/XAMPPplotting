from ClusterSubmission.Utils import FillWhiteSpaces, WriteList
import logging
TauCollections = {
    "Incl": "BaseTaus",
    "Real": "BaseTausReal",
    #"HF": "BaseTausHFJets",
    #"LF": "BaseTausLFJets",
    "Quark": "BaseTausQuarkJets",
    "Gluon": "BaseTausGluonJets",
    "PU": "BaseTausPUJets",

    #"Elec": "BaseTausElectrons",
    #"Muon": "BaseTausMuons",
    "Leptonic": "BaseTausLepton",
    #"Conv": "BaseTausConversion",
}
LeptonRequirements = {
    "Incl": "",
    "Real": "NumParCut BaseTrueLeptons > 0",
}

LeptonCollections = {
    "Incl": "BaseLightLeptons",
    "Real": "BaseTrueLeptons",
}
Trigger_Comb = {
    "Base": [],
    "LepTrigger": [
        "CombCut OR",
        "%sCondAppliedCut TriggerWeightMuons = 1" % (FillWhiteSpaces(4)),
        "%sCondAppliedCut TriggerWeightElectrons = 1" % (FillWhiteSpaces(4)), "End_CombCut"
    ],
    "TauTrigger": [
        "CombCut OR",
        "%sCondAppliedCut TriggerWeightTausMuons = 1" % (FillWhiteSpaces(4)),
        "%sCondAppliedCut TriggerWeightTausElectrons = 1" % (FillWhiteSpaces(4)), "End_CombCut"
    ],
}

cfg = ["Import XAMPPplotting/HistoConf/LepHadStau/Histos_Defs.conf\n\n"]

for kind, collection in TauCollections.iteritems():
    if kind != "Incl": cfg += ["ifdef isMC"]
    for trig_def, trig_cut in Trigger_Comb.iteritems():
        for lep_kind, lep_cut in LeptonRequirements.iteritems():
            if lep_kind != "Incl" and kind == "Incl":
                cfg += ["ifdef isMC"]
            for prong in ["Incl", "1P", "3P"]:
                sel_cut = []
                if len(trig_cut) == 0 and len(lep_cut) == 0:
                    logging.info("No selection cut defined")
                elif len(trig_cut) > 0 and len(lep_cut) == 0:
                    sel_cut = trig_cut
                else:
                    sel_cut = ["%sCombCut AND" %
                               (FillWhiteSpaces(4))] + ["%s%s" % (FillWhiteSpaces(8), x)
                                                        for x in trig_cut + [lep_cut]] + ["%sEnd_CombCut" % (FillWhiteSpaces(4))]

                #### Eta variable
                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauAbsEta" % (trig_def, kind, prong, lep_kind),
                    "   Template fine_abseta_ff",
                    "   ParReader %s%s |eta|[0]" % (collection, "" if prong == "Incl" else prong),
                    "   xLabel |#eta| (#tau)",
                ] + sel_cut + ["EndVar", ""]
                ###   Pt variable
                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauPt" % (trig_def, kind, prong, lep_kind),
                    "   Template fine_tau_pt_ff",
                    "   ParReader %s%s pt[0]" % (collection, "" if prong == "Incl" else prong),
                    "   xLabel p_{T}(#tau) [GeV]",
                ] + sel_cut + ["EndVar", ""]
                ### Width
                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauWidth" % (trig_def, kind, prong, lep_kind),
                    "   Template jet_width",
                    "   ParReader %s%s Width[0]" % (collection, "" if prong == "Incl" else prong),
                    "   xLabel width (#tau)",
                    "   noUnderFlowPull",
                ] + sel_cut + ["EndVar", ""]

                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauTrkJetWidth" % (trig_def, kind, prong, lep_kind),
                    "   Template jet_width",
                    "   ParReader %s%s TrkJetWidth[0]" % (collection, "" if prong == "Incl" else prong),
                    "   noUnderFlowPull",
                    "   xLabel track-width (#tau)",
                ] + sel_cut + ["EndVar", ""]
                #                cfg += [
                #                    "NewVar",
                #                    "   Type 1D",
                #                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauTrkJetWidthTauVtx" %
                #                    (trig_def, kind, prong, lep_kind),
                #                    "   Template jet_width",
                #                    "   ParReader %s%s TrkJetWidthTauVtx[0]" % (collection, "" if prong == "Incl" else prong),
                #                    "   noUnderFlowPull",
                #                    "   xLabel track-width (#tau)",
                #                ] + sel_cut + ["EndVar", ""]
                ### Z0 sin theta
                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauZ0SinTheta" % (trig_def, kind, prong, lep_kind),
                    "   Template WideZ0sin",
                    "   ParReader %s%s z0sinTheta[0]" % (collection, "" if prong == "Incl" else prong),
                    "   xLabel z_{0} sin#theta (#tau)",
                ] + sel_cut + ["EndVar", ""]
                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauD0" % (trig_def, kind, prong, lep_kind),
                    "   Template WideZ0sin",
                    "   ParReader %s%s d0[0]" % (collection, "" if prong == "Incl" else prong),
                    "   xLabel d_{0} (#tau)",
                ] + sel_cut + ["EndVar", ""]
                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauD0sig" % (trig_def, kind, prong, lep_kind),
                    "   Template D0sig",
                    "   ParReader %s%s d0sig[0]" % (collection, "" if prong == "Incl" else prong),
                    "   xLabel d_{0} significance (#tau)",
                ] + sel_cut + ["EndVar", ""]

                ### Pt --- Width
                cfg += [
                    "NewVar",
                    "   Type 2D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauPtWidth" % (trig_def, kind, prong, lep_kind),
                    "   Template fine_tau_pt_width_ff",
                    "   ParReader %s%s pt[0]" % (collection, "" if prong == "Incl" else prong),
                    "   ParReader %s%s Width[0]" % (collection, "" if prong == "Incl" else prong),
                    "   xLabel p_{T} (#tau) [GeV]",
                    "   yLabel width (#tau)",
                    "   noUnderFlowPull",
                ] + sel_cut + ["EndVar", ""]

                cfg += [
                    "NewVar",
                    "   Type 2D",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauPtTrkJetWidth" % (trig_def, kind, prong, lep_kind),
                    "   Template fine_tau_pt_width_ff",
                    "   ParReader %s%s pt[0]" % (collection, "" if prong == "Incl" else prong),
                    "   ParReader %s%s TrkJetWidth[0]" % (collection, "" if prong == "Incl" else prong),
                    "   xLabel p_{T} (#tau) [GeV]",
                    "   yLabel track-width (#tau)",
                    "   noUnderFlowPull",
                ] + sel_cut + ["EndVar", ""]

                #               ### Tau met  dPhi
                #               cfg += [
                #                   "NewVar",
                #                   "   Type 1D",
                #                   "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauMetDPhi" % (trig_def, kind, prong, lep_kind),
                #                   "   Template fine_dPhiMet_ff",
                #                   "   |dPhiToMetReader| %s%s[0] MetTST" % (collection, "" if prong == "Incl" else prong),
                #                   "   xLabel |#Delta #phi| (#tau, E_{T}^{miss})",
                #               ] + sel_cut + ["EndVar", ""]

                ### Tau pt - |eta|
                cfg += [
                    "NewVar", "    Type 2D", "    Template fine_tau_pt_abseta_ff",
                    "   ParReader %s%s pt[0]" % (collection, "" if prong == "Incl" else prong),
                    "   ParReader %s%s |eta|[0]" % (collection, "" if prong == "Incl" else prong),
                    "    xLabel p_{T} (#tau^{baseline}) [GeV]", "    yLabel |#eta|(#tau^{baseline})",
                    "    Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauPtAbsEta" % (trig_def, kind, prong, lep_kind)
                ] + sel_cut + ["EndVar", ""]

#                ### Tau pt -- eta
#                cfg += [
#                    "NewVar", "    Type 2D", "    Template fine_tau_pt_eta_ff",
#                    "   ParReader %s%s pt[0]" % (collection, "" if prong == "Incl" else prong),
#                    "   ParReader %s%s eta[0]" % (collection, "" if prong == "Incl" else prong), "    xLabel p_{T} (#tau^{baseline}) [GeV]",
#                    "    yLabel #eta(#tau^{baseline})",
#                    "    Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varTauPtEta" % (trig_def, kind, prong, lep_kind)
#                ] + sel_cut + ["EndVar", ""]

            if lep_kind != "Incl" and kind == "Incl":
                cfg += ["endif\n\n"]

    if kind != "Incl": cfg += ["endif"]
    cfg += ["\n\n\n"]

for kind, collection in LeptonCollections.iteritems():
    #break
    if kind != "Incl": cfg += ["ifdef isMC"]
    for trig_def, trig_cut in Trigger_Comb.iteritems():
        for tau_kind, tau_collection in TauCollections.iteritems():
            if tau_kind != "Incl" and kind == "Incl":
                cfg += ["ifdef isMC"]

            for prong in ["Incl", "1P", "3P"]:
                tau_cut = [
                    "%sCombCut AND" % (FillWhiteSpaces(4)),
                ] + trig_cut + ["%sNumParCut %s%s > 0 " % (FillWhiteSpaces(4), tau_collection, "" if prong == "Incl" else prong)]
                if prong != "Incl":
                    tau_cut += ["    ParCut %s%s NTrks[0] = %s" % (tau_collection, "" if prong == "Incl" else prong, prong[0])]
                tau_cut += ["End_CombCut"]

                #cfg += [
                #    "NewVar",
                #    "   Type 1D",
                #    "   Template fine_abseta_ff",
                #    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varLepAbsEta" % (trig_def, tau_kind, prong, kind),
                #    "   ParReader %s |eta|[0]" % (collection),
                #    "   xLabel |#eta| (#it{l})",
                #] + ["   %s" % (c) for c in tau_cut] + ["EndVar"]
                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Template fine_lep_pt_ff",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varLepPt" % (trig_def, tau_kind, prong, kind),
                    "   ParReader %s pt[0]" % (collection),
                    "   xLabel p_{T} (#it{l}) [GeV]",
                ] + ["   %s" % (c) for c in tau_cut] + ["EndVar"]

                #cfg += [
                #    "NewVar",
                #    "   Type 1D",
                #    "   Template fine_dPhiMet_ff",
                #    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varLepMetDPhi" % (trig_def, tau_kind, prong, kind),
                #    "   |dPhiToMetReader| %s[0] MetTST" % (collection),
                #    "   xLabel |#Delta #phi| (#it{l}, E_{T}^{miss})",
                #] + ["   %s" % (c) for c in tau_cut] + ["EndVar"]
                #cfg += [
                #    "NewVar",
                #    "   Type 2D",
                #    "   Template fine_lep_pt_abseta_ff",
                #    "   ParReader %s pt[0]" % (collection),
                #    "   ParReader %s |eta|[0]" % (collection),
                #    "   xLabel p_{T} (#it{l}^{baseline}) [GeV]",
                #    "   yLabel |#eta|(#it{l}^{baseline})",
                #    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varLepPtAbsEta" % (trig_def, tau_kind, prong, kind),
                #] + ["   %s" % (c) for c in tau_cut] + ["EndVar"]
                #### AbsMet
                cfg += [
                    "NewVar",
                    "   Type 1D",
                    "   Template met_et",
                    "   Name TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_varMet" % (trig_def, tau_kind, prong, kind),
                    "   EvReader floatGeV MetTST_met",
                    "   xLabel E_{T}^{miss} [GeV]",
                ] + ["   %s" % (c) for c in tau_cut[0:1] + ["NumParCut %s = 1" % (collection)] + tau_cut[1:]] + ["EndVar"]
            if tau_kind != "Incl" and kind == "Incl":
                cfg += ["endif\n\n"]

    if kind != "Incl": cfg += ["endif"]
    cfg += ["\n\n\n"]

WriteList(cfg, "XAMPPplotting/data/HistoConf/LepHadStau/Histos_RevisedFF.conf")
