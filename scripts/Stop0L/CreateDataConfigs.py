#!/usr/bin/env python
import os
import sys
import commands
import mimetypes

ROOTCOREBIN = os.getenv("ROOTCOREBIN")

Periods = {
    # 2015
    "2015_D": [
        (276073, 276954),
    ],
    "2015_E": [
        (278727, 279928),
    ],
    "2015_F": [
        (279932, 280422),
    ],
    "2015_G": [
        (280423, 281075),
    ],
    "2015_H": [
        (281130, 281411),
    ],
    "2015_I": [
        (281662, 282482),
    ],
    "2015_J": [
        (282625, 284484),
    ],
    "2015_All": [
        (276073, 284484),
    ],
    # 2016
    "2016_A": [
        (296939, 300287),
    ],
    "2016_B": [
        (300345, 300908),
    ],
    "2016_C": [
        (301912, 302393),
    ],
    "2016_D": [
        (302737, 303560),
    ],
    "2016_E": [
        (303638, 303892),
    ],
    "2016_F": [
        (303943, 304494),
    ],
    "2016_G": [
        (305291, 306714),
    ],
    "2016_H": [
        (305359, 310216),
    ],
    "2016_I": [
        (307124, 308084),
    ],
    "2016_J": [
        (308979, 309166),
    ],
    "2016_K": [
        (309311, 309759),
    ],
    "2016_L": [
        (310015, 311481),
    ],
    "2016_All": [
        (296939, 999999),
    ],
    "Run2": [
        (276073, 999999),
    ],
}

ExcludeFromAllLists = []


def GetFiles(files, directory):
    Files = []
    for f in files:
        if os.path.isdir(directory + f):
            for myfile in GetFiles(os.listdir(directory + f), directory + f):
                Files.append(myfile)
        else:
            Files.append(directory + '/' + f)
    return Files


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Script for creating Data input configs",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', dest='indir', help='Folder with files', required=True)
    parser.add_argument('-o', '--output', dest='outdir', help='Output directory to put file list(s) into')
    parser.add_argument('-f',
                        '--readROOTFiles',
                        help='Directly read from folder containing merged root files',
                        action='store_true',
                        default=False)
    options = parser.parse_args()

    if options.outdir:
        os.system("mkdir -p %s" % options.outdir)

    files = os.listdir(options.indir)

    if len(files) == 0:
        print 'No files found, exiting...'
        sys.exit(1)
    if mimetypes.guess_type(files[0])[0] != None:
        print 'Having found a text file, assuming to create input configs from file lists...'
        if options.readROOTFiles:
            print 'Cannot use the --readROOTFiles option when reading from text files, exiting...'
            sys.exit(1)
    else:
        print 'Having found a root file, assuming to create input configs from root files...'
        if not options.readROOTFiles:
            print 'Use the --readROOTFiles option when reading from root files, exiting...'
            sys.exit(1)

    allfiles = GetFiles(files, options.indir)

    FilesPerPeriod = {}
    ListsPerPeriod = {}

    for thing in allfiles:
        if options.readROOTFiles:
            if not 'Data' in thing: continue
            if 'Stop0L.00' in thing:
                fileperiod = str(thing.split('Stop0L.00')[-1].split(".")[0])
            elif 'Stop0LGamma.00' in thing:
                fileperiod = str(thing.split('Stop0LGamma.00')[-1].split(".")[0])
            elif 'Stop0L.period' in thing:
                fileperiod = str(thing.split('Stop0L.period')[-1].split(".")[0])
                if 'Data15' in thing:
                    fileperiod = '2015_%s' % fileperiod
                elif 'Data16' in thing:
                    fileperiod = '2016_%s' % fileperiod
            elif 'Stop0LGamma.period' in thing:
                fileperiod = str(thing.split('Stop0LGamma.period')[-1].split(".")[0])
                if 'Data15' in thing:
                    fileperiod = '2015_%s' % fileperiod
                elif 'Data16' in thing:
                    fileperiod = '2016_%s' % fileperiod
            else:
                print 'File %s not recognized!' % thing
            if fileperiod in Periods.iterkeys():
                if not fileperiod in FilesPerPeriod.iterkeys():
                    FilesPerPeriod[fileperiod] = []
                FilesPerPeriod[fileperiod] += [thing]
                for Period in Periods.iterkeys():
                    if Period == fileperiod:
                        continue
                    thisperiod = ""
                    if Periods[fileperiod][0][0] >= Periods[Period][0][0] and Periods[fileperiod][0][0] <= Periods[Period][0][1]:
                        thisperiod = Period
                        if thisperiod != "" and ((not thisperiod in ExcludeFromAllLists and 'All' in thisperiod)
                                                 or "All" not in thisperiod):
                            if not thisperiod in FilesPerPeriod.iterkeys():
                                FilesPerPeriod[thisperiod] = []
                            FilesPerPeriod[thisperiod] += [thing]
            else:
                thisperiod = ""
                if int(fileperiod) > 0:
                    rstr = str(fileperiod)
                    if not rstr in FilesPerPeriod.iterkeys():
                        FilesPerPeriod[rstr] = []
                    else:
                        print "Careful, duplicate run!!"
                    FilesPerPeriod[rstr] += [thing]
                    for Period in Periods.iterkeys():
                        thisperiod = ""
                        if int(fileperiod) >= Periods[Period][0][0] and int(fileperiod) <= Periods[Period][0][1]:
                            thisperiod = Period
                        if thisperiod != "" and ((not thisperiod in ExcludeFromAllLists and 'All' in thisperiod)
                                                 or "All" not in thisperiod):
                            if not thisperiod in FilesPerPeriod.iterkeys():
                                FilesPerPeriod[thisperiod] = []
                            FilesPerPeriod[thisperiod] += [thing]
        else:
            if not 'Data' in thing:
                continue
            with open("%s/%s" % (options.indir, thing), "r") as fin:
                content = [l.split("\n")[0] for l in fin.readlines()]
            if "period" in thing:
                fileperiod = str(thing.split(".period")[-1].split(".")[0])
                if 'Data15' in thing:
                    fileperiod = '2015_%s' % fileperiod
                elif 'Data16' in thing:
                    fileperiod = '2016_%s' % fileperiod
                if not fileperiod in FilesPerPeriod.iterkeys():
                    FilesPerPeriod[fileperiod] = []
                    ListsPerPeriod[fileperiod] = []
                FilesPerPeriod[fileperiod] += content
                if not thing in ListsPerPeriod[fileperiod]:
                    ListsPerPeriod[fileperiod].append(options.indir + "/" + thing)
                for Period in Periods.iterkeys():
                    if Period == fileperiod:
                        continue
                    thisperiod = ""
                    if Periods[fileperiod][0][0] >= Periods[Period][0][0] and Periods[fileperiod][0][0] <= Periods[Period][0][1]:
                        thisperiod = Period
                        if thisperiod != "" and ((not thisperiod in ExcludeFromAllLists and 'All' in thisperiod)
                                                 or "All" not in thisperiod):
                            if not thisperiod in FilesPerPeriod.iterkeys():
                                FilesPerPeriod[thisperiod] = []
                                ListsPerPeriod[thisperiod] = []
                            FilesPerPeriod[thisperiod] += content
                            if not thing in ListsPerPeriod[thisperiod]:
                                ListsPerPeriod[thisperiod].append(options.indir + "/" + thing)
            elif ".00" in thing:
                runno = int(thing.split(".00")[-1].split(".")[0])
                rstr = str(runno)
                if not rstr in FilesPerPeriod.iterkeys():
                    FilesPerPeriod[rstr] = []
                    ListsPerPeriod[rstr] = []
                else:
                    print "Careful, duplicate run!!"
                FilesPerPeriod[rstr] += content
                if not thing in ListsPerPeriod[rstr]:
                    ListsPerPeriod[rstr].append(options.indir + "/" + thing)
                for Period in Periods.iterkeys():
                    thisperiod = ""
                    if runno >= Periods[Period][0][0] and runno <= Periods[Period][0][1]:
                        thisperiod = Period
                        if thisperiod != "" and ((not thisperiod in ExcludeFromAllLists and 'All' in thisperiod)
                                                 or "All" not in thisperiod):
                            if not thisperiod in FilesPerPeriod.iterkeys():
                                FilesPerPeriod[thisperiod] = []
                                ListsPerPeriod[thisperiod] = []
                            FilesPerPeriod[thisperiod] += content
                            if not thing in ListsPerPeriod[thisperiod]:
                                ListsPerPeriod[thisperiod].append(options.indir + "/" + thing)

    for Period in FilesPerPeriod.iterkeys():
        word = "Run"
        # check of Period is a real period
        if Period in Periods.iterkeys():
            word = "Period"
        else:
            # convert the runNumber in the right real period
            thisperiod = ""
            for PeriodName in Periods.iterkeys():
                if int(Period) >= Periods[PeriodName][0][0] and int(Period) <= Periods[PeriodName][0][1]:
                    thisperiod = PeriodName

        print " ====== Writing Input list for %s %s ========" % (word, Period)

        with open("%s/Data_%s_%s_Input.conf" % (options.outdir, word, Period), "w") as fout:
            fout.write("SampleName %s\n" % Period)
            fout.write("\n".join(["Input " + p for p in sorted(FilesPerPeriod[Period])]))
            fout.write("\n")
