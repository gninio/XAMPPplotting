from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.CalculateLumiFromIlumicalc import *


def CalculateEfficiency(probe, match):
    efficiencyName = "eff_%s" % (probe.GetName().replace("probe", ""))
    effGraph = ROOT.TGraphAsymmErrors(probe)
    effGraph.Set(0)
    effGraph.SetMarkerStyle(ROOT.kFullDotLarge)
    # create a new TGraphAsymmErrors for doing the efficiency calculation
    eff2 = ROOT.TGraphAsymmErrors()
    # create 2 helper histograms needed for the default efficiency calculation
    hx1 = ROOT.TH1F("hx1%s" % efficiencyName, "hx1%s" % efficiencyName, 1, 0, 1)
    hx2 = ROOT.TH1F("hx2%s" % efficiencyName, "hx2%s" % efficiencyName, 1, 0, 1)
    for i in range(probe.GetNbinsX()):
        return1Pm1 = False
        if match.GetBinContent(i + 1) > probe.GetBinContent(i + 1):
            print 'WARNING: More matches than probes in %s, returning 1\pm 1' % probe.GetName()
            return1Pm1 = True
        if not probe.GetBinContent(i + 1) > 0.:
            print 'WARNING: No probes found in %s, returning 1\pm 1' % probe.GetName()
            return1Pm1 = True
        if not return1Pm1:
            hx1.SetBinContent(1, match.GetBinContent(i + 1))
            hx1.SetBinError(1, match.GetBinError(i + 1))
            hx2.SetBinContent(1, probe.GetBinContent(i + 1))
            hx2.SetBinError(1, probe.GetBinError(i + 1))
            # reset the helper TGraphAsymmErrors
            eff2.Set(0)

            #####################################
            # which Divide option to use here??
            #####################################
            #             eff2.Divide(hx1, hx2, 'cl=0.683 b(1,1) mode e0')
            eff2.Divide(hx1, hx2, 'norm')
            #####################################

            if eff2.GetN() == 0:
                continue

            efficiency = eff2.GetY()[0]
            efficiencyUp = eff2.GetErrorYhigh(0)
            efficiencyDown = eff2.GetErrorYlow(0)
        else:
            efficiency = 1.
            efficiencyUp = 1.
            efficiencyDown = 1.
        xc = probe.GetXaxis().GetBinCenter(i + 1)
        xu = probe.GetXaxis().GetBinUpEdge(i + 1)
        xl = probe.GetXaxis().GetBinLowEdge(i + 1)
        dxu = xu - xc
        dxl = xc - xl

        effGraph.SetPoint(effGraph.GetN(), xc, efficiency)
        effGraph.SetPointEXhigh(effGraph.GetN() - 1, dxu)
        effGraph.SetPointEXlow(effGraph.GetN() - 1, dxl)
        effGraph.SetPointEYhigh(effGraph.GetN() - 1, efficiencyUp)
        effGraph.SetPointEYlow(effGraph.GetN() - 1, efficiencyDown)
    return effGraph


triggerPeriods = {
    "HLT_xe70_mht": (0, 284484),
    "HLT_xe70_tc_lcw": (0, 284484),
    "HLT_xe90_mht_L1XE50": (296939, 302872),
    "HLT_xe100_mht_L1XE50": (302919, 304008),
    "HLT_xe110_mht_L1XE50": (304128, 311481),
}

if __name__ == "__main__":

    MCfile = ROOT.TFile("/ptmp/mpp/niko/Cluster/Output/2018-04-11/stop_triggerTurnOn/output_histograms.root", "READ")
    Datafile = ROOT.TFile("/ptmp/mpp/niko/Cluster/Output/2018-04-11/stop_triggerTurnOn/output_histograms_data.root", "READ")

    MCmetnoTrig = MCfile.Get("Stop0L_Nominal/TriggerTurnOn_noTrig/met")

    styling = MCmetnoTrig.Clone("%s_style" % MCmetnoTrig.GetName())

    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    pu = PlotUtils(status="Internal", lumi=1., size=22)
    pu.Prepare1PadCanvas("MC", 800, 600)

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)
    dummy.SaveAs("AllTrigTurnOn.pdf[")

    prw = SetupPRWTool(ilumicalcFiles=[
        "/afs/ipp-garching.mpg.de/home/n/niko/0L/TriggerTest/XAMPPbase/data/GRL/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.root",
        "/afs/ipp-garching.mpg.de/home/n/niko/0L/TriggerTest/XAMPPbase/data/GRL/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.root"
    ])

    for trig in ["HLT_xe70_mht", "HLT_xe70_tc_lcw", "HLT_xe90_mht_L1XE50", "HLT_xe100_mht_L1XE50", "HLT_xe110_mht_L1XE50"]:

        DatametnoTrig = Datafile.Get("Stop0L_Nominal/TriggerTurnOn_noTrig%s/met" % trig)

        pu.Lumi = CalculateLumiFromRuns(triggerPeriods[trig][0], triggerPeriods[trig][1])
        pu.GetCanvas().cd()

        MCmetTrig = MCfile.Get("Stop0L_Nominal/TriggerTurnOn_Trig%s/met" %
                               ("HLT_xe100_mht_L1XE50" if trig == "HLT_xe110_mht_L1XE50" else trig))
        DatametTrig = Datafile.Get("Stop0L_Nominal/TriggerTurnOn_Trig%s/met" % trig)

        MCturnOn = CalculateEfficiency(MCmetnoTrig, MCmetTrig)
        DataturnOn = CalculateEfficiency(DatametnoTrig, DatametTrig)

        styling.Reset()
        styling.GetYaxis().SetTitle("Efficiency")
        pu.drawStyling(styling, 0., 1.01, TopPad=True)
        #         print styling.GetMinimum(),styling.GetMaximum()
        #         styling.Draw()

        MCturnOn.SetMarkerStyle(ROOT.kOpenCircle)
        MCturnOn.SetMarkerColor(ROOT.kRed)
        MCturnOn.SetLineColor(ROOT.kRed)
        MCturnOn.SetTitle("MC (W#rightarrow #mu#nu)")
        DataturnOn.SetTitle("Data")
        DataturnOn.SetLineColor(ROOT.kBlack)

        MCturnOn.Draw("Psame")
        DataturnOn.Draw("Psame")

        pu.DrawLumiSqrtS(0.9, 0.29, align=31)
        pu.DrawTLatex(0.9, 0.22, trig, align=31)

        pu.CreateLegend(0.65, 0.5, 0.94, 0.65)
        pu.AddToLegend(DataturnOn, "PL")
        pu.AddToLegend(MCturnOn, "PL")
        pu.DrawLegend()

        pu.saveHisto("trigTurnOn%s" % trig, ["pdf"])
        pu.GetCanvas().SaveAs("AllTrigTurnOn.pdf")

    dummy.SaveAs("AllTrigTurnOn.pdf]")
