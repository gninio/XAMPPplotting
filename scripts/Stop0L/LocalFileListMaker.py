#!/usr/bin/env python

import sys
import os, errno
import subprocess


def parseCommandLine():
    """
    Builds the command line argument parser.
    """

    try:

        # Requires python 2.7, which we don't appear to have (?)
        # I don't want to throw away this code, as it may be useful in the future
        import argparse
        parser = argparse.ArgumentParser(description="""Script for making file list objects in a configurable way.""", )

        parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.1', help='Prints version string')
        # Could add more (keyword) arguments here
        parser.add_argument('-d', '--debug', action='store_true', dest='debug', help='Turns on extra output')
        parser.add_argument('-i', '--input', dest='indir', help='Folder with files', required=True)
        parser.add_argument('-o', '--output', dest='outdir', help='Output directory to put file list(s) into')

        return parser.parse_args()

    except ImportError:

        # Last python version for OptParse!
        from optparse import OptionParser

        parser = OptionParser(
            description="""Script for making data file list from downloaded CAF data on /ptmp.""",
            version='%prog 0.1',
            usage="""usage: %prog [options] INPUT

    INPUT: Folder with files to put into the filelist""",
        )

        # Could add more (keyword) arguments here
        parser.add_option('-d', '--debug', action='store_true', dest='debug', help='Turns on extra output')
        parser.add_argument('-i', '--input', dest='indir', help='Folder with files')
        parser.add_option('-o', '--output', dest='outdir', help='Output directory to put file list(s) into')
        # TODO: Final checks require the DS type to be added (ie merge.AOD or PhysCont.AOD)

        (options, args) = parser.parse_args()

        # Check that we have the correct number of arguments
        try:
            assert (len(args) in range(1, 4))
        except AssertionError:
            parser.error('Invalid number of command-line arguments: one or two expected')
            # parser.error exits the code here

        options.indir = args[0]
        options.outdir = args[1]
        try:
            options.VUAsuffix = args[2]
        except IndexError:
            options.VUAsuffix = ''

        return options


if __name__ == '__main__':

    # Get user options
    options = parseCommandLine()

    files_tmp = os.listdir(options.indir)

    PerRunStuff = {}
    if "." in options.indir:
        print "is this already one folder containing the root files? -> %s" % options.indir

        print 'then only taking this one...'
        f = options.indir
        if "c914" in f:
            stuff = [os.getcwd() + "/" + f + "/" + g for g in os.listdir(f) if "13TeV" in g and "NTUP_MCPTP" in g]
            runNumber = f.split(".")[1].split(".")[0]
            if not runNumber in PerRunStuff.iterkeys():
                PerRunStuff[runNumber] = []
            PerRunStuff[runNumber] += stuff

    else:
        for f in files_tmp:
            if not "c914" in f:
                continue
            if os.path.isdir(options.indir + "/" + f):
                stuff = [
                    os.getcwd() + "/" + options.indir + "/" + f + "/" + g for g in os.listdir(options.indir + "/" + f)
                    if "13TeV" in g and "NTUP_MCPTP" in g
                ]
                runNumber = f.split(".")[1].split(".")[0]
                if not runNumber in PerRunStuff.iterkeys():
                    PerRunStuff[runNumber] = []
                PerRunStuff[runNumber] += stuff

    if options.outdir != None:
        outdir = options.outdir
    else:
        outdir = "test"

    os.system("mkdir -p %s" % outdir)

    for run, files in PerRunStuff.iteritems():
        with open("%s/filelist_data15_13TeV.%s.physics_Main.NTUP_MCPTP.txt" % (outdir, run), "w") as fout:
            fout.write("\n".join([f for f in files]))
            fout.write("\n")

    from pprint import pprint
    #pprint (PerRunStuff)

    print 'Done. Have a look at folder: %s' % outdir
