#!/usr/bin/env python
import os
import sys
import commands
import mimetypes

ROOTCOREBIN = os.getenv("ROOTCOREBIN")


def GetNewTreeName(filename, options):
    if not options.DoDMHF:
        if 'ttbar_Herwigpp' in filename: return options.region + '__SYST__PowhegHerwigpp__1up'
        elif 'singleTop_Herwigpp' in filename: return options.region + '__SYST__PowhegHerwigpp__1up'
        elif 'singleTop_radHi' in filename: return options.region + '__SYST__Rad__1up'
        elif 'singleTop_radLo' in filename: return options.region + '__SYST__Rad__1down'
        elif 'singleTop_DS' in filename: return options.region + '__SYST__DS__1up'
        elif 'ttbar_Input' in filename or 'singleTop_Input' in filename: return options.region
        elif 'ttbar_McAtNlo' in filename: return options.region + '__SYST__aMCAtNLO__1up'
        elif 'ttbar_radHi' in filename: return options.region + '__SYST__Rad__1up'
        elif 'ttbar_radLo' in filename: return options.region + '__SYST__Rad__1down'
    else:
        if 'ttbar_Herwigpp' in filename: return options.region + '_PowhegHerwigpp__1up'
        elif 'singleTop_Herwigpp' in filename: return options.region + '_PowhegHerwigpp__1up'
        elif 'singleTop_radHi' in filename: return options.region + '_Rad__1up'
        elif 'singleTop_radLo' in filename: return options.region + '_Rad__1down'
        elif 'singleTop_DS' in filename: return options.region + '_DS__1up'
        elif 'ttbar_Input' in filename or 'singleTop_Input' in filename: return options.region
        elif 'ttbar_McAtNlo' in filename: return options.region + '_aMCAtNLO__1up'
        elif 'ttbar_radHi' in filename: return options.region + '_Rad__1up'
        elif 'ttbar_radLo' in filename: return options.region + '_Rad__1down'
    return None


def GetOldTreeName(filename):
    if 'ttbar_Herwigpp' in filename: return 'ttbar_Herwigpp_Nominal'
    elif 'singleTop_Herwigpp' in filename: return 'singleTop_Herwigpp_Nominal'
    elif 'singleTop_DS' in filename: return 'singleTop_DS_Nominal'
    elif 'singleTop_radHi' in filename: return 'singleTop_radHi_Nominal'
    elif 'singleTop_radLo' in filename: return 'singleTop_radLo_Nominal'
    elif 'singleTop_Input' in filename: return 'singleTop_Nominal'
    elif 'ttbar_Input' in filename: return 'ttbar_Nominal'
    elif 'ttbar_McAtNlo' in filename: return 'ttbar_McAtNlo_Nominal'
    elif 'ttbar_radHi' in filename: return 'ttbar_radHi_Nominal'
    elif 'ttbar_radLo' in filename: return 'ttbar_radLo_Nominal'


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Script for creating Data input configs",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', help='input files to merge', nargs='+', required=True)
    parser.add_argument('-r', '--region', help='name of region (e.g. stop_0Lep)', required=True)
    parser.add_argument('--DoDMHF', help='use for merging DMHF trees', action="store_true", default=False)
    options = parser.parse_args()

    import ROOT

    InputFilesToMerge = {}
    NominalFileName = None
    for i in options.input:
        if os.path.exists(i):
            if GetNewTreeName(i, options) == options.region: NominalFileName = i
            else: InputFilesToMerge[i] = GetNewTreeName(i, options)
    if len(InputFilesToMerge) == 0 or not NominalFileName:
        print 'No files found, exiting...'
        sys.exit(1)

    iteration = 0
    for ifile, newTreeName in InputFilesToMerge.iteritems():
        iteration += 1
        print 'Reading file %s ...' % ifile
        TFile = ROOT.TFile(ifile, "READ")
        if TFile == None or not TFile.IsOpen():
            print "ERROR: Could not open the file " + ifile
            sys.exit(1)
        if options.DoDMHF: TTree = TFile.Get(GetOldTreeName(ifile))
        else: TTree = TFile.Get(options.region)
        # TTree.SetName('WTF%s'%iteration)
        TTree.SetName(newTreeName)
        newTree = None
        ROOT.gROOT.cd()
        newTree = TTree.CloneTree()
        TFile.Close()
        newTree.SetAutoFlush(0)
        newTree.SetAutoSave(0)
        print 'Writing tree %s to nominal file %s ...' % (newTreeName, NominalFileName)
        NominalFile = ROOT.TFile(NominalFileName, "UPDATE")
        NominalFile.cd()
        NominalFile.WriteTObject(newTree, newTreeName)
        NominalFile.Close()
