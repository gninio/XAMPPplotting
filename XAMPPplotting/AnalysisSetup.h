#ifndef XAMPPPLOTTING_ANALYSISSETUP_H
#define XAMPPPLOTTING_ANALYSISSETUP_H
#include <XAMPPplotting/Analysis.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>

#include <map>
#include <memory>
#include <string>
#include <vector>
namespace XAMPP {
    class Analysis;
    class ParticleReader;
    class Condition;
    class Histo;
    class ITreeVarReader;
    class ITreeVarWriter;
    class DMXSecUpdater;
    class UniqueEventCondition;
    class AnalysisRegion {
    public:
        AnalysisRegion(const std::string& Name);

        ~AnalysisRegion();
        std::string name() const;
        bool AppendCut(std::vector<std::shared_ptr<Condition>> Cuts);
        bool AppendCut(std::shared_ptr<Condition> C);

        bool PrependCut(std::vector<std::shared_ptr<Condition>> Cuts);
        bool PrependCut(std::shared_ptr<Condition> C);

        void Finalize();
        const std::vector<std::shared_ptr<Condition>>& GetCuts() const;
        std::shared_ptr<Condition> RetrieveCompressedCut();
        ITreeVarReader* RegionReader() const;

        std::shared_ptr<Histo> CreateHistoArray(const std::vector<std::shared_ptr<Histo>>& HistosToUse, bool DoCutFlow);

    private:
        std::string m_RegionName;
        bool m_Init;
        std::vector<std::shared_ptr<Condition>> m_Cuts;
        std::shared_ptr<Condition> m_SummarizedCut;
        ITreeVarReader* m_region_reader;
    };

    class AnalysisSetup {
    public:
        AnalysisSetup();
        virtual ~AnalysisSetup();
        virtual void SetOutputLocation(const std::string& File);
        virtual bool ParseConfig(std::string HistoConfig, std::string RunConfig, std::string InputConfig);
        virtual bool ParseTreeConfig(std::string TreeConfig, std::string InputConfig);

        std::shared_ptr<Analysis> SetupAnalyses(unsigned int begin = 0, unsigned int end = -1);
        std::shared_ptr<Analysis> SetupNTupleWriters(unsigned int begin = 0, unsigned int end = -1);
        std::shared_ptr<Analysis> SetupEventChecker();
        void LoadWeightsOnly();
        void ignoreMetaInput();

        std::vector<std::string> inputFiles() const;

        bool do_kinematic_syst() const;
        bool do_weight_syst() const;

    protected:
        typedef bool (AnalysisSetup::*ConfigReader)(std::ifstream& inf);

    protected:
        enum PropertyStatus { Failure = 0, NothingFound, New };
        virtual int CheckAnaConfigProperties(std::ifstream& inf, const std::string& line);
        virtual int CheckInputConfigProperties(std::ifstream& inf, const std::string& line);

        bool ReadInputFiles(std::ifstream& inf);
        bool ReadAnaConfig(std::ifstream& inf);
        bool loadWeightsOnly() const;

    private:
        bool ReadHistoConfig(std::ifstream& inf);
        bool ReadTreeConfig(std::ifstream& inf);

        bool ImportFile(const std::string& Config, ConfigReader Parser, bool always = false);

        bool CreateNewRegion(const std::string& Name);
        bool RenameSystematic(std::stringstream& sstr);
        bool ReplaceStringOfSystematic(std::stringstream& sstr);
        void setPRWperiod(std::stringstream& sstr);

    protected:
        void ParseOptionsToAnalysis(std::shared_ptr<Analysis> Ana, unsigned int begin = 0, unsigned int end = -1);
        bool FinalizeActRegion();
        bool InitMetaData();
        void ClearVectors();
        void SetCrossSectionDir();

    private:
        std::vector<std::string> m_InputFiles;
        std::vector<std::string> m_MetaInputFiles;

        std::vector<std::string> m_PRWlumiFiles;
        std::vector<std::string> m_PRWconfigFiles;

        std::vector<std::string> m_Systematics;
        std::vector<std::string> m_Weights;

        std::vector<std::string> m_DataWeights;
        std::vector<std::string> m_WeightSystematics;

    protected:
        std::vector<ITreeVarReader*> m_Readers;

    private:
        std::vector<std::shared_ptr<Condition>> m_Cuts;
        std::vector<std::shared_ptr<Histo>> m_Histos;

    protected:
        std::string m_Tree;
        std::string m_SampleName;
        std::string m_AnaName;

        std::string m_outFile;

    private:
        bool m_NormSUSYprocs;
        bool m_ApplyXS;
        bool m_doSyst;
        bool m_doSystW;
        bool m_doNominal;
        bool m_doCutFlow;
        bool m_ignoreMetaInputs;

    protected:
        std::vector<std::string> m_ImportedConfigs;

        std::map<std::string, std::shared_ptr<AnalysisRegion>> m_RegionCuts;
        std::shared_ptr<AnalysisRegion> m_ActRegion;

        std::shared_ptr<Analysis> m_Analysis;

    private:
        std::vector<ITreeVarWriter*> m_Branches;
        std::map<std::string, std::string> m_ReplaceBranchNames;
        std::map<std::string, std::string> m_ReplaceSystNames;
        std::map<std::string, std::string> m_ReplaceStringOfSyst;

        std::string m_SystPrefix;

        bool m_LoadWeightsOnly;

        std::string m_xSecDirSig;
        std::string m_xSecDirBkg;
        std::unique_ptr<XAMPP::DMXSecUpdater> m_DMXSec;
        bool m_Debug;

    protected:
        std::string m_NominalName;
        bool m_SetNominalName;
        bool m_IsHistFitInput;

    private:
        std::vector<std::string> m_GRLs;
        std::shared_ptr<UniqueEventCondition> m_uniqueRun;
    };
}  // namespace XAMPP
#endif  // ANALYSISSETUP_H
