#ifndef XAMPPplotting_HistFitterDataTreeReader_H
#define XAMPPplotting_HistFitterDataTreeReader_H

#include <TFile.h>
#include <TTree.h>
#include <XAMPPplotting/MetaDataTreeReader.h>

namespace XAMPP {
    class HistFitterNormDataBase : public NormalizationDataBase {
    public:
        static NormalizationDataBase *getDataBase();
        bool init(const std::vector<std::string> &In) override;
        bool init(const std::vector<std::shared_ptr<TFile>> &In) override;

        bool isData() const override;
        virtual ~HistFitterNormDataBase() = default;
        void SetNominalName(const std::string &Nominal);

    protected:
        HistFitterNormDataBase();
        HistFitterNormDataBase(const HistFitterNormDataBase &) = delete;
        void operator=(const HistFitterNormDataBase &) = delete;
        TTree *GetNominalTree(const std::string &Path);
        bool isDataTree(TTree *tree);
        bool m_isData;
        std::string m_NominalName;
        std::shared_ptr<TFile> m_file;
    };
    // Sometimes people want to pass data and mc within a single job. This is insane
    // But let's make it possible
    class HybridNormDataBase : public NormalizationDataBase {
    public:
        static NormalizationDataBase *getDataBase();
        bool init(const std::vector<std::string> &In) override;
        bool init(const std::vector<std::shared_ptr<TFile>> &In) override;
        bool isData() const override;
        ~HybridNormDataBase() = default;

    protected:
        HybridNormDataBase();
        HybridNormDataBase(const HybridNormDataBase &) = delete;
        void operator=(const HybridNormDataBase &) = delete;
    };
}  // namespace XAMPP
#endif
